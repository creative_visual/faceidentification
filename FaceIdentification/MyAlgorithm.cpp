#include "StdAfx.h"
#include "MyAlgorithm.h"
#include "recog\\ProcessRecog.h"

extern float *g_dict , *g_AT , *g_ATA ;
extern vector<int> g_class_flag;
extern int g_nsamples;
extern int g_samplew, g_sampleh;
extern vector<string> g_face_names, g_dirpathes, g_attention_names, g_attention_pathes;
extern vector<vector<int>> g_recog_flag;

extern int **g_base_features;
extern vector<int> g_det_db_flag;
extern int totalDim;

void myDetectionForSingle(IplImage* image,CascadeClassifier& cascade,FACE &face)//检测单个图片,获取人脸坐标
{
	if (!face.faces.empty())
	{
		face.faces.clear();
		vector<rect>().swap(face.faces);
	}

	int width = (int)image->width;
	int height = (int)image->height;

	IplImage *gray = cvCreateImage(cvSize(width,height), 8, 1);

	if( image )
	{
		cvCvtColor(image, gray, CV_BGR2GRAY);

		Mat frame_gray(gray);

		detect_gray(frame_gray, cascade, face.faces);
	}
	cvReleaseImage(&gray);
}

vector<int> myRecognitionForSingle(IplImage* image,CascadeClassifier& cascade,ALLFACE &face)//检测单个图片,获取人脸坐标
{
	vector<int> idx;
	if(!image)return idx;

	float *xvec = new float[ g_nsamples ];
	double font_scale = 2;
	CvFont font;//以下用文字标识说明
	double hScale=1, vScale=1;
	int lineWidth=1;
	cvInitFont(&font,/*CV_FONT_HERSHEY_SIMPLEX*/CV_FONT_HERSHEY_PLAIN |CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);

//	myDetectionForSingle(image,cascade,face.face);
	if (!face.name.empty())
	{
		face.name.clear();
		vector<CString>().swap(face.name);
	}

	for(int i = 0; i < face.face.faces.size(); i++)
	{
		int w = face.face.faces[i].xmax - face.face.faces[i].xmin + 1;
		int h = face.face.faces[i].ymax - face.face.faces[i].ymin + 1;

		IplImage *fimg = cvCreateImage(cvSize(w,h), 8, 1);

		copy_roi_pxl(fimg, image, face.face.faces[i].xmin, face.face.faces[i].ymin);
		//float *y = get_img_vector(fimg, g_samplew, g_sampleh);
		float *y = get_alignment_data_sparse(image, face.face.faces[i], g_samplew, g_sampleh);

		float lambda = 0.001;
		int recog_idx;

		l1ls_featuresign(g_dict, g_AT, g_ATA, g_samplew * g_sampleh, g_nsamples, y, 1,lambda, xvec, recog_idx);
		if (recog_idx>=0)
		{
			float e = get_residual(g_dict, g_nsamples, g_samplew*g_sampleh, y, xvec, recog_idx);

			if( e < g_samplew*g_sampleh*3.5 )
			{
				draw_recog_info(image, face.face.faces[i],  g_attention_names[g_class_flag[recog_idx]], font);//g_face_names[g_class_flag[recog_idx]
				CString name;
				name.Format("%s", g_attention_pathes[g_class_flag[recog_idx]].c_str());
				face.name.push_back(name);
				idx.push_back(recog_idx);
			}
			else
			{
				face.name.push_back("");
			}
		}
		else
		{
			face.name.push_back("");
		}
		
		delete[] y;
		cvReleaseImage(&fimg);
	}
	return idx;
}

vector<int> myRecognitionLBP(IplImage* image,CascadeClassifier& cascade,ALLFACE &face)//检测单个图片,获取人脸坐标
{
// 	ofstream outPut;
// 	outPut.open("F:\\1.txt",ios::app);
	vector<int> idx;
	if(!image)return idx;

	float *xvec = new float[ g_nsamples ];
	double font_scale = 2;
	CvFont font;//以下用文字标识说明
	double hScale=1, vScale=1;
	int lineWidth=1;
	cvInitFont(&font,/*CV_FONT_HERSHEY_SIMPLEX*/CV_FONT_HERSHEY_PLAIN |CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);

//	myDetectionForSingle(image,cascade,face.face);
	if (!face.name.empty())
	{
		face.name.clear();
		vector<CString>().swap(face.name);
	}
	vector<double> value;


	int *test_feature = new int[totalDim];
	for(int i = 0; i < face.face.faces.size(); i++)
	{
		int w = face.face.faces[i].xmax - face.face.faces[i].xmin + 1;
		int h = face.face.faces[i].ymax - face.face.faces[i].ymin + 1;

		memset(test_feature, 0, sizeof(int)*totalDim);
		get_face_feature(image, face.face.faces[i], test_feature);

		vector<double> corr( g_attention_names.size() );
		for(int j = 0; j < g_attention_names.size(); j++)
		{
// 			if( g_det_db_flag[j] )
// 			{
				double r = get_correlation(test_feature, g_base_features[j], totalDim);// 第 i 个测试图像与第 j 个数据库图像的相关性
				corr[j] = r;
// 			}
// 			else
// 			{
// 				corr[j] = 0;
// 			}
		}

		vector<int> index = descend_vec(corr);
		string recoged_name = g_attention_names[ index[0] ];//g_face_names[ index[0] ];

		if (corr[0]>0.650)
		{
//			draw_recog_info(image, face.face.faces[i], recoged_name, font);
			CString name;
			name.Format("%s", g_attention_pathes[index[0]].c_str());//g_dirpathes[index[0]].c_str()
 			face.name.push_back(name);
			value.push_back(corr[0]);

			idx.push_back(index[0]);
		}
		else
		{
			face.name.push_back("");
			value.push_back(0);
		}


	}
////////////////////////////////////////////////////////////////////////////
	int k=(int)value.size();
	if (k>1)
	{
		for (int i=0;i<k;i++)
		{
			for (int j=(i+1);j<k;j++)
			{
				if (face.name[i]==face.name[j])
				{
					if (value[i]>value[j])
					{
						face.name[j]="";
					}
					else
					{
						face.name[i]="";
					}
				}
			}
		}
	}
	value.clear();

	delete[] test_feature;
	return idx;
}

