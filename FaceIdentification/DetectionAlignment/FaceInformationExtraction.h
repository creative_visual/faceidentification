#ifndef __FACEINFORMATIONEXTRACTION_H__
#define __FACEINFORMATIONEXTRACTION_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>
#include "cxcore.h"
#include <windows.h>
#include "typedef.h"

typedef struct ExtractionInforParamsTag
{
	int Extraction_Width;
	int Extraction_Height;
	
	Rect RefFaceROI;
	IplImage *RefMaskImage;
	Loc *RefPts;
}ExtractInforParams;

void Apply_Similarity_T(float input_x,float input_y, float MT[9], float &output_x, float &output_y);

IplImage *Bicubic_SimilarityT(IplImage *img, int newWidth, int newHeight,float MT[9]);
IplImage *Bilinear_SimilarityT(IplImage *img, int newWidth, int newHeight, float MT[9]);


void FaceInformationExtraction(IplImage *GrayI, Mat_<double> current_shape,int landmark_number,
								  ExtractInforParams ExtractInfor, int type, 
								  vector <float> &FaceInforvector,Rect &AdjustmentR);

#endif