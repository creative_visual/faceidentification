#ifndef SVD_H
#define SVD_H

//extern
//int Singular_Value_Decomposition(float* A, int nrows, int ncols, float* U, float* singular_values, float* V, float* dummy_array);

//extern
void Singular_Value_Decomposition_Solve(float* U, float* D, float* V,  float tolerance, int nrows, int ncols, float *B, float* x);

//extern
//void Singular_Value_Decomposition_Inverse(float* U, float* D, float* V,  float tolerance, int nrows, int ncols, float *Astar);

int Matrix_inverse(float *M,float *M_inverse,int nrows,int ncols);


#endif
