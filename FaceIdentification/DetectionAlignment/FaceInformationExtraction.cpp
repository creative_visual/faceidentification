#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include "highgui.h"
#include <iostream>
#include <math.h>
#include "FaceAlignment.h"
#include "typedef.h"
#include "FaceInformationExtraction.h"
#include "../Matrix.h"

IplImage *Bilinear_SimilarityT(IplImage *img, int newWidth, int newHeight, float MT[9])
{
	int w= newWidth;
	int h = newHeight;
	IplImage * img2 =0;
	img2 = cvCreateImage(cvSize(w,h),IPL_DEPTH_8U,1);
	uchar *Data = (uchar *)img2->imageData;
	uchar *data = (uchar *)img->imageData;
	int a,b,c,d,x,y,index;
	float tx = (float)(img->width-1)/w;
	float ty = (float)(img->height-1)/h;
	float x_diff, y_diff;
	int i,j,k;
	float out_x,out_y;

	for(i=0; i<h; i++)
	{
		for(j=0; j<w; j++)
		{
			Apply_Similarity_T((float)j,(float)i,MT,out_x,out_y);
			x = (int)(out_x);//(int)(tx*j);
			y =(int)(out_y);

			x_diff= out_x-x;
			y_diff= out_y -y;
		     
			index = clip(y,0,img->height-1)*img->widthStep + (clip(x,0,img->width-1))*img->nChannels;
			// cac diem lan can
			a = (int)index;
			b = (int)(clip(y,0,img->height-1)*img->widthStep + (clip(x+1,0,img->width-1))*img->nChannels);                  
			c = (int)(clip(y+1,0,img->height-1)*img->widthStep + (clip(x,0,img->width-1))*img->nChannels);
			d = (int)(clip(y+1,0,img->height-1)*img->widthStep + (clip(x+1,0,img->width-1))*img->nChannels);
		     
			for(k=0; k<1; k++)
			{
			  float new_value = 
			  data[a+k]*(1-x_diff)*(1-y_diff)
			  +data[b+k]*(1-y_diff)*(x_diff)
			  +data[c+k]*(y_diff)*(1-x_diff)
			  +data[d+k]*(y_diff)*(x_diff);
			  Data[i*img2->widthStep + j*img2->nChannels  +k] = (uchar)(clip(new_value,0,255));
			}
		}
	}
	return img2;
}

IplImage *Bicubic_SimilarityT(IplImage *img, int newWidth, int newHeight, float MT[9])
{
     
	IplImage *img2 = cvCreateImage(cvSize(newWidth,newHeight),img->depth,img->nChannels);
	//img2 = createImage(newWidth,newHeight);
	uchar *data = (uchar *)img->imageData;
	uchar *Data = (uchar *)img2->imageData;
	int a,b,c,d,index;
	uchar Ca,Cb,Cc;
	uchar C[5];
	uchar d0,d2,d3,a0,a1,a2,a3;
	int i,j,k,ii,jj;
	int x,y;
	float dx,dy;
	float tx,ty;
	float out_x,out_y;
	for(i=0; i<newHeight; i++)
	{
		for(j=0; j<newWidth; j++)
		{
			// printf("%d : %d\n",i,j);

			Apply_Similarity_T((float)j,(float)i,MT,out_x,out_y);
			x = (int)(out_x);//(int)(tx*j);
			y =(int)(out_y);

			dx= out_x-x;
			dy= out_y -y;


			index = y*img->widthStep + x*img->nChannels;
			a = y*img->widthStep + (x+1)*img->nChannels;
			b = (y+1)*img->widthStep + x*img->nChannels;
			c = (y+1)*img->widthStep + (x+1)*img->nChannels ;

			for(k=0;k<img->nChannels;k++)
			{
				for(jj=0;jj<=3;jj++)
				{
					 d0 = data[clip((y-1+jj),0,img->height-1)*img->widthStep + clip((x-1),0,img->width-1)*img->nChannels +k] - data[clip((y-1+jj),0,img->height-1)*img->widthStep + clip(x,0,img->width-1)*img->nChannels +k] ;
					 d2 = data[clip((y-1+jj),0,img->height-1)*img->widthStep + clip((x+1),0,img->width-1)*img->nChannels +k] - data[clip((y-1+jj),0,img->height-1)*img->widthStep + clip(x,0,img->width-1)*img->nChannels +k] ;
					 d3 = data[clip((y-1+jj),0,img->height-1)*img->widthStep + clip((x+2),0,img->width-1)*img->nChannels +k] - data[clip((y-1+jj),0,img->height-1)*img->widthStep + clip(x,0,img->width-1)*img->nChannels +k] ;
					 a0 = data[clip((y-1+jj),0,img->height-1)*img->widthStep + clip((x),0,img->width-1)*img->nChannels +k];
					 a1 =  -1.0/3*d0 + d2 -1.0/6*d3;
					 a2 = 1.0/2*d0 + 1.0/2*d2;
					 a3 = -1.0/6*d0 - 1.0/2*d2 + 1.0/6*d3;
					 C[jj] = a0 + a1*dx + a2*dx*dx + a3*dx*dx*dx;
				}
				 d0 = C[0]-C[1];
				 d2 = C[2]-C[1];
				 d3 = C[3]-C[1];
				 a0=C[1];
				 a1 =  -1.0/3*d0 + d2 -1.0/6*d3;
				 a2 = 1.0/2*d0 + 1.0/2*d2;
				 a3 = -1.0/6*d0 - 1.0/2*d2 + 1.0/6*d3;
				 Cc = a0 + a1*dy + a2*dy*dy + a3*dy*dy*dy;
					 
				 Cc = data[clip(y,0,img->height-1) * img->widthStep + clip(x,0,img->width-1)];
				 if((int)Cc>255) Cc=255;
				 if((int)Cc<0) Cc=0;
				 Data[i*img2->widthStep +j*img2->nChannels +k] = Cc;
			}//end of k
		}//end of j
	}//end of i
	return img2;  
}

void Apply_Similarity_T(float input_x,float input_y, float MT[9], float &output_x, float &output_y)
{
	float M[3];
	float out_M[3];

	M[0] = input_x;
	M[1] = input_y;
	M[2] = 1.0f;

	//Matrix_Multiply_Ver2(MT,3,3,M,3,1,out_M);
	Matrix_Multiply(MT,3,3,M,3,1,out_M);

	output_x = out_M[0];
	output_y = out_M[1];
}

void Similarity_T(Loc *Src_pt, Loc *Tar_pt, int num, float MT[9])
{
	float *SrcMatrix;
	float *TarMatrix;
	float *InvSrcMatrix;
	int i;

	SrcMatrix = (float *)calloc(num*3,sizeof(float));
	TarMatrix = (float *)calloc(num*3,sizeof(float));
	InvSrcMatrix = (float *)calloc(num*3,sizeof(float));

	for (i = 0; i < num; i ++)
	{
		SrcMatrix[i] = Src_pt[i].x;//Tar_pt[i].x;//
		SrcMatrix[i+num] = Src_pt[i].y;//Tar_pt[i].y;//
		SrcMatrix[i+num*2] = 1.0f;

		TarMatrix[i] = Tar_pt[i].x;
		TarMatrix[i+num] = Tar_pt[i].y;
		TarMatrix[i+num*2] = 1.0f;
	}
	
	Matrix_Inverse2(SrcMatrix,InvSrcMatrix,3,num);
//	Matrix_Inverse(SrcMatrix,InvSrcMatrix,3,num);
//	Matrix_Multiply_Ver2(TarMatrix,3,num,InvSrcMatrix,num,3,MT);
	Matrix_Multiply(TarMatrix,3,num,InvSrcMatrix,num,3,MT);

	free(InvSrcMatrix);
	free(SrcMatrix);
	free(TarMatrix);
}


void FaceInformationExtraction(IplImage *GrayI, Mat_<double> current_shape,int landmark_number,
								  ExtractInforParams ExtractInfor, int type, 
								  vector <float> &FaceInforvector,Rect &AdjustmentR)
{
	int i,j;
	int ind;
	int Min_x,Min_y,Max_x,Max_y;
	Min_y = GrayI->height + 1;
	Min_x = GrayI->width + 1;
	Max_x = Max_y = -1;
	cv::Mat Resizegrayimage;
	cv::Mat Cropgrayimage;
	int Extraction_Width = ExtractInfor.Extraction_Width;
	int Extraction_Height = ExtractInfor.Extraction_Height;
	
	for (i = 0; i < landmark_number; i ++)
	{
		Min_x = clip(MIN2(Min_x,current_shape(i,0)),0,GrayI->width-1);
		Min_y = clip(MIN2(Min_y,current_shape(i,1)),0,GrayI->height-1);
		Max_x = clip(MAX2(Max_x,current_shape(i,0)),0,GrayI->width-1);
		Max_y = clip(MAX2(Max_y,current_shape(i,1)),0,GrayI->height-1);
	}

	//Rect ROI;
	AdjustmentR.x = Min_x;
	AdjustmentR.y = Min_y;
	AdjustmentR.width = Max_x - Min_x + 1;
	AdjustmentR.height = Max_y - Min_y + 1;

	if (type == 1)
	{
		cvSetImageROI(GrayI,AdjustmentR);
		IplImage *Crop = cvCreateImage(cvSize(AdjustmentR.width,AdjustmentR.height),GrayI->depth,GrayI->nChannels);
		cvCopy(GrayI,Crop, NULL);
		cvResetImageROI(GrayI);

		if (Extraction_Width <= 0)
			Extraction_Width = Crop->width;
		if (Extraction_Height <= 0)
			Extraction_Height = Crop->height;
		IplImage *CropResize = cvCreateImage(cvSize(Extraction_Width,Extraction_Height),GrayI->depth,GrayI->nChannels);
		cvResize(Crop,CropResize);

		ind = 0;
		BYTE *ptr = (BYTE *)CropResize->imageData;
		for (i = 0; i < CropResize->height; i ++)
		{
			for (j = 0; j < CropResize->width; j ++)
			{
				FaceInforvector.push_back((float)ptr[i * CropResize->widthStep + j * CropResize->nChannels]);
			}
		}

		//cvSaveImage("E:\\FaceLogoRecognition\\Data\\FacePlusPlus_Compare\\Debug.bmp",CropResize);

		cvReleaseImage(&Crop);
		cvReleaseImage(&CropResize);
	}

	if (type == 2)
	{
		Loc *CurrentPts;
		float MT[9];
		
		CurrentPts = (Loc *)calloc(landmark_number,sizeof(Loc));
		for (i = 0; i < landmark_number; i ++)
		{
			CurrentPts[i].x = current_shape(i,0);
			CurrentPts[i].y = current_shape(i,1);
		}
		Similarity_T(ExtractInfor.RefPts,CurrentPts,landmark_number,MT);

		//for (i = 0; i < 9; i ++)
		//	printf("%f ",MT[i]);

		IplImage* Transformed;
		Transformed = Bilinear_SimilarityT(GrayI,ExtractInfor.RefMaskImage->width,
			ExtractInfor.RefMaskImage->height,MT);
		//float Matching_x,Matching_y;
		//for (i = 0; i < ExtractInfor.RefMaskImage->height; i ++)
		//{
		//	for (j = 0; j < ExtractInfor.RefMaskImage->width; j ++)
		//	{
		//		Apply_Similarity_T(j,i,MT,Matching_x,Matching_y);

		//		cvSetReal2D(mapx, i, j, Matching_x); 
		//		cvSetReal2D(mapy, i, j, Matching_y); 
		//	}
		//}
		//IplImage* Transformed = cvCloneImage(ExtractInfor.RefMaskImage);
		//cvRemap( Transformed, GrayI, mapx, mapy );


		cvSetImageROI(Transformed,ExtractInfor.RefFaceROI);
		IplImage *Crop = cvCreateImage(cvSize(ExtractInfor.RefFaceROI.width,ExtractInfor.RefFaceROI.height),Transformed->depth,Transformed->nChannels);
		cvCopy(Transformed,Crop, NULL);
		cvResetImageROI(Transformed);

		if (Extraction_Width <= 0)
			Extraction_Width = Crop->width;
		if (Extraction_Height <= 0)
			Extraction_Height = Crop->height;
		IplImage *CropResize = cvCreateImage(cvSize(Extraction_Width,Extraction_Height),Transformed->depth,Transformed->nChannels);
		cvResize(Crop,CropResize);
		ind = 0;
		BYTE *ptr = (BYTE *)CropResize->imageData;
		for (i = 0; i < CropResize->height; i ++)
		{
			for (j = 0; j < CropResize->width; j ++)
			{
				FaceInforvector.push_back((float)ptr[i * CropResize->widthStep + j * CropResize->nChannels]);
			}
		}

		//cvSaveImage("E:\\FaceLogoRecognition\\Data\\FacePlusPlus_Compare\\Transformed.bmp", Transformed);
		//cvSaveImage("E:\\FaceLogoRecognition\\Data\\FacePlusPlus_Compare\\TransformedCropped.bmp", Crop);

		cvReleaseImage(&Crop);
		cvReleaseImage(&CropResize);
        cvReleaseImage(&Transformed);
        //cvReleaseImage(&mapx);
        //cvReleaseImage(&mapy); 
		free(CurrentPts);
	}
	
	if (type == 3)
	{
		Loc *CurrentPts;
		float MT[9];
//		IplImage* mapx = cvCreateImage( cvGetSize(ExtractInfor.RefMaskImage), IPL_DEPTH_32F, 1 );
//        IplImage* mapy = cvCreateImage( cvGetSize(ExtractInfor.RefMaskImage), IPL_DEPTH_32F, 1 ); 
		
		CurrentPts = (Loc *)calloc(landmark_number,sizeof(Loc));
		for (i = 0; i < landmark_number; i ++)
		{
			CurrentPts[i].x = current_shape(i,0);
			CurrentPts[i].y = current_shape(i,1);
		}
		Similarity_T(ExtractInfor.RefPts,CurrentPts,landmark_number,MT);

		IplImage* Transformed;
		Transformed = Bilinear_SimilarityT(GrayI,ExtractInfor.RefMaskImage->width,
			ExtractInfor.RefMaskImage->height,MT);

		cvSetImageROI(Transformed,ExtractInfor.RefFaceROI);
		IplImage *Crop = cvCreateImage(cvSize(ExtractInfor.RefFaceROI.width,ExtractInfor.RefFaceROI.height),Transformed->depth,Transformed->nChannels);
		cvCopy(Transformed,Crop, NULL);
		cvResetImageROI(Transformed);

		cvSetImageROI(ExtractInfor.RefMaskImage,ExtractInfor.RefFaceROI);
		IplImage *CropMask = cvCreateImage(cvSize(ExtractInfor.RefFaceROI.width,ExtractInfor.RefFaceROI.height),ExtractInfor.RefMaskImage->depth,ExtractInfor.RefMaskImage->nChannels);
		cvCopy(ExtractInfor.RefMaskImage,CropMask, NULL);
		cvResetImageROI(ExtractInfor.RefMaskImage);

		if (Extraction_Width <= 0)
			Extraction_Width = Crop->width;
		if (Extraction_Height <= 0)
			Extraction_Height = Crop->height;

		IplImage *CropResize = cvCreateImage(cvSize(Extraction_Width,Extraction_Height),Transformed->depth,Transformed->nChannels);
		cvResize(Crop,CropResize);

		IplImage *CropMaskResize = cvCreateImage(cvSize(Extraction_Width,Extraction_Height),ExtractInfor.RefMaskImage->depth,ExtractInfor.RefMaskImage->nChannels);
		cvResize(CropMask,CropMaskResize,CV_INTER_NN);
		//cvSaveImage("E:\\FaceLogoRecognition\\Data\\FacePlusPlus_Compare\\Mask_Debug.bmp", CropMaskResize);
		//cvSaveImage("E:\\FaceLogoRecognition\\Data\\FacePlusPlus_Compare\\Image_Debug.bmp", CropResize);

		ind = 0;
		BYTE *ptr = (BYTE *)CropResize->imageData;
		BYTE *mask_ptr = (BYTE *)CropMaskResize->imageData;
		for (i = 0; i < CropResize->height; i ++)
		{
			for (j = 0; j < CropResize->width; j ++)
			{
				if (mask_ptr[i * CropMaskResize->widthStep + j * CropMaskResize->nChannels] > 0)
					FaceInforvector.push_back((float)ptr[i * CropResize->widthStep + j * CropResize->nChannels]);
				else
					FaceInforvector.push_back(0);
			}
		}

		cvReleaseImage(&Crop);
		cvReleaseImage(&CropResize);
		cvReleaseImage(&CropMask);
		cvReleaseImage(&CropMaskResize);
        cvReleaseImage(&Transformed);
		free(CurrentPts);

	}
	return;	
}