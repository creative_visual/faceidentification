// DetParam.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceIdentification.h"
#include "DetParam.h"
#include "afxdialogex.h"

extern int g_min_face_size, g_max_face_size;
extern double g_zoomrate, g_scalefactor;
extern int g_minneighbors, g_detflag;

// CDetParam 对话框

IMPLEMENT_DYNAMIC(CDetParam, CDialog)

CDetParam::CDetParam(CWnd* pParent /*=NULL*/)
	: CDialog(CDetParam::IDD, pParent)
{

}

CDetParam::~CDetParam()
{
}

void CDetParam::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_MINSIZE, g_min_face_size);
	DDX_Text(pDX, IDC_MAXSIZE, g_max_face_size);
	DDX_Text(pDX, IDC_SCALEFACTOR, g_scalefactor);
	DDX_Text(pDX, IDC_MINNEIGHBORS, g_minneighbors);
	DDX_Text(pDX, IDC_DETFLAG, g_detflag);
}


BEGIN_MESSAGE_MAP(CDetParam, CDialog)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_OK, &CDetParam::OnBnClickedOk)
END_MESSAGE_MAP()


// CDetParam 消息处理程序

BOOL CDetParam::OnInitDialog()
{
	CDialog::OnInitDialog();

	UpdateData();
	UpdateData(FALSE);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CDetParam::OnPaint()
{
	CRect   rect;  
	CPaintDC   dc(this);  
	GetClientRect(rect);  
	dc.FillSolidRect(rect,RGB(240,240,240));   //设置为绿色背景
}

void CDetParam::OnBnClickedOk()
{
	UpdateData();
	UpdateData(FALSE);

	OnOK();
}
