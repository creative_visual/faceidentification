#pragma once


// CRecogParam 对话框

class CRecogParam : public CDialog
{
	DECLARE_DYNAMIC(CRecogParam)

public:
	CRecogParam(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRecogParam();

// 对话框数据
	enum { IDD = IDD_RECOGPARAM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeRecogalg();
	afx_msg void OnPaint();
	void ChangeCtrlWindow(int index);

private:
	RECT m_stlandmark, m_edlandmark;
public:
	afx_msg void OnBnClickedOk();
};
