// RecogParam.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceIdentification.h"
#include "RecogParam.h"
#include "afxdialogex.h"

extern int g_samplew, g_sampleh;
extern int g_ITERMAX, g_IntenalMAXIter;

// CRecogParam 对话框

IMPLEMENT_DYNAMIC(CRecogParam, CDialog)

CRecogParam::CRecogParam(CWnd* pParent /*=NULL*/)
	: CDialog(CRecogParam::IDD, pParent)
{

}

CRecogParam::~CRecogParam()
{

}

void CRecogParam::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDSAMPLEW, g_samplew);
	DDX_Text(pDX, IDC_EDSAMPLEH, g_sampleh);
	DDX_Text(pDX, IDC_EDITER, g_ITERMAX);
	DDX_Text(pDX, IDC_EDINITER, g_IntenalMAXIter);
}


BEGIN_MESSAGE_MAP(CRecogParam, CDialog)
	ON_WM_PAINT()
	ON_CBN_SELCHANGE(IDC_RECOGALG, &CRecogParam::OnCbnSelchangeRecogalg)
	ON_BN_CLICKED(IDC_OK, &CRecogParam::OnBnClickedOk)
END_MESSAGE_MAP()


BOOL CRecogParam::OnInitDialog()
{
	((CComboBox *)GetDlgItem(IDC_RECOGALG))->InsertString(0, "稀疏表示");
	((CComboBox *)GetDlgItem(IDC_RECOGALG))->InsertString(1, "高维LBP");

	GetDlgItem(IDC_STSAMPLEW)->GetWindowRect(&m_stlandmark);
	GetDlgItem(IDC_LANDMAKNUM)->GetWindowRect(&m_edlandmark);

	((CComboBox *)GetDlgItem(IDC_RECOGALG))->SetCurSel(0);
	ChangeCtrlWindow(0);

//	UpdateData();
	UpdateData(FALSE);

	return TRUE;
}

void CRecogParam::ChangeCtrlWindow(int index)
{
	if( index == 0 )
	{
		GetDlgItem(IDC_EDSAMPLEW)->ShowWindow(TRUE);
		GetDlgItem(IDC_EDSAMPLEH)->ShowWindow(TRUE);
		GetDlgItem(IDC_EDINITER)->ShowWindow(TRUE);
		GetDlgItem(IDC_EDITER)->ShowWindow(TRUE);
		GetDlgItem(IDC_STSAMPLEW)->ShowWindow(TRUE);
		GetDlgItem(IDC_STSAMPLEH)->ShowWindow(TRUE);
		GetDlgItem(IDC_STINITER)->ShowWindow(TRUE);
		GetDlgItem(IDC_STITER)->ShowWindow(TRUE);

		GetDlgItem(IDC_STLANDMARK)->ShowWindow(FALSE);
		GetDlgItem(IDC_LANDMAKNUM)->ShowWindow(FALSE);
	}
	else
	{
		RECT stlandmark,edlandmark, stsamplew, edsamplew;
		GetDlgItem(IDC_STLANDMARK)->GetWindowRect(&stlandmark);
		GetDlgItem(IDC_LANDMAKNUM)->GetWindowRect(&edlandmark);
		GetDlgItem(IDC_STSAMPLEW)->GetWindowRect(&stsamplew);
		GetDlgItem(IDC_EDSAMPLEW)->GetWindowRect(&edsamplew);

		GetDlgItem(IDC_EDSAMPLEW)->ShowWindow(FALSE);
		GetDlgItem(IDC_EDSAMPLEH)->ShowWindow(FALSE);
		GetDlgItem(IDC_EDINITER)->ShowWindow(FALSE);
		GetDlgItem(IDC_EDITER)->ShowWindow(FALSE);
		GetDlgItem(IDC_STSAMPLEW)->ShowWindow(FALSE);
		GetDlgItem(IDC_STSAMPLEH)->ShowWindow(FALSE);
		GetDlgItem(IDC_STINITER)->ShowWindow(FALSE);
		GetDlgItem(IDC_STITER)->ShowWindow(FALSE);

		int stw = m_stlandmark.right - m_stlandmark.left, sth = m_stlandmark.bottom - m_stlandmark.top;
		int edw = m_edlandmark.right - m_edlandmark.left, edh = m_edlandmark.bottom - m_edlandmark.top;
		stlandmark.left = 28, stlandmark.top = 55, stlandmark.right = stlandmark.left + stw, stlandmark.bottom = stlandmark.top + sth;
		edlandmark.left = 81, edlandmark.top = 50, edlandmark.right = edlandmark.left + edw, edlandmark.bottom = edlandmark.top + edh;

		GetDlgItem(IDC_STLANDMARK)->MoveWindow(&stlandmark);
		GetDlgItem(IDC_LANDMAKNUM)->MoveWindow(&edlandmark);

		GetDlgItem(IDC_STLANDMARK)->ShowWindow(TRUE);
		GetDlgItem(IDC_LANDMAKNUM)->ShowWindow(TRUE);
	}
}

void CRecogParam::OnCbnSelchangeRecogalg()
{
	int index = ((CComboBox *)GetDlgItem(IDC_RECOGALG))->GetCurSel();

	ChangeCtrlWindow(index);
}

void CRecogParam::OnPaint()
{
	CRect   rect;  
	CPaintDC   dc(this);  
	GetClientRect(rect);  
	dc.FillSolidRect(rect,RGB(240,240,240));   //设置为绿色背景
}

void CRecogParam::OnBnClickedOk()
{
	UpdateData();
	UpdateData(FALSE);

	OnOK();
}
