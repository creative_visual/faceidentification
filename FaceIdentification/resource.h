//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FaceIdentification.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FACEIDENTIFICATION_DIALOG   102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     131
#define IDD_DIALOG_DetectionQuery       132
#define IDD_DIALOG_DETECTIONQUERY       132
#define IDD_RECOGPARAM                  133
#define IDD_TESTDB                      134
#define IDD_DIALOG_SHOWPIC              135
#define IDD_DETPARAM                    157
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_COMBO1                      1003
#define IDC_RECOGALG                    1003
#define IDC_COMBORECOG                  1003
#define IDC_BUTTON4                     1004
#define IDC_BUTTON5                     1005
#define IDC_STATIC_BACKGROUND           1011
#define IDC_STATIC_DECT                 1012
#define IDC_STATIC_IDENT                1013
#define IDC_STATIC_STATIC               1014
#define IDC_STATIC_OPTION               1015
#define IDC_STATIC_START                1017
#define IDC_LISTBASE                    1018
#define IDC_LIST_DQ                     1019
#define IDC_LIST_DETE                   1020
#define IDC_MAXSIZE                     1020
#define IDC_SCALEFACTOR                 1021
#define IDC_MINNEIGHBORS                1022
#define IDC_DETFLAG                     1023
#define IDC_OK                          1024
#define IDC_EDSAMPLEW                   1025
#define IDC_EDSAMPLEH                   1026
#define IDC_STATIC_TITLE                1027
#define IDC_EDITER                      1028
#define IDC_LANDMAKNUM                  1030
#define IDC_STSAMPLEW                   1031
#define IDC_STSAMPLEH                   1032
#define IDC_STINITER                    1033
#define IDC_EDINITER                    1034
#define IDC_STITER                      1036
#define IDC_STLANDMARK                  1037
#define IDC_STATIC_PRO                  1040
#define IDC_ENAME                       1041
#define IDC_DBINFOGROUP                 1042
#define IDC_SID                         1043
#define IDC_STATIC_RATE                 1043
#define IDC_EID                         1044
#define IDC_STATIC_SHOW                 1044
#define IDC_ESEX                        1045
#define IDC_SCROLLBAR1                  1045
#define IDC_SNAME                       1046
#define IDC_SCROLLBAR2                  1046
#define IDC_SSEX                        1047
#define IDC_STATIC_TIP                  1047
#define IDC_EDIT2                       1048
#define IDC_EAGE                        1048
#define IDC_SAGE                        1049
#define IDC_MINSIZE                     1119
#define IDC_DBPATH                      1241
#define IDC_DBDIR                       1242
#define IDC_OUTPATH                     1243
#define IDC_OUTDIR                      1244

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1048
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
