#pragma once


// CDetParam 对话框

class CDetParam : public CDialog
{
	DECLARE_DYNAMIC(CDetParam)

public:
	CDetParam(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDetParam();

// 对话框数据
	enum { IDD = IDD_DETPARAM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnBnClickedOk();
};
