#pragma once


// CTestDB 对话框

class CTestDB : public CDialog
{
	DECLARE_DYNAMIC(CTestDB)

public:
	CTestDB(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CTestDB();
	void ShowRate(int i,int sum);
	void Finish();
// 对话框数据
	enum { IDD = IDD_TESTDB };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedDbdir();
	afx_msg void OnBnClickedOutdir();
	afx_msg void OnBnClickedCancel();
};
