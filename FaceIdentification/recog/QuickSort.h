#ifndef _QUICKSORT_H_
#define _QUICKSORT_H_

#include <stdio.h>

void quickSort(float *a, int *index,int l, int r);
int partition(float *a,int *index, int l, int r);


#endif