#ifndef _QRDECOMPOSITION_H_
#define _QRDECOMPOSITION_H_

//http://rosettacode.org/wiki/QR_decomposition

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
	int m, n;
	float **v;
} mat_t, mat;

mat *matrix_new(int m, int n);
void matrix_delete(mat *m);

void QR(mat *m, float *RM, float *QM);

int qrdcmp2(float *a, int m, int n, float *c, float *d);

mat *matrix_copy(float *a, int m, int n);
void matrix_show(mat *m);

void Dec(float *A, int m, int n, float *QR,float *RDiag, int p);
void solve(float *QR, float *b, float *solved_x,float *RDiag,int m,int n);
#endif