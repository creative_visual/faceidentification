#ifndef __TYPE_DEF_H__
#define __TYPE_DEF_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>

#define PI 3.14159265

//typedef unsigned short WORD;
//typedef unsigned short *PWORD;
//typedef unsigned long DWORD;
//typedef unsigned long LWORD;
//typedef unsigned long ULONG;
//#define INT8   __int8
//#define INT16  __int16
//#define INT32  __int32
//#define INT64  __int64
//#define UINT8  unsigned __int8
//#define UINT16 unsigned __int16
//#define UINT32 unsigned __int32
//#define UINT64 unsigned __int64
//#define BYTE   UINT8

#define clip(x,a,b) max(a,min(x,b))
#define DIFF(x, y) (((x)>(y))?((x)-(y)):((y)-(x)))
#define ABS(x) ((x<0)?-(x):(x))
#define MAXA(x,y) ((ABS(x)>ABS(y))?(x):(y))
#define MIN2(x, y) (((x)<(y))?(x):(y))
#define MAX2(x, y) (((x)>(y))?(x):(y))

#endif