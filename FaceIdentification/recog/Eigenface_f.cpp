#include "StdAfx.h"
#include <cmath>

#include "Eigenface_f.h"
#include "../Matrix.h"
// #include "Eigen_C_Code.h"
// #include "eispack.h"

void train_eigen(double *Tr_DAT, int feature_num, int train_num, int dim, double *Tr_DAT_NORM, double *disc_set_T, double *Proj_M)
{
	double *disc_set = new double[feature_num*dim];
	Eigenface_f(Tr_DAT, train_num, feature_num, dim, disc_set);

	//double *disc_set_T = new double[ dim*feature_num ];
	Matrix_Transpose(disc_set, disc_set_T, feature_num, dim);

	//double *tr_dat = new double[dim * train_num];
	Matrix_Multiply(disc_set_T, dim, feature_num, Tr_DAT, feature_num, train_num, Tr_DAT_NORM);

	Matrix_Norm_Col(Tr_DAT_NORM, train_num, dim);
	//save_matrix("tr_data_norm.txt", tr_dat, 700, 300);

	double *tr_dat_T = new double[ train_num * dim ];
	Matrix_Transpose(Tr_DAT_NORM, tr_dat_T, dim, train_num);

	double *tmp_tr_dat = new double[ train_num * train_num ];
	Matrix_Multiply(tr_dat_T, train_num, dim, Tr_DAT_NORM, dim, train_num, tmp_tr_dat);

	double kappa = 0.001;
	double *identity = eye(train_num);
	Matrix_Multiply(identity, train_num, train_num, kappa);

	double *M_sum = new double[ train_num * train_num ];
	Matrix_Sum(tmp_tr_dat, train_num, train_num, identity, M_sum);
	//save_matrix("sum.txt", M_sum, 700, 700);

	double *M_inv = new double[ train_num * train_num ];
	Matrix_Inverse(M_sum, M_inv, train_num, train_num);

	//save_matrix("inv.txt", M_inv, 700, 700);
	//double *Proj_M = new double[ train_num * dim ];
	Matrix_Multiply(M_inv, train_num, train_num, tr_dat_T, train_num, dim, Proj_M);

	delete[] disc_set;
	//delete[] disc_set_T;
	delete[] tmp_tr_dat;
	delete[] M_sum;
	delete[] M_inv;
}

int CRC_RLS(double *D, int D_width, int D_height, double *class_pinv_M, int M_width, int M_height, double *y, int y_len,
	int *Dlabels, vector<double> &error)
{
	error.clear();
	double *coef = new double[ M_height * 1 ];
	Matrix_Multiply(class_pinv_M, M_height, M_width, y, y_len, 1, coef);

	int i,j,k;
	int max_label = vec_max(Dlabels, D_width);

	//vector<double> error;
	for(i = 0; i <= max_label; i++)
	{
		vector<double> tmp;
		vector<int> index;
		for(j = 0; j < D_width; j++)
		{
			if( Dlabels[j] == i )
			{
				tmp.push_back( coef[ j ] );
				index.push_back( j );
			}
		}

		double *coef_c = new double[ tmp.size() ];
		for(k = 0; k < tmp.size(); k++)
			coef_c[ k ] = tmp[ k ];

		double *Dc = new double[ D_height * tmp.size() ];

		for(int ii = 0; ii < index.size(); ii++)
			for(int jj = 0; jj < D_height; jj++)
			{
				Dc[ jj * tmp.size() + ii ] = D[ jj * D_width + index[ii] ];
			}

		double *dco = new double[ D_height ];
		Matrix_Multiply(Dc, D_height, tmp.size(), coef_c, tmp.size(), 1, dco);

		double *diff = new double[ y_len ];
		vector_sub(y, dco, diff, y_len);

		double	nd = norm(diff, y_len, 2);

		double sum = 0;
		for(int ii = 0; ii < tmp.size(); ii++)
			sum += coef_c[ii]*coef_c[ii];

		double ci = nd * nd / sum;
		error.push_back(ci);

		delete[] Dc;
		delete[] dco;
		delete[] diff;
		delete[] coef_c;
	}

	delete[] coef;

	k = 0;
	for(i = 0; i < error.size(); i++)
	{
		if( error[i] < error[k] )
			k = i;
	}

	return k;
}

void Find_K_Max_Eigen(CvMat Matrix, double Eigen_NUM, CvMat Eigen_Vec, double *Eigen_Val, int vl)
{
	int w = Matrix.cols;

	double *c = new double[w];
	for(int i = 0; i < w; i++)
		c[i] = 0;

	double *vec = new double[ w*w ];

	CvMat ProVector = cvMat(w,w, CV_64FC1, vec);
	CvMat ProValue = cvMat(w,1, CV_64FC1, c);

	cvEigenVV(&Matrix, &ProVector,&ProValue,1.0e-6F);

// 	double **m = new double *[w];
// 	double **vc = new double *[w];
// 	for(int i = 0; i < w; i++)
// 	{
// 		m[i] = new double[w];
// 		vc[i] = new double[w];
// 	}

	double *a = new double[w*w];
	double *l = new double[w];

// 	for(int i = 0; i < Matrix.rows; i++)
// 		for(int j = 0; j < Matrix.cols; j++)
// 		{
// 			a[i*w + j] = Matrix.data.db[ i * w + j ];
// 		}

	//rs( w, Matrix.data.db, ProValue.data.db, 1, ProVector.data.db );
	//eigen(w, m, l, vc);
	//save_matrix("eV.txt", vc, w, w);
	//save_matrix("eV.txt",ProVector.data.db, w,w );

	double *tmp = new double[ w*w ];
	Matrix_Transpose(ProVector.data.db, tmp, w, w);
	//save_matrix("V.txt", ProVector.data.db, w,w);
	for(int i = 0; i < Eigen_NUM; i++)
	{
		Eigen_Val[i] = cvmGet(&ProValue, i, 0);

		for(int j = 0; j < w; j++)
			Eigen_Vec.data.db[ j*(int)Eigen_NUM + i ] = -1*tmp[ j*w + i ];
	}

	delete[] tmp;
}

void Eigenface_f(double *Train_SET, int w, int h, int Eigen_NUM, double *disc_set)
{
	int NN = h, Train_NUM = w;

	int i,j,k;

	double *train_set = new double[ w * h ];
	memcpy(train_set, Train_SET, w * h * sizeof(double));

	double *Mean_Image = new double[h];
	for(i = 0; i < h; i++)
	{
		double sum = 0;
		for(j = 0; j < w; j++)
			sum += train_set[ i * w + j ];

		Mean_Image[i] = sum / w;
	}

	for(j = 0; j < w; j++)
		for(i = 0; i < h; i++)
		{
			train_set[ i * w + j ] -= Mean_Image[i];
		}

	double *Train_SET_T = new double[ w * h ];
	Matrix_Transpose(train_set, Train_SET_T, h, w);

	if( NN <= Train_NUM )
	{
		double *R = new double[ h * h ];
		Matrix_Multiply(train_set, h, w, Train_SET_T, w, h, R);

		for(i = 0; i < h*h; i++)
			R[i] = R[i] / (Train_NUM-1);

		double *vec = new double[ h*Eigen_NUM ];

		CvMat train_data = cvMat(h, h, CV_64FC1, R);
		CvMat Eigen_Vec = cvMat(h, Eigen_NUM, CV_64FC1, vec);
		double *Eigen_Val = new double[ Eigen_NUM ];

		Find_K_Max_Eigen(train_data, Eigen_NUM, Eigen_Vec, Eigen_Val, Eigen_NUM);

		memset(disc_set, 0, h * Eigen_NUM *sizeof(double));

		for(i = 0; i < w*h; i++)
			train_set[i] = train_set[i] / sqrt(Train_NUM-1.0);

		for(k = 0; k < Eigen_NUM; k++)
		{
			double *a = new double[h];
			double *col = new double[Train_NUM];

			for(j = 0; j < h; j++)
				col[j] = Eigen_Vec.data.db[ j * Eigen_NUM + k ];

			Matrix_Multiply(train_set, h, w, col, Train_NUM, 1, a);

			double b = 1.0 / sqrt(Eigen_Val[k]);

			for(i = 0; i < h; i++)
				disc_set[ i * Eigen_NUM + k ] = a[i]*b;
		}
	}
	else   // NN > Train_NUM
	{
		double *R = new double[ w * w ];
		Matrix_Multiply(Train_SET_T, w, h, train_set, h, w, R);

		for(i = 0; i < w*w; i++)
			R[i] = R[i] / (Train_NUM-1);

		double *vec = new double[ Train_NUM*Eigen_NUM ];

		CvMat train_data = cvMat(w, w, CV_64FC1, R);
		CvMat Eigen_Vec = cvMat(Train_NUM, Eigen_NUM, CV_64FC1, vec);
		double *Eigen_Val = new double[ Eigen_NUM ];

		Find_K_Max_Eigen(train_data, Eigen_NUM, Eigen_Vec, Eigen_Val, Eigen_NUM);

		//save_matrix("val.txt", Eigen_Val, 1, Eigen_NUM);
		//double *disc_set = new double[ h * Eigen_NUM ];
		memset(disc_set, 0, h * Eigen_NUM *sizeof(double));

		for(i = 0; i < w*h; i++)
			train_set[i] = train_set[i] / sqrt(Train_NUM-1.0);

		//save_matrix("Train_SET.txt", Train_SET, w, h);
		//save_matrix("V.txt", Eigen_Vec.data.db, Eigen_NUM, Train_NUM);

		for(k = 0; k < Eigen_NUM; k++)
		{
			double *a = new double[h];
			double *col = new double[Train_NUM];

			for(j = 0; j < Train_NUM; j++)
				col[j] = Eigen_Vec.data.db[ j * Eigen_NUM + k ];

			Matrix_Multiply(train_set, h, w, col, Train_NUM, 1, a);

			double b = 1.0 / sqrt(Eigen_Val[k]);

			for(i = 0; i < h; i++)
				disc_set[ i * Eigen_NUM + k ] = a[i]*b;
		}
	}

	delete[] train_set;
	delete[] Mean_Image;
}

void Matrix_Inner_Product(double *M, double *M2, int width, int height, double *output)
{
	int i,j;
	for(i = 0; i < height; i++)
		for(j = 0; j < width; j++)
		{
			output[ i * width + j ] = M[i * width + j] * M2[i * width + j];
		}
}

void Matrix_Sum_Col(double *M, int width, int height, double *output)
{
	int i,j;
	for(j = 0; j < width; j++)
	{
		double sum = 0;
		for(i = 0; i < height; i++)
		{
			sum += M[ i * width + j ];
		}
		
		output[j] = sum;
	}
}

void Matrix_Sum(double *M, int width, int height, double *M2, double *output)
{
	int i,j;

	for(i = 0; i < height; i++)
		for(j = 0; j < width; j++)
		{
			output[ i * width + j ] = M[ i * width + j ] + M2[ i * width + j ];
		}
}

void Matrix_Sqrt(double *M, int width, int height)
{
	int i,j;

	for(i = 0; i < height; i++)
		for(j = 0; j < width; j++)
		{
			M[ i * width + j ] = sqrt( M[i * width + j] );
		}
}

void Vector_Sqrt(double *vec, int length)
{
	int i;
	for(i = 0; i < length; i++)
	{
		vec[ i ] = sqrt( vec[i] );
	}
}

void Matrix_Norm_Col(double *M, int width, int height)
{
	int i,j;

	double *M3 = new double[ width * height ];
	Matrix_Inner_Product(M, M, width, height, M3);

	double *vec = new double[ width ];
	Matrix_Sum_Col(M3, width, height, vec);

	Vector_Sqrt(vec, width);

	for(j = 0; j < width; j++)
		for(i = 0; i < height; i++)
		{
			M[ i * width + j ] = M[ i * width + j ] / vec[ j ];
		}

	delete[] M3;
	delete[] vec;
}

double *eye(int n)
{
	int i;
	double *M = new double[ n * n ];
	memset(M, 0, n * n * sizeof(double));

	for(i = 0; i < n; i++)
		M[ i * n + i ] = 1;

	return M;
}

void vector_sub(double *vec, double *vec2, double *output, int len)
{
	int i;
	for(i = 0; i < len; i++)
		output[i] = vec[i] - vec2[i];
}

double norm(double *vec, int len, int n)
{
	double sum = 0;

	int i;
	for(i = 0; i < len; i++)
	{
		sum += pow( vec[i], n );
	}

	sum = pow(sum, 1.0 / n);

	return sum;
}

double vec_sum(double *vec, int len)
{
	int i;
	double sum=  0;
	for(i = 0; i < len; i++)
		sum += vec[i];

	return sum;
}