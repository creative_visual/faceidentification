#include "StdAfx.h"
#include <stdio.h>
#include "QuickSort.h"

//void main() 
//{
//	int a[] = { 7, 12, 1, -2, 0, 15, 4, 11, 9};
//
//	int i;
//	printf("\n\nUnsorted array is:  ");
//	for(i = 0; i < 9; ++i)
//		printf(" %d ", a[i]);
//
//	quickSort( a, 0, 8);
//
//	printf("\n\nSorted array is:  ");
//	for(i = 0; i < 9; ++i)
//		printf(" %d ", a[i]);
//
//}

void quickSort(float *a, int *index,int l, int r)
{
	int j;

	if( l < r ) 
	{
		// divide and conquer
		j = partition(a, index,l, r);
		quickSort(a, index,l, j-1);
		quickSort(a, index,j+1, r);
	}
}



int partition(float *a,int *index, int l, int r) 
{
	float pivot,t;
	int i, j,t1;

	pivot = a[l];
	i = l; j = r+1;
		
	while(1)
	{
		do ++i; while( a[i] <= pivot && i <= r );
		do --j; while( a[j] > pivot );
		if( i >= j ) break;
		t = a[i]; a[i] = a[j]; a[j] = t;
		t1 = index[i]; index[i] = index[j]; index[j] = t1;
	}
	t = a[l]; a[l] = a[j]; a[j] = t;
	t1 = index[l]; index[l] = index[j]; index[j] = t1;

	return j;
}




