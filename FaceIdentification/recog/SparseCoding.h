#ifndef _SPARSECODING_H_
#define _SPARSECODING_H_

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "typedef.h"

using namespace std;

void ls_featuresign_sub(float *A, float *Y_i, float *ATA, float *ATY_i, float gamma, int Feature_Number, 
						int Training_Number, int Test_Number, float *Xout_i, float &fObj);
void  fobj_featuresign(float *x, float *A, float *y, float *AtA, float *Aty, 
					   int Feature_number, int Training_number, float gamma,float &fobj);


void l1ls_featuresign(float *A, float *AT, float *ATA, int Feature_number, int Training_number, float *Y, 
					  int Test_number,float gamma, float *Xout, int &recog_idx);

void compute_FS_step(float *x, float *A, float *y, float *AtA, float *Aty, float *theta,
					 int Feature_Number, int Training_Number,
					 float *act, vector <int> &act_indx1, float gamma,
					 bool &optimality1, float &lsearch, float &fObj);

#endif