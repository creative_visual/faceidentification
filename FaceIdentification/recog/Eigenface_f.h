#ifndef _EIGENFACE_F_H_
#define _EIGENFACE_F_H_
#include <io.h>
#include <iomanip>
#include <opencv2/opencv.hpp>
#include <fstream>

#define MAX_ITERATION_COUNT 30

using namespace cv;
using namespace std;

//#pragma comment(lib, "RecogDLL.lib")

//_declspec(dllexport) void train_eigen(double *Tr_DAT, int feature_num, int train_num, int dim, double *Tr_DAT_NORM, double *disc_set_T, double *Proj_M);
//_declspec(dllexport) int CRC_RLS(double *D, int D_width, int D_height, double *class_pinv_M, int M_width, int M_height, double *y, int y_len, int *Dlabels);

void Find_K_Max_Eigen(double *Matrix, int w, int h, double Eigen_NUM, double *Eigen_Vec, int ew, int eh, double *Eigen_Val, int vl);
void train_eigen(double *Tr_DAT, int feature_num, int train_num, int dim, double *Tr_DAT_NORM, double *disc_set_T, double *Proj_M);

void Eigenface_f(double *Train_SET, int w, int h, int Eigen_NUM, double *disc_set);
void Matrix_Inner_Product(double *M, double *M2, int width, int height, double *output);
void Matrix_Sum_Col(double *M, int width, int height, double *output);
void Matrix_Sum(double *M, int width, int height, double *M2, double *output);
void Matrix_Sqrt(double *M, int width, int height);
void Matrix_Norm_Col(double *M, int width, int height);
double *eye(int n);

template<class T>
double vec_max(T *arr, int length)
{
	int i,idx = 0;
	for(i = 1; i < length; i++)
	{
		if( arr[i] > arr[idx] )
			idx = i;
	}

	return arr[idx];
}

int CRC_RLS(double *D, int D_width, int D_height, double *class_pinv_M, int M_width, int M_height, double *y, int y_len,
	int *Dlabels, vector<double> &error);

double norm(double *vec, int len, int n);
void vector_sub(double *vec, double *vec2, double *output, int len);
void train(double *Tr_DAT, int feature_num, int train_num, int dim, double *Tr_DAT_NORM, double *disc_set_T, double *Proj_M);
#endif