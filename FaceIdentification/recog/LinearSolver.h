#ifndef _LINEARSOLVER_H_
#define _LINEARSOLVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "QR_Decomposition.h"

void LinearSolver(float *A, int A_rows, int A_cols, float *B, float *x);
#endif