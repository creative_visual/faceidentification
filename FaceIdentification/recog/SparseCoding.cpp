#include "StdAfx.h"
#include "SparseCoding.h"
#include "..\Matrix.h"
#include "LinearSolver.h"
#include "QuickSort.h"

using namespace std;

extern int g_ITERMAX;//1000;
extern int g_IntenalMAXIter;

float sign(float data)
{
	if (data == 0)
		return 0;
	if (data < 0)
		return -1;
	if (data > 0)
		return 1;
}

/************************************
Correspond to:
Xout = l1ls_featuresign (A, Y, gamma, Xinit)
But we can Remove 'Xinit' since we donot use it
A:  Feature_number * Training_number
Y:  Feature_number * Test_number
************************************/

void l1ls_featuresign(float *A, float *AT, float *ATA, int Feature_number, int Training_number, float *Y, 
					  int Test_number,float gamma, float *Xout, int &recog_idx)
{
	int i,j;
	for (i = 0; i < (Training_number*Test_number); i ++)
		Xout[i] = 0;

	// AT Corresponding to A' in MATLAB
//	float *AT = (float *)calloc(Feature_number*Training_number,sizeof(float));
//	float *ATA = (float *)calloc(Training_number*Training_number,sizeof(float));
	float *ATY = (float *)calloc(Training_number*Test_number,sizeof(float));
// 	Matrix_Transpose(A,AT,Feature_number,Training_number);
// 	Matrix_Multiply(AT,Training_number,Feature_number,A,Feature_number,Training_number,ATA);
	Matrix_Multiply(AT,Training_number,Feature_number,Y,Feature_number,Test_number,ATY);

	//for (int mm =0; mm < Training_number; mm ++)
	//{
	//	for (int nn = 0; nn < Test_number; nn ++)
	//	{
	//		printf("%f ",ATY[mm * Test_number + nn]);
	//	}
	//	printf("\n");
	//}

	//The current MATLAB code seems to process each test image one by one, not simultaneously
	//We follow the same way here.
	//The implementation can be optimized later

	//And since no "init', use the code from '[Xout(:,i), fobj]= ls_featuresign_sub (A, Y(:,i), AtA, AtY(:, i), gamma);' directly
	//Ignore 'rankA' and codes from Line 34~38 in MATLAB
	//These 3 variables are used to store i-th column data from Test images (ith Test image's feature) and output
	float *Y_i = (float *)calloc(Feature_number,sizeof(float));
	float *ATY_i = (float *)calloc(Training_number,sizeof(float));
	float *Xout_i = (float *)calloc(Training_number,sizeof(float));
	float fObj; //Seem not be used, but keep here;
	for (i = 0; i < Test_number; i ++)
	{
		//Extract the ith test image feature
		for (j = 0; j < Feature_number; j ++)
		{
			Y_i[j] = Y[j * Test_number + i];
		}
		for (j = 0; j < Training_number; j ++)
		{
			ATY_i[j] = ATY[j * Test_number + i];
		}
		ls_featuresign_sub(A,Y_i,ATA,ATY_i,gamma,Feature_number,Training_number,Test_number,Xout_i,fObj);
		//Store output vector into output matrix

			float MAXVAL = -1;
			int maxind = -1;
			for (j = 0; j < Training_number; j ++)
			{
				//printf("%f ",Xout_i[j]);
				if (ABS(Xout_i[j]) > MAXVAL)
				{
					MAXVAL = ABS(Xout_i[j]);
					maxind = j;
				}
			}
		
		//printf("\n maxind = %d MaxValue = %f \n",maxind,MAXVAL);
		for (j = 0; j < Training_number; j ++)
			Xout[j * Test_number + i] = Xout_i[j];

		recog_idx = maxind;
	}
	free(Y_i);
	free(ATY_i);
	free(Xout_i);

// 	free(AT);
// 	free(ATA);
	free(ATY);
}

/***********************************
The Following Function matches the function in MATLAB;
[Xout(:,i), fobj]= ls_featuresign_sub (A, Y(:,i), AtA, AtY(:, i), gamma);
************************************/
void ls_featuresign_sub(float *A, float *Y_i, float *ATA, float *ATY_i, float gamma, int Feature_Number, 
						int Training_Number, int Test_Number, float *x, float &fobj)
{
	//Function Main Body
	int i;
	int iter;
	int L = Feature_Number;
	int M = Training_Number;
	int rankA = MIN2(L-10,M-10);

	//Step1 : Initialization
	bool usexinit = false;
	bool allowZero = false;
	float *theta = (float *)calloc(M,sizeof(float));
	float *act = (float *)calloc(M,sizeof(float));
	//'x' in MATLAB corresponds to Xout_i;
	for (i = 0; i < M; i ++)
		x[i] = theta[i] = act[i] = 0;

	float *grad = (float *)calloc(M,sizeof(float));
	float *ATA_X = (float *)calloc(Training_Number,sizeof(float));

	fobj = 0; //fobj_featuresign(x, A, y, AtA, Aty, gamma);
	g_ITERMAX=10;//1000;
	g_IntenalMAXIter = 100;//
	bool optimality1=false;

	for (iter = 1; iter <= g_ITERMAX; iter ++)
	{
		printf("Iter = %d \n",iter);
		if (iter == 111)
			printf("mmm\n");
//		if ((iter % 100) == 0)	
//			printf("Iter = %d \n",iter);
	    // check optimality0
		//Correspond to : act_indx0 = find(act == 0);
		vector <int> act_indx0;
		act_indx0.clear();
		for (i = 0; i < M; i ++)
		{
			if (act[i] == 0) 
				act_indx0.push_back(i);
		}
		//Corresonding to :
		//grad = AtA*sparse(x) - Aty;
		//theta = sign(x);
		Matrix_Multiply(ATA,Training_Number,Training_Number,x,Training_Number,1,ATA_X);
		for (i = 0; i < Training_Number; i ++)
		{
			grad[i] = ATA_X[i] - ATY_i[i];
			theta[i] = sign(x[i]);
		}
		bool optimality0= false;

		//Step 2:
		//Corresponds to  [mx,indx] = max (abs(grad(act_indx0)));
		vector <int> indx;
		int mx_ind;
		float mx = -1;
		indx.clear();
		for (i = 0; i < act_indx0.size(); i ++)
		{
			if (ABS(grad[act_indx0[i]]) > mx)
			{
				mx = ABS(grad[act_indx0[i]]);
				mx_ind = i;
			}
		}
		indx.push_back(mx_ind);
		//for (i = 0; i < act_indx0.size(); i ++)
		//	mx = MAX2(mx,ABS(grad[act_indx0[i]]));
		//for (i = 0; i < act_indx0.size(); i ++)
		//{
		//	if (mx == ABS(grad[act_indx0[i]])) indx.push_back(act_indx0[i]);
		//}


		//Corresponds to Lines 91~100
		if ((!(mx==-1))&&(mx>gamma)&&(iter>1||!usexinit))
		{
			for (i = 0; i < indx.size(); i ++)
			{
				act[act_indx0[indx[i]]] = 1;
				theta[act_indx0[indx[i]]] = -1 * sign(grad[act_indx0[indx[i]]]);
				usexinit = false;
			}
		}else
		{
			optimality0= true;
			if (optimality1)
	            break;
		}
		//Corresponds to act_indx1 = find(act == 1);
		vector <int> act_indx1;
		act_indx1.clear();
		for (i = 0; i < M; i ++)
		{
			if (act[i] == 1.0f) 
				act_indx1.push_back(i);
		}
		if ((int)(act_indx1.size()) > rankA)
		{
			printf("WARNING: sparsity penalty is too small: too many coefficients are activated.\n");
			return;
		}
		if (act_indx1.size() == 0)
		{
			if (allowZero) //Have some questions ???:  if allowZero, allowZero= false; continue, end
			{
				allowZero = false;
				continue;
			}
			return;
		}
		
		int k = 0;
		while (1)
		{
			k = k + 1;
			if (k > g_IntenalMAXIter)
//			if (k > ITERMAX)
			{
				printf("WARNING: Maximum number of iteration reached. The solution may not be optimal.\n");
				return;
			}
			if (act_indx1.size() == 0) //if allowZero, allowZero= false; break, end
			{
				if (allowZero)
				{
					allowZero = true;
					allowZero = false;
					break;
				}
				return;
			}

			//Step 3: feature-sign step ----> Call compute_FS_step Function
			float lsearch = 0; //  output from computer_FS_step function
			compute_FS_step(x,A,Y_i,ATA,ATY_i,theta,Feature_Number,Training_Number,
				act,act_indx1,gamma,optimality1,lsearch,fobj);
			
			if (iter == 74)
			{
				printf("================\n");
				for (int mm = 0; mm < Training_Number; mm ++)
				{
					if (x[mm] != 0)
						printf("[%d]%f ",mm,x[mm]);
				}
				printf("\n");
				printf("================\n");
			}
			//Step 4: check optimality condition 1
			if (optimality1)
				break;
			if (lsearch >0)
				continue;
		}//End of While(1)
	}//End of (iter ......)
	

	if (iter > g_ITERMAX)
	{
		printf("WARNING: Maximum number of iteration reached. The solution may not be optimal \n");
		return;
	}

	//Skip Line 146~151, since the condition in MATLAB is "if 0"
		
	//Corresponds to: fobj = fobj_featuresign(x, A, y, AtA, Aty, gamma);
	fobj_featuresign(x, A, Y_i, ATA, ATY_i, Feature_Number,Training_Number,gamma,fobj);

	free(theta);
	free(act);
	free(grad);
	free(ATA_X);

	return;
}

/***********************************
The Following Function matches the function in MATLAB;
function [x, theta, act, act_indx1, optimality1, lsearch, fobj] = compute_FS_step (x, A, y, AtA, Aty, theta, act, act_indx1, gamma);
************************************/
void compute_FS_step(float *x, float *A, float *y, float *AtA, float *Aty, float *theta,
					 int Feature_Number, int Training_Number,
					 float *act, vector <int> &act_indx1, float gamma,
					 bool &optimality1, float &lsearch, float &fObj)
{
	int i,j;
	float *x2,*ATA2,*Theta2;
	float *Aty2;
	float *y_temp;

	int act_indx1_size = act_indx1.size();
	x2 = (float *)calloc(act_indx1_size,sizeof(float));
	ATA2 = (float *)calloc(act_indx1_size*act_indx1_size,sizeof(float));
	Theta2 = (float *)calloc(act_indx1_size,sizeof(float));
	y_temp = (float *)calloc(act_indx1_size,sizeof(float));
	Aty2 = (float *)calloc(act_indx1_size,sizeof(float));
	for (i = 0; i < act_indx1_size; i ++)
	{
		x2[i] = x[act_indx1[i]];
		Theta2[i] = theta[act_indx1[i]];
		Aty2[i] = Aty[act_indx1[i]];
		for (j = 0; j < act_indx1_size; j ++)
			ATA2[i*act_indx1_size+j] = AtA[act_indx1[i] * Training_Number +act_indx1[j]];

		y_temp[i] = Aty2[i] - gamma * Theta2[i];
	}


	//Corresponds to:
	//x_new = AtA2 \ ( Aty(act_indx1) - gamma.*theta2 ); % RR
	//Solve:  AX = B
	float *x_new;
	x_new = (float *)calloc(act_indx1_size,sizeof(float));
	LinearSolver(ATA2,act_indx1_size,act_indx1_size,y_temp,x_new);
	free(y_temp);

	//Corresponds to: Line 170~181
	optimality1 = false;
	int SignEqualNumber = 0;
	for (i = 0; i < act_indx1_size; i ++)
	{
		if (sign(x_new[i]) == sign(x2[i]))
			SignEqualNumber ++;
	}
	if (SignEqualNumber == act_indx1_size)
	{
		optimality1= true;
		for (i = 0; i < act_indx1_size; i ++)
			x[act_indx1[i]] = x_new[i];
		fObj = 0;
		lsearch = 1;

		free(x_new);
		free(x2);
		free(ATA2);
		free(Theta2);
		free(Aty2);
		return;
	}

	lsearch = 0;
	float *progress = (float *)calloc(act_indx1_size+1,sizeof(float)); // +1 since Line 195: sort([progress',1]);
	float *x_new_x2 = (float *)calloc(act_indx1_size,sizeof(float));
	for (i = 0; i < act_indx1_size; i ++)
	{
		x_new_x2[i] = x_new[i] - x2[i];
		//if (x_new_x2[i] <= 0.001)
		//{
		//	if (x_new_x2[i] < 0)
		//		progress[i] = (0 - x2[i])/0.001 * (-1);
		//	else
		//		progress[i] = (0 - x2[i])/0.001 * (1);
		//}
		if (x_new_x2[i] == 0)
			progress[i] = (0 - x2[i])/1;
		else
			progress[i] = (0 - x2[i])/x_new_x2[i];
	}
	progress[act_indx1_size] = 1;

	float a,b;
	//Corresponds to:a = 0.5 *((x_new- x2)'*AtA2*(x_new- x2));
	float *a1 = (float *)calloc(act_indx1_size,sizeof(float));
	float *a2 = (float *)calloc(1,sizeof(float));
	Matrix_Multiply(x_new_x2,1,act_indx1_size,ATA2,act_indx1_size,act_indx1_size,a1);
	Matrix_Multiply(a1,1,act_indx1_size,x_new_x2,act_indx1_size,1,a2);
	a = 0.5*a2[0];
	free(a1);
	free(a2);
	//Corresponds to:b= (x2'*AtA2*(x_new- x2) - (x_new- x2)'*Aty(act_indx1));
	float *b1 = (float *)calloc(act_indx1_size,sizeof(float));
	float *b2 = (float *)calloc(1,sizeof(float));
	float *b3 = (float *)calloc(1,sizeof(float));
	Matrix_Multiply(x2,1,act_indx1_size,ATA2,act_indx1_size,act_indx1_size,b1);
	Matrix_Multiply(b1,1,act_indx1_size,x_new_x2,act_indx1_size,1,b2);
	Matrix_Multiply(x_new_x2,1,act_indx1_size,Aty2,act_indx1_size,1,b3);
	b = b2[0] - b3[0];
	free(b1);
	free(b2);
	free(b3);
	//Corresponds to:fobj_lsearch = gamma*sum(abs(x2));
	float sum = 0;
	float fobj_lsearch;
	for (i = 0; i < act_indx1_size; i ++)
		sum += ABS(x2[i]);
	fobj_lsearch = gamma * sum;

	//Corresponds to:[sort_lsearch, ix_lsearch] = sort([progress',1]);
	int *ix_lsearch = (int *)calloc(act_indx1_size+1,sizeof(int));
	for (i = 0;i < (act_indx1_size+1); i ++)
		ix_lsearch[i] = i;
	quickSort(progress,ix_lsearch,0,act_indx1_size);
	float *sort_lsearch = (float *)calloc(act_indx1_size+1,sizeof(float));
	memcpy(sort_lsearch,progress,(act_indx1_size+1)*sizeof(float));
	free(progress);

	//Corresponds to MATLAB Code 196~214
	vector <int> remove_idx;
	remove_idx.clear();
	for (i = 0; i < (act_indx1_size+1); i ++)
	{
		float t = sort_lsearch[i];
		if ((t <= 0) || (t > 1))
			continue;
		float *s_temp = (float *)calloc(act_indx1_size,sizeof(float));
		float s_temp_sum = 0;
		for (j = 0; j < act_indx1_size; j ++)
		{
			s_temp[j] = x2[j] + (x_new[j] - x2[j]) * t;
			s_temp_sum += ABS(s_temp[j]);
		}
		float fobj_temp;
		fobj_temp = a * t * t + b * t + gamma * s_temp_sum;
		if (fobj_temp < fobj_lsearch)
		{
			fobj_lsearch = fobj_temp;
			lsearch = t;
			if (t < 1)
				remove_idx.push_back(ix_lsearch[i]);
		}else if (fobj_temp > fobj_lsearch)
		{
			break;
		}else
		{
			int x2_zero_number = 0;
			for (j = 0; j < act_indx1_size; j ++)
			{
				if (x2[j] == 0)
					x2_zero_number ++;
			}
			if (x2_zero_number == 0)
			{
				fobj_lsearch = fobj_temp;
				lsearch = t;
				if (t < 1)
					remove_idx.push_back(ix_lsearch[i]);
			}
		}
		free(s_temp);
	}

	//Corresponds to MATLAB Code: Line 218 ~223
	//Update x and Theta	
	if (lsearch > 0)
	{
		for (i = 0; i < act_indx1_size; i ++)
		{
			x_new[i] = x2[i] + (x_new[i] - x2[i])*lsearch;
			x[act_indx1[i]] = x_new[i];
			theta[act_indx1[i]] = sign(x_new[i]);
		}
	}

	//Corresponds to MATLAB Code: Line 227 ~ 235
	//if x encounters zero along the line search, then remove it from
	//active set
	if ((lsearch < 1) && (lsearch > 0))
	{
		remove_idx.clear();
		for (i = 0; i < act_indx1_size; i ++)
		{
			if (ABS(act_indx1[i]) < 2.2204e-016) //2.2204e-016 --> eps
			{
				remove_idx.push_back(i);
				x[act_indx1[i]] = 0;
				theta[act_indx1[i]] = 0;
				act[act_indx1[i]] = 0;
			}
		}

		for (i = remove_idx.size()-1; i >=0 ; i --)
			act_indx1.erase(act_indx1.begin() + remove_idx[i]);
	}

	float fobj_new = 0; //fobj_featuresign(x, A, y, AtA, Aty, gamma);
	fObj = fobj_new;

	free(x_new);
	free(x_new_x2);
	free(ix_lsearch);
	free(sort_lsearch);
	free(Aty2);
	free(x2);
	free(ATA2);
	free(Theta2);

}



/***********************************
Corresponds to Function fobj_featuresign
In MATLAB, it can include 1 or 2 outputs
In the current implementation, we only consider 1 output, so only implement
 'f= 0.5*norm(y-A*x)^2;
 AND 'f= f+ gamma*norm(x,1);'
 We can extend it to include 2 outputs later.
 ************************************/
void  fobj_featuresign(float *x, float *A, float *y, float *AtA, float *Aty, 
					   int Feature_number, int Training_number, float gamma,float &fobj)
{
	float norm1,norm2;
	int i;
	float *Ax = (float *)calloc(Feature_number,sizeof(float));
	norm1 = 0; // Corresponds to norm(y-A*x)^2
	norm2 = 0; // Correponds to norm(x,1)

	Matrix_Multiply(A,Feature_number,Training_number,x,Training_number,1,Ax);
	for (i = 0; i < Feature_number; i ++)
	{
		norm1 += (y[i] - Ax[i])*(y[i] - Ax[i]);
		norm2 += ABS(x[i]);
	}

	fobj = 0.5f * norm1;
	fobj = fobj + gamma * norm2;
	free(Ax);
}