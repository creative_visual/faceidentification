#include "stdafx.h"
#include <io.h>
#include "ProcessRecog.h"
#include "SparseCoding.h"

#include "../common.h"
#include "../Matrix.h"
#include "FaceDetector.h"
#include "LBPFeatureExtractor.h"
#include "FaceNormalizer.h"
#include "sirius_util.h"
#include "../DetectionAlignment/Tracker.h"
#include "../DetectionAlignment/FaceAlignment.h"
#include "../DetectionAlignment/FaceInformationExtraction.h"


const int notFace = 0.5;
/*const*/ vector<int>scales(5);//  = {300, 212, 150, 106, 75};
//scales.resize(5);

const int patchSize = 10;
const int gridNumX = 4;
const int gridNumY = 4;
const int lbpDim = 59;
const int landmarkNum = 16;//16
const double eyeDistanceX = 2;
const double eyeDistanceUp = 1.5;
const double eyeDistanceDown = 2.5;
int totalDim = scales.size()*gridNumX*gridNumY*lbpDim*(landmarkNum);

int g_min_face_size = 40, g_max_face_size = 300;
double g_zoomrate = 1, g_scalefactor = 1.1;
int g_minneighbors = 3, g_detflag = 0;
int g_nsamples = 0;

vector<int> g_class_flag;
int g_samplew = 30, g_sampleh = 30;
int g_ITERMAX = 20, g_IntenalMAXIter = 100;
float *g_dict = NULL, *g_AT = NULL, *g_ATA = NULL;
vector<string> g_face_names, g_dirpathes, g_attention_names, g_attention_pathes;
vector<vector<int>> g_recog_flag;

int **g_base_features, g_last_db_size = 0, g_del_idx;
vector<int> g_det_db_flag;
vector<vector<int>> g_tmp_bfeatures; // lbp
vector<vector<float>> g_sp_bfeatures;// sparse_coding
HANDLE g_init_model_hdl, g_create_lbp_hdl, g_create_sp_hdl, g_extract_lbps_hdl, g_extract_sps_hdl;
vector<HANDLE> g_extract_feature_hdls;
vector<string> g_undet_pathes;
CRITICAL_SECTION g_csf;
extern HWND g_dlg;

ShapeRegressor g_regressor;
ExtractInforParams g_ExtractInfor;
FACETRACKER::Tracker g_model;

void detect_gray( Mat gray, CascadeClassifier& cascade, vector<rect> &faces_target )
{
	int i = 0;
	double t = 0;
	vector<Rect> faces;

	equalizeHist( gray, gray );

	//t = (double)cvGetTickCount();//用来计算算法执行时间

	int minl = g_min_face_size * g_zoomrate;
	int maxl = g_max_face_size * g_zoomrate;
	cascade.detectMultiScale( gray, faces,
		g_scalefactor, g_minneighbors, g_detflag
		//|CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		|CV_HAAR_SCALE_IMAGE
		,
		Size(minl, minl),Size(maxl,maxl) );

	for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
	{
		Mat smallImgROI;
		vector<Rect> nestedObjects;
		Point center;

		int radius;
		center.x = cvRound((r->x + r->width*0.5));//还原成原来的大小
		center.y = cvRound((r->y + r->height*0.5));
		radius = cvRound((r->width + r->height)*0.25);

		int x = center.x - radius, y = center.y - radius;
		//double rate = face_area_rate(bny, x, y, 2*radius);

		rect tgt;
		tgt.xmin = x, tgt.ymin = y, tgt.xmax = x + 2 * radius, tgt.ymax = y + 2 * radius;

		//if( rate > 0.35 ) // ecr >= mincr && ecr <= (int)(span_cr + mincr) && ecb >= mincb && ecb <= (int)(mincb + span_cb)
		{
			faces_target.push_back(tgt);
		}		
	}
}

void copy_roi_pxl(IplImage *dst, IplImage *src, int x, int y)
{
	int i,j;
	for(i = y; i < y + dst->height; i++)
		for(j = x; j < x + dst->width; j++)
		{
			dst->imageData[ (i-y) * dst->widthStep + dst->nChannels * (j-x) ] = src->imageData[ i * src->widthStep + src->nChannels * j ];
			dst->imageData[ (i-y) * dst->widthStep + dst->nChannels * (j-x) + 1] = src->imageData[ i * src->widthStep + src->nChannels * j + 1 ];
			dst->imageData[ (i-y) * dst->widthStep + dst->nChannels * (j-x) + 2] = src->imageData[ i * src->widthStep + src->nChannels * j + 2 ];
		}
}

void face_alignment(IplImage *image, rect &face, float *data)
{
	int i;
	char ftFile[256];
	cv::Mat gray,im;
	char Basepath[128];
	char AllImageFilename[128];
	char BaseFilename[128],Base[128];
	char ImageFilename[128],SaveFilename[128];
	char RefMaskImageFilename[128],RefLocFilename[128],RefROIFilename[128];
	FILE *fp;
	
	/****************************************
		Load Config Files that are needed
	****************************************/

	//--> Face Part Contour Detection
	int initial_number = 20;
    int landmark_num = 68;//45;
	//char ModelFilename[128];
	BoundingBox FaceRegion;
	//ShapeRegressor regressor;

	/****************************************
		Process Main Body
	****************************************/
	int img_num = 0;

	//Read into Image and Change it into Gray Level Image
	sprintf(ImageFilename,"%s%s",Basepath,BaseFilename);

	IplImage *GrayI;

	GrayI = cvCreateImage(cvSize(image->width,image->height),IPL_DEPTH_8U,1);
	if (image->nChannels == 1)
		cvCopyImage(image,GrayI);
	else
		cvCvtColor(image,GrayI, CV_RGB2GRAY);
	gray = GrayI;

	//Face Part

	//Face Part Contour Detection
	FaceRegion.start_x = face.xmin;//R.x;
	FaceRegion.start_y = face.ymin;//R.y;
	FaceRegion.width = face.xmax - face.xmin + 1;//R.width;
	FaceRegion.height = face.ymax - face.ymin + 1;//R.height;
    FaceRegion.centroid_x = FaceRegion.start_x + FaceRegion.width/2.0;
    FaceRegion.centroid_y = FaceRegion.start_y + FaceRegion.height/2.0; 
	Mat_<uchar> readimg_Mat = gray;
    Mat_<double> current_shape = 
		g_regressor.Predict(readimg_Mat,FaceRegion,initial_number);

	//Face Alignment & Face Information Extraction
	vector <float> FaceVector1;
	vector <float> FaceVector2;
	vector <float> FaceVector3;
	int FaceVectorSize;
	int ExtractionWidth,ExtractionHeight;
	int Oper_Type;
	Rect Adjustment_R;
// 	ExtractInforParams ExtractInfor;
// 
// 	ExtractInfor.Extraction_Width = g_sampw;
// 	ExtractInfor.Extraction_Height = g_samph;
// 	ExtractInfor.RefMaskImage = NULL;
// 	ExtractInfor.RefPts = NULL;

// 	Oper_Type = 1;
// 	FaceVector1.clear();
// 	FaceInformationExtraction(GrayI,current_shape,landmark_num,
// 		ExtractInfor,Oper_Type,FaceVector1,Adjustment_R);

// 	ExtractInfor.RefPts = (Loc *)calloc(landmark_num,sizeof(Loc));
// 
// 	FILE *fp_rPts = fopen(RefLocFilename,"r");
// 	for (i = 0; i < landmark_num; i ++)
// 		fscanf(fp_rPts,"%f %f\n",&ExtractInfor.RefPts[i].x,&ExtractInfor.RefPts[i].y);
// 	fclose(fp_rPts);
// 
// 	fp_rPts = fopen(RefROIFilename,"r");
// 	fscanf(fp_rPts,"%d %d %d %d",&ExtractInfor.RefFaceROI.x,&ExtractInfor.RefFaceROI.y,
// 		&ExtractInfor.RefFaceROI.width,&ExtractInfor.RefFaceROI.height);
// 	fclose(fp_rPts);
// 
// 	ExtractInfor.RefMaskImage = (IplImage *)cvLoadImage(RefMaskImageFilename);

// 	Oper_Type = 2;
// 	FaceVector2.clear();
// 	FaceInformationExtraction(GrayI,current_shape,landmark_num,
// 		ExtractInfor,Oper_Type,FaceVector2,Adjustment_R);

	Oper_Type = 3;
	FaceVector3.clear();
	FaceInformationExtraction(GrayI,current_shape,landmark_num,
		g_ExtractInfor,Oper_Type,FaceVector3,Adjustment_R);

	for(i = 0; i < FaceVector3.size(); i++)
		data[i] = FaceVector3[i];

	cvReleaseImage(&GrayI);
	readimg_Mat.release();
	gray.release();
}

float *get_alignment_data_sparse(IplImage *image, rect &face, int samplew, int sampleh)
{
	float *face_data = new float[ samplew * sampleh ];

	face_alignment(image, face, face_data);

	return face_data;
}

void recog_video_sparse(string video_path, string cascade_path, vector<string> &dir_path)
{
	CvCapture *capture = cvCreateFileCapture(video_path.c_str());
	IplImage *frame;

	int frameRate = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FPS);//capture.get(CV_CAP_PROP_FPS);
	int framenum = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);//capture.get(CV_CAP_PROP_FRAME_COUNT);
	int width = (int)cvGetCaptureProperty(capture,CV_CAP_PROP_FRAME_WIDTH);
	int height = (int)cvGetCaptureProperty(capture,CV_CAP_PROP_FRAME_HEIGHT);

	//CvVideoWriter *writer=cvCreateVideoWriter("recognition.avi",CV_FOURCC('M','J','P','G'),  frameRate, cvSize(width,height));

	IplImage *gray = cvCreateImage(cvSize(width,height), 8, 1);
	//Mat frame_gray(height, width, CV_8UC1);

	CascadeClassifier cascade;
	cascade.load(cascade_path.c_str());

	double font_scale = 2;
	CvFont font;//以下用文字标识说明
	double hScale=1, vScale=1;
	int lineWidth=2;
	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);

	//g_deting = 1;
	//init_recog_table();
	//vector<int> nmatchrate(g_face_name.size(), 0);
	int frame_idx = 0;
	float *xvec = new float[ g_nsamples ];
	while(1)
	{
		frame = cvQueryFrame(capture);
		if(!frame)break;

		//RecogInfo recoged;

		cvCvtColor(frame, gray, CV_BGR2GRAY);
		Mat frame_gray(gray);

		vector<rect> faces;
		detect_gray(frame_gray, cascade, faces);

		//is_valid_face(frame->width, frame->height, faces);

		//if( frame_idx % g_det_inv == 0 )
		{
			for(int i = 0; i < faces.size(); i++)
			{
				int w = faces[i].xmax - faces[i].xmin + 1;
				int h = faces[i].ymax - faces[i].ymin + 1;

				IplImage *fimg = cvCreateImage(cvSize(w,h), 8, 1);

				copy_roi_pxl(fimg, frame, faces[i].xmin, faces[i].ymin);
				float *y = get_img_vector(fimg, g_samplew, g_sampleh);
				//float *y = get_alignment_data_sparse(frame, faces[i], g_samplew, g_sampleh);

				float lambda = 0.001;
				int recog_idx;

				l1ls_featuresign(g_dict, g_AT, g_ATA, g_samplew * g_sampleh, g_nsamples, y, 1,lambda, xvec, recog_idx);
				draw_recog_info(frame, faces[i], g_face_names[g_class_flag[recog_idx]/*-1*/], font);

				//recoged.faces.push_back(faces[i]);
				//recoged.names.push_back(g_face_name[g_class_flag[recog_idx]/*-1*/]);

				//g_recog_statistics.nfaces[ g_class_flag[recog_idx] ]++;

// 				float rate = recog_matched_rate(xvec, g_nsamples);
// 				if( rate >= g_recog_match_rate )
// 					nmatchrate[ g_class_flag[recog_idx] ]++;

				delete[] y;
				cvReleaseImage(&fimg);
			}
		}
//		g_recoged.push_back(recoged);
		frame_idx++;

		//g_precog_rate = (int)(frame_idx / (double)framenum * 100 + 0.5);
		//::SendMessage(g_dlgwnd, WM_SETPROGRESS, NULL, NULL);
		//cvWriteFrame(writer,frame);
	}

// 	for(int i = 0; i < g_face_name.size(); i++)
// 	{
// 		g_recog_statistics.facesrate[i] = g_recog_statistics.nfaces[i] / (double)framenum;
// 		g_recog_statistics.matchrate[i] = nmatchrate[i] / (double)framenum;
// 	}

	//g_deting = 0;
	delete[] xvec;
	cvReleaseImage(&gray);
	cvReleaseCapture(&capture);
	//cvReleaseVideoWriter(&writer);
}

float *get_dict_matrix(vector<string> &dir_path, float *&AT, float *&ATA, int samplew, int sampleh, 
	int &matrix_length, int &nsamples, vector<int> &class_flag, vector<string> &filename)
{
	//vector<string> dir_path;
	vector<vector<string>> samples_path;
	//dir_path.push_back("1"),dir_path.push_back("2"),dir_path.push_back("3"),dir_path.push_back("4");

	//int sample_l = 90;
	int i,j,k;
	nsamples = 0;
	filename.clear();
	for(i = 0; i < dir_path.size(); i++)
	{
		vector<string> tmp;
		get_path_files2(dir_path[i], tmp);
		samples_path.push_back(tmp);
		nsamples += tmp.size();

		for(j = 0; j < tmp.size(); j++)
			filename.push_back(tmp[j]);
	}

	matrix_length = samplew * sampleh * nsamples;
	float *Atmp = new float[ matrix_length ];

	int sidx = 0, cls = 0, aidx = 0;
	for(i = 0; i < samples_path.size(); i++)
	{
		for(j = 0; j < samples_path[i].size(); j++)
		{
			IplImage *image = cvLoadImage(samples_path[i][j].c_str());
			IplImage *gray = cvCreateImage(cvSize(image->width,image->height), 8, 1);
			IplImage *std_l = cvCreateImage(cvSize(samplew,sampleh), 8, 1);

			if( image->nChannels == 1 )
				memcpy(gray->imageData, image->imageData, image->widthStep * image->height);
			else
				cvCvtColor(image, gray, CV_BGR2GRAY);

			cvResize(gray, std_l);

			for(int ii = 0; ii < std_l->height; ii++)
				for(int jj = 0; jj < std_l->width; jj++)
				{
					unsigned char g = std_l->imageData[ ii * std_l->widthStep + jj ];
					Atmp[ ( ii * samplew + jj ) * nsamples + sidx/*aidx++*/ ] = g;
				}

			sidx++;
			class_flag.push_back(cls);
			cvReleaseImage(&std_l);
			cvReleaseImage(&gray);
			cvReleaseImage(&image);

// 				g_precog_rate = (int)(class_flag.size() / (double)nsamples * 90 + 0.5);
// 				::SendMessage(g_hwnd, WM_SETPROGRESS, NULL, NULL);
		}

		cls++;
	}

	norm_matrix(Atmp, nsamples, samplew * sampleh);
	//save_matrix("dict.txt", Atmp, nsamples, sample_l * sample_l);

	AT = (float *)calloc(samplew*sampleh*nsamples,sizeof(float));
	ATA = (float *)calloc(nsamples*nsamples,sizeof(float));

	Matrix_Transpose(Atmp,AT,samplew*sampleh,nsamples);
	Matrix_Multiply(AT,nsamples,samplew*sampleh,Atmp,samplew*sampleh,nsamples,ATA);

	//	g_precog_rate = 95;
	return Atmp;
}

float *get_dict_matrix(vector<string> &img_path, float *&AT, float *&ATA, int samplew, int sampleh, 
	int &matrix_length, vector<int> &class_flag)
{
	//vector<string> dir_path;
	vector<vector<string>> samples_path;
	//dir_path.push_back("1"),dir_path.push_back("2"),dir_path.push_back("3"),dir_path.push_back("4");

	//int sample_l = 90;
	int i,j,k;
	int nsamples = img_path.size();

	matrix_length = samplew * sampleh * nsamples;
	float *Atmp = new float[ matrix_length ];

	int sidx = 0, cls = 0, aidx = 0;

	vector<float> face_data;
	for(i= 0; i<  img_path.size(); i++)
	{
		IplImage *image = cvLoadImage( img_path[i].c_str());

		detect_alignment(image, face_data);

// 		IplImage *gray = cvCreateImage(cvSize(image->width,image->height), 8, 1);
// 		IplImage *std_l = cvCreateImage(cvSize(samplew,sampleh), 8, 1);

// 		if( image->nChannels == 1 )
// 			memcpy(gray->imageData, image->imageData, image->widthStep * image->height);
// 		else
// 			cvCvtColor(image, gray, CV_BGR2GRAY);
// 
// 		cvResize(gray, std_l);

// 		for(int ii = 0; ii < std_l->height; ii++)
// 			for(int jj = 0; jj < std_l->width; jj++)
// 			{
// 				unsigned char g = std_l->imageData[ ii * std_l->widthStep + jj ];
// 				Atmp[ ( ii * samplew + jj ) * nsamples + sidx/*aidx++*/ ] = g;
// 			}

		for(int ii = 0; ii < face_data.size(); ii++)
		{
			Atmp[ ii*nsamples + sidx ] = face_data[ii];
		}

		sidx++;
		class_flag.push_back(cls);
		// 		cvReleaseImage(&std_l);
		// 		cvReleaseImage(&gray);
		cvReleaseImage(&image);
		cls++;
	}

	norm_matrix(Atmp, nsamples, samplew * sampleh);
	//save_matrix("dict.txt", Atmp, nsamples, sample_l * sample_l);

	AT = (float *)calloc(samplew*sampleh*nsamples,sizeof(float));
	ATA = (float *)calloc(nsamples*nsamples,sizeof(float));

	Matrix_Transpose(Atmp,AT,samplew*sampleh,nsamples);
	Matrix_Multiply(AT,nsamples,samplew*sampleh,Atmp,samplew*sampleh,nsamples,ATA);

	g_nsamples=nsamples;
	return Atmp;
}

void init_detect_model()
{
	int i;
	int landmark_num = 68;

	g_model.Load("DetectionAlignment\\face2.tracker");
	g_regressor.Load("DetectionAlignment\\My_model_68Points_OurFaceDetection.txt");

	g_ExtractInfor.Extraction_Width = g_samplew;//30;
	g_ExtractInfor.Extraction_Height = g_sampleh;//30;
	g_ExtractInfor.RefMaskImage = NULL;
	g_ExtractInfor.RefPts = NULL;

	g_ExtractInfor.RefPts = (Loc *)calloc(landmark_num,sizeof(Loc));
	FILE *fp_rPts = fopen("DetectionAlignment\\ReferenceImage_68points.txt","r");
	for (i = 0; i < landmark_num; i ++)
		fscanf(fp_rPts,"%f %f\n",&g_ExtractInfor.RefPts[i].x,&g_ExtractInfor.RefPts[i].y);
	fclose(fp_rPts);

	fp_rPts = fopen("DetectionAlignment\\Reference_Mask_ROI_68points.txt","r");
	fscanf(fp_rPts,"%d %d %d %d",&g_ExtractInfor.RefFaceROI.x,&g_ExtractInfor.RefFaceROI.y,
		&g_ExtractInfor.RefFaceROI.width,&g_ExtractInfor.RefFaceROI.height);
	fclose(fp_rPts);

	g_ExtractInfor.RefMaskImage = (IplImage *)cvLoadImage("DetectionAlignment\\Reference_Mask_68points.bmp");
	scales[0] = 300, scales[1] = 212, scales[2] = 150, scales[3] = 106, scales[4] = 75;
}

unsigned _stdcall init_detect_model_thread(void* param)
{
	init_detect_model();
// 	HMENU sysMenu = GetSystemMenu(g_dlg, FALSE);
// 	EnableMenuItem(sysMenu, SC_CLOSE, MF_BYCOMMAND| MF_ENABLED);
	return 1;
}

int detect_alignment(IplImage *image, Mat &shape)
{
	int i;
	char ftFile[256];
	cv::Mat /*frame,*/gray/*,im*/;
	char AllImageFilename[128];
	char BaseFilename[128],Base[128];
	char ImageFilename[128],SaveFilename[128];
	char RefMaskImageFilename[128],RefLocFilename[128],RefROIFilename[128];
	FILE *fp;

	//--> Face Detection
	//strcpy(ftFile,"DetectionAlignment\\face2.tracker");

	//--> Face Part Contour Detection
	int initial_number = 20;
	int landmark_num = 68;//45;
	char ModelFilename[128];
	BoundingBox FaceRegion;
	// 	ShapeRegressor regressor;
	// 
	// 	strcpy(ModelFilename,"DetectionAlignment\\My_model_68Points_OurFaceDetection.txt");
	// 	regressor.Load(ModelFilename);
	//Reference Image
	//	strcpy(RefMaskImageFilename,"DetectionAlignment\\Reference_Mask_68points.bmp");
	//	strcpy(RefLocFilename,"DetectionAlignment\\ReferenceImage_68points.txt");
	//	strcpy(RefROIFilename,"DetectionAlignment\\Reference_Mask_ROI_68points.txt");

	DWORD st = GetTickCount();
	int img_num = 0;
	IplImage *GrayI;

	GrayI = cvCreateImage(cvSize(image->width,image->height),IPL_DEPTH_8U,1);
	if (image->nChannels == 1)
		cvCopyImage(image,GrayI);
	else
		cvCvtColor(image,GrayI, CV_RGB2GRAY);
	gray = GrayI;

	//Face Part
	cv::Rect R;
	g_model.FaceDetectionFromTrackingVersion(gray,R);
	if (R.width == 0)
	{
		//printf("No Face ....\n");
		return 0;
	}
	//Face Part Contour Detection
	FaceRegion.start_x = R.x;
	FaceRegion.start_y = R.y;
	FaceRegion.width = R.width;
	FaceRegion.height = R.height;
	FaceRegion.centroid_x = FaceRegion.start_x + FaceRegion.width/2.0;
	FaceRegion.centroid_y = FaceRegion.start_y + FaceRegion.height/2.0; 
	Mat_<uchar> readimg_Mat = gray;
	Mat_<double> current_shape = 
		g_regressor.Predict(readimg_Mat,FaceRegion,initial_number);

	//shape.create(current_shape.rows, current_shape.cols/*, CV_64F*/);

	//Face Alignment & Face Information Extraction
	vector <float> FaceVector1;
	vector <float> FaceVector2;
	vector <float> FaceVector3;
	int FaceVectorSize;
	int ExtractionWidth,ExtractionHeight;
	int Oper_Type;
	Rect Adjustment_R;

	Oper_Type = 3;
	FaceVector3.clear();
	FaceInformationExtraction(GrayI,current_shape,landmark_num,
		g_ExtractInfor,Oper_Type,FaceVector3,Adjustment_R);

	for(int i = 0; i < current_shape.rows; i++)
	{
		shape.at<double>(i,0) = current_shape.at<double>(i,0);
		shape.at<double>(i,1) = current_shape.at<double>(i,1);
	}

	cvReleaseImage(&GrayI);
	readimg_Mat.release();
	//	im.release();
	gray.release();
	//	frame.release();

	DWORD tp = GetTickCount() - st;

	//	printf("%d", tp);

	return 1;
}

int detect_alignment(IplImage *image, vector<float> &face_data)
{
	int i;
	char ftFile[256];
	cv::Mat /*frame,*/gray/*,im*/;
	char AllImageFilename[128];

	FILE *fp;

	//--> Face Detection
	//strcpy(ftFile,"DetectionAlignment\\face2.tracker");

	//--> Face Part Contour Detection
	int initial_number = 20;
	int landmark_num = 68;//45;
	char ModelFilename[128];
	BoundingBox FaceRegion;
	// 	ShapeRegressor regressor;

	DWORD st = GetTickCount();
	int img_num = 0;
	IplImage *GrayI;

	GrayI = cvCreateImage(cvSize(image->width,image->height),IPL_DEPTH_8U,1);
	if (image->nChannels == 1)
		cvCopyImage(image,GrayI);
	else
		cvCvtColor(image,GrayI, CV_RGB2GRAY);
	gray = GrayI;

	//Face Part
	cv::Rect R;
	g_model.FaceDetectionFromTrackingVersion(gray,R);
	if (R.width == 0)
	{
		//printf("No Face ....\n");
		return 0;
	}
	//Face Part Contour Detection
	FaceRegion.start_x = R.x;
	FaceRegion.start_y = R.y;
	FaceRegion.width = R.width;
	FaceRegion.height = R.height;
	FaceRegion.centroid_x = FaceRegion.start_x + FaceRegion.width/2.0;
	FaceRegion.centroid_y = FaceRegion.start_y + FaceRegion.height/2.0; 
	Mat_<uchar> readimg_Mat = gray;
	Mat_<double> current_shape = 
		g_regressor.Predict(readimg_Mat,FaceRegion,initial_number);

	//shape.create(current_shape.rows, current_shape.cols/*, CV_64F*/);

	//Face Alignment & Face Information Extraction
	vector <float> FaceVector1;
	vector <float> FaceVector2;
	vector <float> FaceVector3;
	int FaceVectorSize;
	int ExtractionWidth,ExtractionHeight;
	int Oper_Type;
	Rect Adjustment_R;

	Oper_Type = 3;
	FaceVector3.clear(), face_data.clear();
	FaceInformationExtraction(GrayI,current_shape,landmark_num,
		g_ExtractInfor,Oper_Type,/*FaceVector3*/face_data,Adjustment_R);

// 	for(int i = 0; i < current_shape.rows; i++)
// 	{
// 		shape.at<double>(i,0) = current_shape.at<double>(i,0);
// 		shape.at<double>(i,1) = current_shape.at<double>(i,1);
// 	}

	cvReleaseImage(&GrayI);
	readimg_Mat.release();
	//	im.release();
	gray.release();
	//	frame.release();

	return 1;
}

int alignment(IplImage *image, rect &pos, Mat &shape)
{
	int i;
	char ftFile[256];
	cv::Mat /*frame,*/gray/*,im*/;
	FILE *fp;

	int initial_number = 20;
	int landmark_num = 68;//45;
	char ModelFilename[128];
	BoundingBox FaceRegion;

	DWORD st = GetTickCount();
	int img_num = 0;
	IplImage *GrayI;

	GrayI = cvCreateImage(cvSize(image->width,image->height),IPL_DEPTH_8U,1);
	if (image->nChannels == 1)
		cvCopyImage(image,GrayI);
	else
		cvCvtColor(image,GrayI, CV_RGB2GRAY);
	gray = GrayI;

	//Face Part

	//Face Part Contour Detection
	FaceRegion.start_x = pos.xmin;//R.x;
	FaceRegion.start_y = pos.ymin;//R.y;
	FaceRegion.width = pos.xmax-pos.xmin+1;//R.width;
	FaceRegion.height = pos.ymax-pos.ymin+1;//R.height;
	FaceRegion.centroid_x = FaceRegion.start_x + FaceRegion.width/2.0;
	FaceRegion.centroid_y = FaceRegion.start_y + FaceRegion.height/2.0; 
	Mat_<uchar> readimg_Mat = gray;
	Mat_<double> current_shape = 
		g_regressor.Predict(readimg_Mat,FaceRegion,initial_number);

	//shape.create(current_shape.rows, current_shape.cols/*, CV_64F*/);

	//Face Alignment & Face Information Extraction
	vector <float> FaceVector1;
	vector <float> FaceVector2;
	vector <float> FaceVector3;
	int FaceVectorSize;
	int ExtractionWidth,ExtractionHeight;
	int Oper_Type;
	Rect Adjustment_R;

	Oper_Type = 3;
	FaceVector3.clear();
	FaceInformationExtraction(GrayI,current_shape,landmark_num,
		g_ExtractInfor,Oper_Type,FaceVector3,Adjustment_R);

	for(int i = 0; i < current_shape.rows; i++)
	{
		shape.at<double>(i,0) = current_shape.at<double>(i,0);
		shape.at<double>(i,1) = current_shape.at<double>(i,1);
	}

	cvReleaseImage(&GrayI);
	readimg_Mat.release();
	gray.release();

	return 1;
}

int get_face_feature(IplImage *image, int *feature)
{
	LBPFeatureExtractor lbpFeatureExtractor(scales, patchSize, gridNumX, gridNumY, true);

	Mat_<double> shape(68, 2);

	vector< pair<double, double> > points;
	if ( detect_alignment(image, shape) )/*fa->Detect(image,face,landmarks,score) == INTRAFACE::IF_OK*/
	{
		pair<double, double>point;
		//Left and Right eye center

		double lx = 0, ly = 0, rx = 0, ry = 0;
		for(int i = 36; i <= 41; i++)
		{
			lx += shape.at<double>(i,0);
			ly += shape.at<double>(i,1);
		}

		for(int i = 42; i <= 47; i++)
		{
			rx += shape.at<double>(i,0);
			ry += shape.at<double>(i,1);
		}

		lx /= 6, ly /= 6, rx /= 6, ry /= 6;

		point.first = lx;//0.5*(shape.at<double>(36,0) + shape.at<double>(39,0));//0.5*(landmarks.at<float>(0,19) + landmarks.at<float>(0,22));
		point.second = ly;//0.5*(shape.at<double>(36,1) + shape.at<double>(39,1));//0.5*(landmarks.at<float>(1,19) + landmarks.at<float>(1,22));
		points.push_back(point);

		point.first = rx;//0.5*(shape.at<double>(42,0) + shape.at<double>(45,0));
		point.second = ry;//0.5*(shape.at<double>(42,1) + shape.at<double>(45,1));
		points.push_back(point);

		vector<int> pidx;
		int p14[] = {36, 39, 27, 28, 30, 31, 33, 35, 42, 45, 48, 51, 54, 57};
		//int pidx[] = {17, 19, 21, 22, 24, 26, 27, 31, 33, 35, 48, 51, 54, 57};

		if( landmarkNum == 16 )
		{
			for(int i = 0; i < landmarkNum - 2; i++)
				pidx.push_back( p14[i] );
		}

		if( landmarkNum == 53 )
		{
			for(int i = 17; i <= 67; i++)
				pidx.push_back(i);

			// 			for(int i = 48; i <= 67; i++)
			// 				pidx.push_back(i);
		}

		// 		for(int i = 0; i < pidx.size(); i++)
		// 		{
		// 			CvPoint p;
		// 			p.x = shape.at<double>(pidx[i],0);
		// 			p.y = shape.at<double>(pidx[i],1);
		// 
		// 			cvCircle(image, p, 2, cvScalar(0, 255, 0), 1, 6, 0);
		// 		}
		// 		cvCircle(image, cvPoint(lx,ly), 3, cvScalar(0, 255, 0), 1, 6, 0);
		// 		cvCircle(image, cvPoint(rx,ry), 3, cvScalar(0, 255, 0), 1, 6, 0);
		// 		cvSaveImage("feature_points.jpg", image);

		for(int i = 0; i < landmarkNum - 2; i++)
		{
			point.first = shape.at<double>(pidx[i],0);//landmarks.at<float>(0,landmarkPosition[i]);
			point.second = shape.at<double>(pidx[i],1);//landmarks.at<float>(1,landmarkPosition[i]);
			points.push_back(point);
		}
	}
	else
		return 0;

	//	int *feature = new int[totalDim];
	Mat faceImage;
	vector< pair<double, double> > newPoints;
	FaceNormalizer faceNormalizer(eyeDistanceX, eyeDistanceUp, eyeDistanceDown);
	if (faceNormalizer.normalize(image, points, faceImage, newPoints)) 
	{
		lbpFeatureExtractor.extractAt(faceImage, newPoints, feature);
	}

	// 	  ofstream ff("feature.txt");
	// 	  for(int k = 0; k < totalDim; k++)
	// 		  ff<< feature[k]<<endl;
	// 	  ff.close();

	faceImage.release();
	points.clear();
	newPoints.clear();

	return 1;
}

int get_face_feature(IplImage *image, rect &pos, int *feature)
{
	LBPFeatureExtractor lbpFeatureExtractor(scales, patchSize, gridNumX, gridNumY, true);

	Mat_<double> shape(68, 2);

	vector< pair<double, double> > points;
	if ( alignment(image, pos, shape) )/*fa->Detect(image,face,landmarks,score) == INTRAFACE::IF_OK*/
	{
		pair<double, double>point;
		//Left and Right eye center

		double lx = 0, ly = 0, rx = 0, ry = 0;
		for(int i = 36; i <= 41; i++)
		{
			lx += shape.at<double>(i,0);
			ly += shape.at<double>(i,1);
		}

		for(int i = 42; i <= 47; i++)
		{
			rx += shape.at<double>(i,0);
			ry += shape.at<double>(i,1);
		}

		lx /= 6, ly /= 6, rx /= 6, ry /= 6;

		point.first = lx;//0.5*(shape.at<double>(36,0) + shape.at<double>(39,0));//0.5*(landmarks.at<float>(0,19) + landmarks.at<float>(0,22));
		point.second = ly;//0.5*(shape.at<double>(36,1) + shape.at<double>(39,1));//0.5*(landmarks.at<float>(1,19) + landmarks.at<float>(1,22));
		points.push_back(point);

		point.first = rx;//0.5*(shape.at<double>(42,0) + shape.at<double>(45,0));
		point.second = ry;//0.5*(shape.at<double>(42,1) + shape.at<double>(45,1));
		points.push_back(point);

		vector<int> pidx;
		int p14[] = {36, 39, 27, 28, 30, 31, 33, 35, 42, 45, 48, 51, 54, 57};
		//int pidx[] = {17, 19, 21, 22, 24, 26, 27, 31, 33, 35, 48, 51, 54, 57};

		if( landmarkNum == 16 )
		{
			for(int i = 0; i < landmarkNum - 2; i++)
				pidx.push_back( p14[i] );
		}

		if( landmarkNum == 53 )
		{
			for(int i = 17; i <= 67; i++)
				pidx.push_back(i);
		}

		for(int i = 0; i < landmarkNum - 2; i++)
		{
			point.first = shape.at<double>(pidx[i],0);//landmarks.at<float>(0,landmarkPosition[i]);
			point.second = shape.at<double>(pidx[i],1);//landmarks.at<float>(1,landmarkPosition[i]);
			points.push_back(point);
		}
	}
	else
		return 0;

	//	int *feature = new int[totalDim];
	Mat faceImage;
	vector< pair<double, double> > newPoints;
	FaceNormalizer faceNormalizer(eyeDistanceX, eyeDistanceUp, eyeDistanceDown);
	if (faceNormalizer.normalize(image, points, faceImage, newPoints)) 
	{
		lbpFeatureExtractor.extractAt(faceImage, newPoints, feature);
	}

	faceImage.release();
	points.clear();
	newPoints.clear();

	return 1;
}

void read_face_features(string dir, vector<string> &face_names, vector<int> &det_flag, int **features, int dim)
{
	string fpath = dir + "\\features.txt";
	int ff_flag = _access( fpath.c_str(), 0 );

	det_flag.resize(face_names.size());
	if( ff_flag != -1 )
	{
		ifstream ff(fpath.c_str());
		for(int i = 0; i < face_names.size(); i++)
		{
			ff>>det_flag[i];
			for(int j = 0; j < dim; j++)
				ff>>features[i][j];

			printf("readed: %d\n", i);
		}
		ff.close();
	}
	else
	{
		string tmpdir = dir + "\\";
		for(int i = 0; i < face_names.size(); i++)
		{
			string path = tmpdir + face_names[i];
			IplImage *image = cvLoadImage(path.c_str());

			//memset(features[i], 0, totalDim);
			if( get_face_feature(image, features[i]) )
				det_flag[i] = 1;
			else
			{
				det_flag[i] = 0;
				for(int j = 0; j < totalDim; j++)
					features[i][j] = 0;
			}

			printf("get_feature: %d\n", i);
			cvReleaseImage(&image);
		}
	}
}

double get_correlation(int *vec, int *vec2, int length)
{
	double a = 0,b = 0,c = 0,correlation;

	for(int i = 0; i < length; i++)
	{
		a += vec[i] * vec2[i];
		b += vec[i] * vec[i];
		c += vec2[i] * vec2[i];
	}

	/*a = sqrt(a),*/ b = sqrt(b), c = sqrt(c);

	correlation = a / (b*c);

	return correlation;
}

vector<int> descend_vec(vector<double> &data)
{
	vector<int> index(data.size());
	for(int i = 0; i < data.size(); i++)
		index[i] = i;

	for(int i = 0; i < data.size(); i++)
		for(int j = i + 1; j < data.size(); j++)
		{
			if( data[i] < data[j] )
			{
				swap( data[i], data[j] );
				swap( index[i], index[j] );
			}
		}

	return index;
}

vector<int> create_lbp_base(vector<string> &img_pathes, int **base_features)
{
	vector<int> det_flag(img_pathes.size());

	for(int i = 0; i < img_pathes.size(); i++)
	{
		IplImage *image = cvLoadImage(img_pathes[i].c_str());

		if( get_face_feature(image, base_features[i]) )
			det_flag[i] = 1;
		else
		{
			det_flag[i] = 0;
			for(int j = 0; j < totalDim; j++)
				base_features[i][j] = 0;
		}

		printf("get_feature: %d\n", i);
		cvReleaseImage(&image);
	}

	return det_flag;
}
string get_name_from_path(string path)
{
	int l = path.rfind("\\");
	int r = path.rfind(".");
	//string dir = path.substr(0, l+1);
	string name = path.substr(l+1, r);

	return name;
}

string get_dir_from_path(string path)
{
	int l = path.rfind("\\");
	int r = path.rfind(".");
	string dir = path.substr(0, l+1);
	//string name = path.substr(l+1, r);

	return dir;
}

void extract_lbp_feature(string path)
{
	string dir = get_dir_from_path(path);//path.substr(0, l+1);
	string name = get_name_from_path(path);//path.substr(l+1, r);
	string ft = "_lbp";

	vector<int> tmpf(totalDim,0);
	
	string txt_path = dir + name + ft + ".txt";
	if( _access(txt_path.c_str(),0) != -1 )
	{
		ifstream ff(txt_path);
		for(int i = 0; i < totalDim; i++)
			ff>>tmpf[i];

		EnterCriticalSection(&g_csf);
		g_tmp_bfeatures.push_back(tmpf);
		LeaveCriticalSection(&g_csf);
	}
	else
	{
		int *feature = new int[totalDim];
		IplImage *image = cvLoadImage(path.c_str());
		if( get_face_feature(image, feature) )
		{
			for(int i = 0; i < totalDim; i++)
				tmpf[i] = feature[i];

			EnterCriticalSection(&g_csf);
			g_tmp_bfeatures.push_back(tmpf);
			LeaveCriticalSection(&g_csf);

			ofstream ff(txt_path);
			for(int i = 0; i < totalDim; i++)
			{
				ff<<feature[i];
				if( i < totalDim-1 )
					ff<<endl;
			}
			ff.close();
		}
		cvReleaseImage(&image);
		delete[] feature;
	}
}

unsigned _stdcall extract_feature_lbp_thread(void* param)
{
	EnterCriticalSection(&g_csf);
	string cur_path = g_undet_pathes[0];
	g_undet_pathes.erase(g_undet_pathes.begin());
	LeaveCriticalSection(&g_csf);

	WaitForSingleObject(g_init_model_hdl, INFINITE); //  等待初始化线程

// 	if( g_extract_feature_hdls.size() > 2 )
// 		WaitForSingleObject(g_extract_feature_hdls[g_extract_feature_hdls.size()-2], INFINITE);

	extract_lbp_feature(cur_path);
	return 1;
}

unsigned _stdcall extract_feature_lbps_thread(void* param)
{
	WaitForSingleObject(g_init_model_hdl, INFINITE); //  等待初始化线程

	for(int i = 0; i < g_attention_pathes.size(); i++)//g_dirpathes.size()
		extract_lbp_feature(g_attention_pathes[i]);

	return 1;
}

unsigned _stdcall extract_feature_sp_thread(void* param)
{
	EnterCriticalSection(&g_csf);
	string cur_path = g_undet_pathes[0];
	g_undet_pathes.erase(g_undet_pathes.begin());
	LeaveCriticalSection(&g_csf);

	WaitForSingleObject(g_init_model_hdl, INFINITE); //  等待初始化线程

	vector<float> feature;
	IplImage  *image = cvLoadImage(cur_path.c_str());
	detect_alignment(image, feature);
	cvReleaseImage(&image);

	g_sp_bfeatures.push_back(feature);

	string dir = get_dir_from_path(cur_path);
	string name = get_name_from_path(cur_path);
	string ft = "_sp";

	string txt_path = dir + name + ft + ".txt";
	ofstream ff(txt_path);
	for(int i = 0; i < feature.size(); i++)
	{
		ff<<feature[i];
		if( i < feature.size()-1 )
			ff<<endl;
	}
	ff.close();

	return 1;
}

unsigned _stdcall extract_feature_sps_thread(void* param)
{
	WaitForSingleObject(g_init_model_hdl, INFINITE); //  等待初始化线程

	for(int i = 0; i < g_attention_pathes.size(); i++)//g_dirpathes.size()
	{
		vector<float> feature;
		IplImage *image = cvLoadImage(g_attention_pathes[i].c_str());
		detect_alignment(image, feature);
		cvReleaseImage(&image);

		g_sp_bfeatures.push_back(feature);

		string dir = get_dir_from_path(g_attention_pathes[i]);
		string name = get_name_from_path(g_attention_pathes[i]);
		string ft = "_sp";

		string txt_path = dir + name + ft + ".txt";
		ofstream ff(txt_path);
		for(int i = 0; i < feature.size(); i++)
		{
			ff<<feature[i];
			if( i < feature.size()-1 )
				ff<<endl;
		}
		ff.close();
	}

	return 1;
}

unsigned _stdcall delete_feature_lbp_thread(void* param)
{
	for(int i = 0; i < g_extract_feature_hdls.size(); i++)
		WaitForSingleObject(g_extract_feature_hdls[i], INFINITE);

	EnterCriticalSection(&g_csf);
	g_tmp_bfeatures.erase(g_tmp_bfeatures.begin() + g_del_idx);
	LeaveCriticalSection(&g_csf);

	return 1;
}

unsigned _stdcall delete_feature_lbps_thread(void* param)
{
	WaitForSingleObject(g_extract_lbps_hdl, INFINITE);

	EnterCriticalSection(&g_csf);
	for(int i = 0; i < g_tmp_bfeatures.size(); i++)
		g_tmp_bfeatures[i].clear();

	g_tmp_bfeatures.clear();
	LeaveCriticalSection(&g_csf);

	return 1;
}

unsigned _stdcall delete_feature_sp_thread(void* param)
{
	for(int i = 0; i < g_extract_feature_hdls.size(); i++)
		WaitForSingleObject(g_extract_feature_hdls[i], INFINITE);

	EnterCriticalSection(&g_csf);
	g_sp_bfeatures.erase(g_sp_bfeatures.begin() + g_del_idx);
	LeaveCriticalSection(&g_csf);

	return 1;
}

unsigned _stdcall delete_feature_sps_thread(void* param)
{
	WaitForSingleObject(g_extract_sps_hdl, INFINITE);

	EnterCriticalSection(&g_csf);
	for(int i = 0; i < g_sp_bfeatures.size(); i++)
		g_sp_bfeatures[i].clear();

	g_sp_bfeatures.clear();
	LeaveCriticalSection(&g_csf);

	return 1;
}

unsigned _stdcall copy_feature_lbp_thread(void* param)
{
	for(int i = 0; i < g_extract_feature_hdls.size(); i++)
		WaitForSingleObject(g_extract_feature_hdls[i], INFINITE);

	for(int i = 0; i < g_last_db_size; i++)
		delete[] g_base_features[i];

	delete[] g_base_features;

	EnterCriticalSection(&g_csf);
	g_last_db_size = g_tmp_bfeatures.size();
	g_base_features = new int *[g_tmp_bfeatures.size()];

	for(int i = 0; i < g_tmp_bfeatures.size(); i++)
	{
		g_base_features[i] = new int[totalDim];
		for(int j = 0; j < totalDim; j++)
			g_base_features[i][j] = g_tmp_bfeatures[i][j];

		g_tmp_bfeatures[i].clear();
	}
	
	g_tmp_bfeatures.clear();
	LeaveCriticalSection(&g_csf);

	return 1;
}

unsigned _stdcall extract_feature_lbp_thread2(void* param)
{
	WaitForSingleObject(g_init_model_hdl, INFINITE); //  等待初始化线程

	for(int i = 0; i < g_dirpathes.size(); i++)
		extract_lbp_feature(g_dirpathes[i]);

	return 1;
}

unsigned _stdcall create_feature_lbp_thread(void* param)  // old version
{
	WaitForSingleObject(g_init_model_hdl, INFINITE); //  等待初始化线程
	g_det_db_flag = create_lbp_base(g_dirpathes, g_base_features);
	MessageBox(NULL, "数据库创建成功", "提示", MB_OK);
	return 1;
}

unsigned _stdcall create_feature_sparsecoding_thread(void* param)
{
	WaitForSingleObject(g_init_model_hdl, INFINITE); //  等待初始化线程
	//create_lbp_base(g_dirpathes, g_base_features);
	int matrix_length;
	g_dict = get_dict_matrix(g_dirpathes, g_AT, g_ATA, g_samplew, g_sampleh, matrix_length,  g_class_flag);
	MessageBox(NULL, "数据库创建成功", "提示", MB_OK);
	return 1;
}

void recog_face_lbp(string video_path, string cascade_path, vector<string> &base_names, int **base_features, vector<int> &base_det_flag)
{
	CvCapture *capture = cvCreateFileCapture(video_path.c_str());
	IplImage *frame;

	int frameRate = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FPS);//capture.get(CV_CAP_PROP_FPS);
	int framenum = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);//capture.get(CV_CAP_PROP_FRAME_COUNT);
	int width = (int)cvGetCaptureProperty(capture,CV_CAP_PROP_FRAME_WIDTH);
	int height = (int)cvGetCaptureProperty(capture,CV_CAP_PROP_FRAME_HEIGHT);

	IplImage *gray = cvCreateImage(cvSize(width,height), 8, 1);
	//Mat frame_gray(height, width, CV_8UC1);

	CascadeClassifier cascade;
	cascade.load(cascade_path.c_str());

	double font_scale = 2;
	CvFont font;//以下用文字标识说明
	double hScale=1, vScale=1;
	int lineWidth=2;
	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);

	int *test_feature = new int[totalDim];
	while(1)
	{
		frame = cvQueryFrame(capture);
		if(!frame)break;

		cvCvtColor(frame, gray, CV_BGR2GRAY);
		Mat frame_gray(gray);

		vector<rect> faces;
		detect_gray(frame_gray, cascade, faces);

		for(int i = 0; i < faces.size(); i++)
		{
			int w = faces[i].xmax - faces[i].xmin + 1;
			int h = faces[i].ymax - faces[i].ymin + 1;

			//IplImage *fimg = enlarge_local_image(frame, faces[i]);//cvCreateImage(cvSize(w,h), 8, 3);

			memset(test_feature, 0, sizeof(int)*totalDim);
			get_face_feature(frame, faces[i], test_feature);

			vector<double> corr( base_names.size() );
			for(int j = 0; j < base_names.size(); j++)
			{
				if( base_det_flag[j] )
				{
					double r = get_correlation(test_feature, base_features[j], totalDim);// 第 i 个测试图像与第 j 个数据库图像的相关性
					corr[j] = r;
				}
				else
				{
					corr[j] = 0;
				}
			}

			vector<int> index = descend_vec(corr);
			string recoged_name = base_names[ index[0] ];
			draw_recog_info(frame, faces[i], recoged_name, font);

			//cvReleaseImage(&fimg);
		}
	}

	delete[] test_feature;
}

IplImage *enlarge_local_image(IplImage *src, rect &pos)
{
	int w = pos.xmax - pos.xmin + 1, h = pos.ymax - pos.ymin + 1;

	IplImage *large = cvCreateImage(cvSize(w*2,h*2), 8, src->nChannels);
	cvZero(large);

	int l = pos.xmin - (large->width - w) / 2;
	int r = l + large->width - 1;
	int t = pos.ymin - (large->height - h) / 2;
	int b = t + large->height + 1;

	for(int i = t; i <= b; i++)
	{
		if( i < 0 || i >= src->height )
			continue;

		for(int j = l; j <= r; j++)
		{
			if( j < 0 || j >= src->width )
				continue;

			for(int k = 0; k < src->nChannels; k++)
				large->imageData[ (i-t)*large->widthStep + (j-l)*large->nChannels + k ] = src->imageData[ i*src->widthStep + j*src->nChannels + k ];
		}
	}

	return large;
}

void HD_LBP_Recog()
{
	init_detect_model();
	

	string src_dir = "2normal", base_dir = "2base";

	vector<string> src_names, base_names;
	vector<int> src_det_flag, base_det_flag;

//	get_path_files(src_dir, src_names);
//	get_path_files(base_dir, base_names);

	int **src_feature = new int*[src_names.size()];
	int **base_feature = new int*[base_names.size()];//totalDim

	for(int i = 0; i < src_names.size(); i++)
		src_feature[i] = new int[totalDim];

	for(int i = 0; i < base_names.size(); i++)
		base_feature[i] = new int[totalDim];


	read_face_features(src_dir, src_names, src_det_flag, src_feature, totalDim);  //  获取特征数据
	read_face_features(base_dir, base_names, base_det_flag, base_feature, totalDim); //  获取特征数据


	int src_undet = 0, test_undet = 0, recoged = 0, unrecoged = 0;

	for(int i = 0; i < src_names.size(); i++)   //  测试集中的图像
	{
		//ftest<<src_names[i];
		if( !src_det_flag[i] )
		{
			src_undet++;
			//ftest<<"    src_undet"<<endl;
			continue;
		}

		int detflag = 1;
		vector<double> corr( base_names.size() );
		for(int j = 0; j < base_names.size(); j++)
		{
			if( base_det_flag[j] )
			{
				double r = get_correlation(src_feature[i], base_feature[j], totalDim);// 第 i 个测试图像与第 j 个数据库图像的相关性
				corr[j] = r;
			}
			else
			{
				test_undet++;
				corr[j] = 0;
			}
		}

		if( detflag )
		{
			vector<int> index = descend_vec(corr);     // 相关性降序排序
			//int flag = is_recognised(src_names[i], index, base_names, 1);  // 在Top1 中是否识别成功
		}
	}

	int kkk = 0;
}

float get_residual(float *dict, int nsamples, int nfeatures, float *test_data, float *xout, int index)
{
	float error, sum = 0;

	for(int j = 0; j < nfeatures; j++)
	{
		float e = test_data[j] - dict[ j * nsamples + index ] * xout[index];
		sum += e * e;
	}

	error = sqrt(sum);

	return error;
}