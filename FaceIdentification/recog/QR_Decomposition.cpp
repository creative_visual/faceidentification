#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "QR_Decomposition.h" 

#define SIGN(a,b) ((b) > 0.0 ? fabs(a) : - fabs(a))

static float maxarg1,maxarg2;
#define FMAX(a,b) (maxarg1 = (a),maxarg2 = (b),(maxarg1) > (maxarg2) ? (maxarg1) : (maxarg2))

static float sqrarg;
#define SQR(a) ((sqrarg = (a)) == 0.0 ? 0.0 : sqrarg * sqrarg)
 
void Dec(float *A, int m, int n, float *QR,float *RDiag, int p)
{
	int i,j,k;

	for (i = 0; i < (m * n); i ++)
		QR[i] = A[i];

    // main loop.
    for( int k=0; k<p; ++k )
    {
        // Compute 2-norm of k-th column without under/overflow.
        float nrm = 0;
        for( int i=k; i<m; ++i )
            nrm += QR[i*n + k]*QR[i*n + k];
        nrm = sqrt(nrm);
		if( nrm != 0 )
        {
            // Form k-th Householder vector.
            if( QR[k*n+k] < 0 )
                nrm = -nrm;

            for( int i=k; i<m; ++i )
                QR[i*n+k] /= nrm;

            QR[k*n+k] += 1;

            // Apply transformation to remaining columns.
            for( int j=k+1; j<n; ++j )
            {
                float s = 0;
                for( int i=k; i<m; ++i )
                    s += QR[i*n+k]*QR[i*n+j];

                s = -s/QR[k*n+k];
                for( int i=k; i<m; ++i )
                    QR[i*n+j] += s*QR[i*n+k];
            }
        }
        RDiag[k] = -nrm;
	}
}

/**
 * Least squares solution of A*x = b
 * Return x: a vector that minimizes the two norm of Q*R*X-B.
 * If B is non-conformant, or if QR.isFullRank() is false,
 * the routinereturns a null (0-length) vector.
 */
void solve(float *QR, float *b, float *solved_x,float *RDiag,int m,int n)
{
	int i;

	float *x = (float *)calloc(n,sizeof(float));

	for (i = 0; i < m; i ++)
		x[i] = b[i];

    // compute y = transpose(Q)*b
    for( int k=0; k<n; ++k )
    {
        float s = 0;
        for( int i=k; i<m; ++i )
            s += QR[i*n+k]*x[i];

        s = -s/QR[k*n+k];
        for( int i=k; i<m; ++i )
            x[i] += s*QR[i*n+k];
    }

    // solve R*x = y;
    for( int k=n-1; k>=0; --k )
    {
        x[k] /= RDiag[k];
        for( int i=0; i<k; ++i )
            x[i] -= x[k]*QR[i*n+k];
    }
    // return n portion of x
    for( int i=0; i<n; ++i )
        solved_x[i] = x[i];
}


mat *matrix_new(int m, int n)
{
	mat *x = (mat *)malloc(sizeof(mat_t));
	x->v = (float **)malloc(sizeof(float) * m);
	x->v[0] = (float *)calloc(sizeof(float), m * n);
	for (int i = 0; i < m; i++)
		x->v[i] = x->v[0] + n * i;
	x->m = m;
	x->n = n;
	return x;
}
 
void matrix_delete(mat *m)
{
	free(m->v[0]);
	free(m->v);
	free(m);
}
 
void matrix_transpose(mat *m)
{
	for (int i = 0; i < m->m; i++) {
		for (int j = 0; j < i; j++) {
			float t = m->v[i][j];
			m->v[i][j] = m->v[j][i];
			m->v[j][i] = t;
		}
	}
}
 
mat *matrix_copy(float *a, int m, int n)
{
	mat *x = matrix_new(m, n);
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			x->v[i][j] = a[i*n+j];
	return x;
}
 
mat *matrix_mul(mat *x, mat *y)
{
	if (x->n != y->m) return 0;
	mat *r = matrix_new(x->m, y->n);
	for (int i = 0; i < x->m; i++)
		for (int j = 0; j < y->n; j++)
			for (int k = 0; k < x->n; k++)
				r->v[i][j] += x->v[i][k] * y->v[k][j];
	return r;
}
 
mat *matrix_minor(mat *x, int d)
{
	mat *m = matrix_new(x->m, x->n);
	for (int i = 0; i < d; i++)
		m->v[i][i] = 1;
	for (int i = d; i < x->m; i++)
		for (int j = d; j < x->n; j++)
			m->v[i][j] = x->v[i][j];
	return m;
}
 
/* c = a + b * s */
float *vmadd(float a[], float b[], float s, float c[], int n)
{
	for (int i = 0; i < n; i++)
	{
		float cal_val;
		cal_val = a[i] + s * b[i];
		c[i] = cal_val;//a[i] + s * b[i];
	}
	return c;
}
 
/* m = I - v v^T */
mat *vmul(float v[], int n)
{
	mat *x = matrix_new(n, n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			x->v[i][j] = -2 *  v[i] * v[j];
	for (int i = 0; i < n; i++)
		x->v[i][i] += 1;
 
	return x;
}
 
/* ||x|| */
float vnorm(float x[], int n)
{
	float sum = 0;
	for (int i = 0; i < n; i++) sum += x[i] * x[i];
	return sqrt(sum);
}
 
/* y = x / d */
float* vdiv(float x[], float d, float y[], int n)
{
	for (int i = 0; i < n; i++) y[i] = x[i] / d;
	return y;
}
 
/* take c-th column of m, put in v */
float* mcol(mat *m, float *v, int c)
{
	for (int i = 0; i < m->m; i++)
		v[i] = m->v[i][c];
	return v;
}
 
void matrix_show(mat *m)
{
	for(int i = 0; i < m->m; i++) {
		for (int j = 0; j < m->n; j++) {
			printf(" %8.3f", m->v[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
 
void QR(mat *m, float *RM, float *QM)
{
	mat *R,*Q;
	//R = matrix_new(m->m,m->m);
	//Q = matrix_new(m->m,m->n);
	
	mat **q;//[m->m];
	mat *z = m;
	mat *z1;

	q = (mat **)malloc(sizeof(mat)*m->m);
//	for (int k = 0; k < m->m; k ++)
//		q[k] =  (mat *)calloc(1,sizeof(mat));

	for (int k = 0; k < m->n && k < m->m - 1; k++) {
		//float e[m->m], x[m->m], a;
		float *e,*x,a;
		e = (float *)calloc(m->m,sizeof(float));
		x = (float *)calloc(m->m,sizeof(float));

		for (int i = 0; i < m->m; i ++)
		{
			e[i] = x[i] = 0;
		}

		//printf("============== %d ===============\n",k);
		//for (int mm = 0; mm < 5; mm ++)
		//{
		//	for (int nn = 0; nn < 3; nn ++)
		//	{
		//		printf("%f ",z->v[mm][nn]);
		//	}
		//	printf("\n");
		//}

		z1 = matrix_minor(z, k);
		if (z != m) matrix_delete(z);
		z = z1;
 
		mcol(z, x, k);
		a = vnorm(x, m->m);
		if (m->v[k][k] > 0) a = -a;
 
		for (int i = 0; i < m->m; i++)
			e[i] = (i == k) ? 1 : 0;
 
		vmadd(x, e, a, e, m->m);
		vdiv(e, vnorm(e, m->m), e, m->m);
		q[k] = vmul(e, m->m);

		z1 = matrix_mul(q[k], z);
		if (z != m) matrix_delete(z);
		z = z1;

		free(e);
		free(x);
	}
	matrix_delete(z);
//	*Q = q[0];
//	*R = matrix_mul(q[0], m);
	Q = q[0];
	R = matrix_mul(q[0], m);
	for (int i = 1; i < m->n && i < m->m - 1; i++) {
//		z1 = matrix_mul(q[i], *Q);
//		if (i > 1) matrix_delete(*Q);
		z1 = matrix_mul(q[i], Q);
		if (i > 1) matrix_delete(Q);
//		*Q = z1;
		Q = z1;
		matrix_delete(q[i]);
	}
//	matrix_delete(q[0]);
//	z = matrix_mul(*Q, m);
	z = matrix_mul(Q, m);
//	matrix_delete(*R);
	matrix_delete(R);
//	*R = z;
	R = z;
//	matrix_transpose(*Q);
	matrix_transpose(Q);

	//printf("************************\n");
	//matrix_show(Q);
	//printf("************************\n");
	//matrix_show(R);

	for (int i = 0; i < m->m; i ++)
	{
		for (int j = 0; j < m->m; j ++)
		{
			QM[i*m->m+j] = Q->v[i][j];
		}
	}
	for (int i = 0; i < m->m; i ++)
	{
		for (int j = 0; j < m->n; j ++)
		{
			RM[i*m->n+j] = R->v[i][j];
		}
	}
//	matrix_delete(q[0]);
	matrix_delete(Q);
	matrix_delete(R);
//	for (int k = 0; k < m->m; k ++)
//		matrix_delete(q[k]);
//	free(q[0]);
	free(q);
}

int qrdcmp2(float *a, int m, int n, float *c, float *d)
/*Constructs the QR decomposition of a[1..n][1..n]. The upper triangular matrix R is returned in the upper triangle of a, except for the diagonal elements of R which are returned in
d[1..n]. The orthogonal matrix Q is represented as a product of n - 1 Householder matrices
Q1
. . . Qn-1
, where Qj = 1 - uj ? uj /cj . The ith component of uj is zero for i = 1, . . . , j - 1
while the nonzero components are returned in a[i][j] for i = j, . . . , n. sing returns as
true (1) if singularity is encountered during the decomposition, but the decomposition is still
completed in this case; otherwise it returns false (0).*/
{
	int i,j,k;
	float scale,sigma,sum,tau;
	scale = sigma = sum = tau =0.0;
	i=j=k=0;

	int min;
	if (m >= n) 
	{
		min = n;
	}
	else 
	{
		min = m;
	}

	for (k =0;k < min;k++) 
	{
		//printf("K =%d \n N = %d\n", k, n);
		//for (i=k;i<n;i++) scale=FMAX(scale,fabs(a[i][k]));
		for (i=k;i<m;i++) 
			scale=FMAX(scale,fabs(a[i*n+k]));
		//printf ("scale %lf\n", scale);
		if (scale == 0.0) 
		{ //Singular case.
			c[k]=d[k]=0.0;
		}
		else
		{ //Form Qk and Qk ? A.
			//for (i=k;i<n;i++)
			for (i=k;i<m;i++)
			{
				a[i*n+k] /= scale;
			}
			//printf ("a[%d][%d] = %lf\n",i,k, a[i][k]);}
			//orgfor (sum=0.0,i=k;i<n;i++)
			for (sum=0.0,i=k;i<m;i++)
			{
				sum += SQR(a[i*n+k]);
			}
			//printf ("Sum = %lf\n",sum);
			sigma=SIGN(sqrt(sum),a[k*n+k]);
			//printf ("sigma = %lf\n", sigma);
			a[k*n+k] += sigma;
			//printf ("a[%d][%d]= %lf\n",k ,k, a[k][k]);
			c[k]=sigma*a[k*n+k];
			//printf ("c[%d] = %lf\n", k,c[k]);
			d[k] = -scale*sigma;
			//printf ("d[%d] = %lf\n", k,d[k]);
			for (j=k+1;j < n;j++) 
			{
				//printf("j=%d\n",j);
				//orgfor (sum=0.0,i=k;i<n;i++)
				for (sum=0.0,i=k;i<m;i++)
				{
					sum += a[i*n+k]*a[i*n+j];
					//printf("sum = %lf, a[%d][%d] = %lf, a[%d][%d]=%lf\n",sum, i, k, a[i][k], i, j, a[i][j]);
				}
				//printf("sum = %lf\n",sum);
				tau=sum/c[k];
				//printf("tau= %lf\n",tau);
				//org for (i=k;i<n;i++)
				for (i=k;i<m;i++)
				{
					a[i*n+j] -= tau*a[i*n+k];
					// printf("a[%d][%d] = %lf\n", i, j, a[i][j]);
				}
			}
		}
	}

	return 0;
}

 
//float in[][3] = {
//	{ 12, -51,   4},
//	{  6, 167, -68},
//	{ -4,  24, -41},
//	{ -1, 1, 0},
//	{ 2, 0, 3},
//};
 
//int main()
//{
//	mat R, Q;
//	mat x = matrix_copy(in, 5, 3);
//	householder(x, &R, &Q);
// 
//	puts("Q"); matrix_show(Q);
//	puts("R"); matrix_show(R);
// 
//	// to show their product is the input matrix
//	mat m = matrix_mul(Q, R);
//	puts("Q * R"); matrix_show(m);
// 
//	matrix_delete(x);
//	matrix_delete(R);
//	matrix_delete(Q);
//	matrix_delete(m);
//	return 0;
//}