#ifndef _PROCESSRECOG_H_
#define _PROCESSRECOG_H_

#include <opencv2/opencv.hpp>
#include "../common.h"

using namespace std;
using namespace cv;

// struct rect
// {
// 	int xmin;
// 	int ymin;
// 	int xmax;
// 	int ymax;
// };

void recog_video_sparse(string video_path, string cascade_path, vector<string> &dir_path);
vector<int> create_lbp_base(vector<string> &img_pathes, int **base_features);
IplImage *enlarge_local_image(IplImage *src, rect &pos);
void recog_face_lbp(string video_path, string cascade_path, vector<string> &base_names, int **base_features, vector<int> &base_det_flag);
unsigned _stdcall init_detect_model_thread(void* param);
unsigned _stdcall create_feature_lbp_thread(void* param);
unsigned _stdcall create_feature_sparsecoding_thread(void* param);
int alignment(IplImage *image, rect &pos, Mat &shape);
int get_face_feature(IplImage *image, rect &pos, int *feature);
int get_face_feature(IplImage *image, int *feature);
double get_correlation(int *vec, int *vec2, int length);
vector<int> descend_vec(vector<double> &data);
float get_residual(float *dict, int nsamples, int nfeatures, float *test_data, float *xout, int index);
int detect_alignment(IplImage *image, Mat &shape);
int detect_alignment(IplImage *image, vector<float> &face_data);
float *get_alignment_data_sparse(IplImage *image, rect &face, int samplew, int sampleh);
unsigned _stdcall extract_feature_lbp_thread(void* param);
unsigned _stdcall extract_feature_lbp_thread2(void* param);
unsigned _stdcall extract_feature_lbps_thread(void* param);
unsigned _stdcall extract_feature_sp_thread(void* param);
unsigned _stdcall extract_feature_sps_thread(void* param);
unsigned _stdcall delete_feature_lbp_thread(void* param);
unsigned _stdcall delete_feature_lbps_thread(void* param);
unsigned _stdcall delete_feature_sp_thread(void* param);
unsigned _stdcall delete_feature_sps_thread(void* param);
unsigned _stdcall copy_feature_lbp_thread(void* param);

string get_name_from_path(string path);
string get_dir_from_path(string path);

#endif