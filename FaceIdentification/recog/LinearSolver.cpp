#include "StdAfx.h"
#include "LinearSolver.h"
#include "..\Matrix.h"
//#include "Eigenface_f.h"
#include "typedef.h"
//Solve AX = B
// A: A_rows * A_cols
// B: A_rows * 1
// Output x: A_cols * 1
void LinearSolver(float *A, int A_rows, int A_cols, float *B, float *x)
{
	if ((A_rows == 1) && (A_cols == 1))
	{
		for (int i = 0; i < A_rows; i ++)
			x[i] = B[i] / A[0];
		return;
	}

	int p = MIN2(A_rows,A_cols);
	float *RDiag = (float *)calloc(p,sizeof(float));
	float *QR = (float *)calloc(A_rows*A_cols,sizeof(float));
	Dec(A, A_rows,A_cols, QR,RDiag, p);
	solve(QR,B,x,RDiag,A_rows,A_cols);
	free(QR);
	free(RDiag);
	return;

	int i,j;
	float *RM,*QM,*QMT,*QMTB,*RMI;

	//float Training[15] = {12, -51,   4, 6, 167, -68, -4,  24, -41, -1, 1, 0,2, 0, 3};
	//float Proc[15] = {12, -51,   4, 6, 167, -68, -4,  24, -41, -1, 1, 0,2, 0, 3};
	//A_rows = 5;
	//A_cols = 3;

	if ((A_rows == 1) && (A_cols == 1))
	{
		for (i = 0; i < A_rows; i ++)
			x[i] = B[i] / A[0];
		return;
	}

//	float *c = (float *)calloc(A_cols,sizeof(float));
//	float *d = (float *)calloc(A_cols,sizeof(float));
//	
//	for (i = 0; i < A_cols; i ++)
//		c[i] = d[i] = 0;
////	qrdcmp2(A,A_rows,A_cols,c,d);
//	qrdcmp2(Proc,A_rows,A_cols,c,d);
//
//	RM = (float *)calloc(A_rows*A_cols,sizeof(float));
//	RMI = (float *)calloc(A_cols*A_rows,sizeof(float));
//	QM = (float *)calloc(A_rows*A_rows,sizeof(float));
//	QMT = (float *)calloc(A_rows*A_rows,sizeof(float));
//	for (i =0; i < A_rows; i++)
//	{ 
//		for (j=0; j < A_cols; j++)
//		{
//			if (i == j)
//			{
//				RM[i*A_cols+i] = d[i];
//			}
//			else if ( i < j)
//			{
//				RM[i*A_cols+j] = Training[i*A_cols+j];
////				RM[i*A_cols+j] = A[i*A_cols+j];
//			}
//			else
//			{
//				RM[i*A_cols+j] = 0;
//			}
//		}
//	}
//
//	for (int mm = 0; mm < 5; mm ++)
//	{
//		for (int nn = 0; nn < 3; nn ++)
//		{
//			printf("%f ",RM[mm*3+nn]);
//		}
//		printf("\n");
//	}
//	Matrix_Inverse(Proc,RMI,A_rows,A_cols);
//	Matrix_Multiply(Training,A_rows,A_cols,RMI,A_cols,A_rows,QM);
//	for (int mm = 0; mm < 5; mm ++)
//	{
//		for (int nn = 0; nn < 5; nn ++)
//		{
//			printf("%f ",QM[mm*5+nn]);
//		}
//		printf("\n");
//	}
//
//	Matrix_Transpose(QM,QMT,A_rows,A_rows);
//	QMTB = (float *)calloc(A_rows,sizeof(float));
//	Matrix_Multiply(QMT,A_rows,A_rows,B,A_rows,1,QMTB);
//	Matrix_Multiply(RMI,A_cols,A_rows,QMTB,A_rows,1,x);
//
//	free(RM);
//	free(QM);
//	free(RMI);
//	free(QMT);
//	free(QMTB);
//	free(c);
//	free(d);


	mat *Src;//*Q,*R;

	Src = matrix_new(A_rows,A_cols);
	//Q = matrix_new(A_rows,A_rows);
	//R = matrix_new(A_cols,A_cols);

	for (i = 0; i < A_rows; i ++)
	{
		for (j = 0; j < A_cols; j ++)
		{
			Src->v[i][j] = A[i * A_rows + j];
		}
	}

//	QR(Src,R,Q);
	
	QM = (float *)calloc(A_rows*A_rows,sizeof(float));
	QMT = (float *)calloc(A_rows*A_rows,sizeof(float));
	RM = (float *)calloc(A_rows*A_cols,sizeof(float));
	RMI = (float *)calloc(A_cols*A_rows,sizeof(float));
	//QR(Src,RM,QM);
	//for (i = 0; i < A_rows; i ++)
	//{
	//	for (j = 0; j < A_rows; j ++)
	//	{
	//		QM[i * A_rows + j] = Q->v[i][j];
	//	}
	//}
	//for (i = 0; i < A_rows; i ++)
	//{
	//	for (j = 0; j < A_cols; j ++)
	//	{
	//		RM[i * A_rows + j] = R->v[i][j];
	//	}
	//}
	
	Matrix_Transpose(QM,QMT,A_rows,A_rows);
	QMTB = (float *)calloc(A_rows,sizeof(float));
	Matrix_Multiply(QMT,A_rows,A_rows,B,A_rows,1,QMTB);

	Matrix_Inverse(RM,RMI,A_rows,A_cols);

	Matrix_Multiply(RMI,A_cols,A_rows,QMTB,A_rows,1,x);


	free(RM);
	free(QM);
	free(RMI);
	free(QMT);
	free(QMTB);

	matrix_delete(Src);
	//matrix_delete(R);
	//matrix_delete(Q);
}