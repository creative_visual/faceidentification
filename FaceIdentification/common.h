#ifndef _COMMON_H_
#define _COMMON_H_

#include <opencv2/opencv.hpp>
#include <vector>

using namespace std;
using namespace cv;

struct rect
{
	int xmin;
	int ymin;
	int xmax;
	int ymax;
};

void get_subdirs(string path, vector<string> &dirs);
void get_subdirs2(string path, vector<string> &dirs);
void norm_matrix(float *data, int width, int height);
void get_path_files(string path, vector<string> &files);
void get_path_files2(string path, vector<string> &files);
float *get_img_vector(IplImage *image, int samplew, int sampleh);
void draw_recog_info(IplImage *image, rect face, string info, CvFont font);
void get_image_pathes(string dir_path, vector<string> &image_pathes);

#endif