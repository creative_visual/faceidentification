
// FaceIdentificationDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceIdentification.h"
#include "FaceIdentificationDlg.h"
#include "afxdialogex.h"
#include "DetParam.h"
#include "RecogParam.h"
#include "TestDB.h"
#include "common.h"
#include "recog/ProcessRecog.h"
#include "CvxText.h"
#include <io.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//全局变量
CWinThread *g_thread_VideoPlay[2];

CWinThread *g_thread_detection = NULL;
CWinThread *g_thread_Recognition = NULL;
extern vector<string> g_face_names, g_dirpathes, g_attention_names, g_attention_pathes;
extern vector<vector<int>> g_recog_flag;
extern float *g_dict , *g_AT , *g_ATA ;
extern vector<int> g_class_flag;
extern int g_nsamples, g_samplew, g_sampleh;

extern vector<string> g_undet_pathes;
extern int g_last_db_size, g_del_idx;
extern int **g_base_features;
extern int totalDim;
extern vector<vector<int>> g_tmp_bfeatures; // lbp
extern vector<vector<float>> g_sp_bfeatures;// sparse_coding
extern HANDLE g_init_model_hdl,g_create_lbp_hdl, g_create_sp_hdl, g_extract_lbps_hdl, g_extract_sps_hdl;
extern vector<HANDLE> g_extract_feature_hdls;
extern CRITICAL_SECTION g_csf;
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

HWND g_dlg;

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CFaceIdentificationDlg 对话框


CFaceIdentificationDlg::CFaceIdentificationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFaceIdentificationDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	MenuNum=-1;

	m_dspbuf=NULL;
	m_imageBack=NULL;
	LoadedPicture=NULL;
	LoadedPictureCopy[0]=NULL;
	LoadedPictureCopy[1]=NULL;
	image_settings=NULL;

	videoState[0]=false;
	videoState[1]=false;
	picture_video_f[0]=false;
	picture_video_f[1]=false;
	CensusFlag=false;

	g_thread_VideoPlay[0]=NULL;//检测视频播放
	g_thread_VideoPlay[1]=NULL;//识别视频播放

	m_thread_showVideoData = NULL;
	toCensusFlag=false;

	//CurrentAlgorithm=0;
	m_sel_algorithm = HD_LBP;//sparse_coding;
	ShowDropDownListFlag=false;
	m_dbDrowDownFlag=false;
	m_sexDropDownFlag = false;
	DetectionOver=true;
	RecognitionOver=true;
	stopThreadFlag=false;
	MenuFlag=false;
	g_dict=NULL;
	play_speed=NULL;
	FastForwardRewindFlag=false;
	FastForwardRewindNum=-1;
	draftingFlag=false;
	FinishDetectionFlag=false;
	FinishRecognitionFlag=false;
	m_DetectionForRecognition=NULL;
	QueryRecognitionFlag=false;
	CurrentDetectionCensusPic=NULL;
}

void CFaceIdentificationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LISTBASE, m_list);
	DDX_Control(pDX, IDC_EID, m_id);
	DDX_Control(pDX, IDC_ENAME, m_name);
	DDX_Control(pDX, IDC_ESEX, m_sex);
	DDX_Control(pDX, IDC_EAGE, m_age);

	DDX_Control(pDX, IDC_DBINFOGROUP, m_group);
	DDX_Control(pDX, IDC_SID, m_sid);
	DDX_Control(pDX, IDC_SNAME, m_sname);
//	DDX_Control(pDX, IDC_SSEX, m_ssex);
	DDX_Control(pDX, IDC_SAGE, m_sage);

	DDX_Text(pDX, IDC_EID, m_nid);
	DDX_Text(pDX, IDC_ENAME, m_strname);
	DDX_Text(pDX, IDC_EAGE, m_nage);
//	DDX_Text(pDX, IDC_ESEX, m_bsex);
}

BEGIN_MESSAGE_MAP(CFaceIdentificationDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_WM_CTLCOLOR()
	ON_WM_CLOSE()
	ON_WM_MOUSEMOVE()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_DETE, &CFaceIdentificationDlg::OnLvnItemchangedListDete)
	ON_NOTIFY(NM_CLICK, IDC_LISTBASE, &CFaceIdentificationDlg::OnNMClickListbase)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LISTBASE, &CFaceIdentificationDlg::OnChoose)
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


// CFaceIdentificationDlg 消息处理程序

BOOL CFaceIdentificationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	theApp.pFaceIdentificationDlg=this;

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	MyInit();//初始化设置

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CFaceIdentificationDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CFaceIdentificationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
	if(m_dspbuf )
	{
		CPaintDC dc(GetDlgItem(IDC_STATIC_BACKGROUND));

		CDC dcMem;
		CBitmap bmpMem;
		dcMem.CreateCompatibleDC(&dc);
		bmpMem.CreateCompatibleBitmap(&dc, m_dspbuf->width, m_dspbuf->height);
		dcMem.SelectObject(&bmpMem);

		int linebyte;
		int w = m_dspbuf->width;
		int h = m_dspbuf->height;
		linebyte = m_dspbuf->widthStep;

		bmpMem.SetBitmapBits(linebyte * h, m_dspbuf->imageData);


		BOOL f=GetDlgItem(IDC_LIST_DETE)->IsWindowVisible();
		if (f==TRUE&&MenuNum==2&&toCensusFlag==true)
		{
			dc.BitBlt(0, 0, ImageDisplayArea_w,ImageDisplayArea_y+50, &dcMem, 0, 0, SRCCOPY);
// 			dc.BitBlt(0, y, x,h, &dcMem, 0, y, SRCCOPY);
// 			dc.BitBlt(x+w,y, 270,h, &dcMem, x+w,y, SRCCOPY);
// 			dc.BitBlt(0,y+h, m_Width,m_Height-y-h, &dcMem, 0,y+h, SRCCOPY);
			GetDlgItem(IDC_LIST_DETE)->ShowWindow(SW_SHOWNORMAL);
		}
		else
		{
			dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);
		}

//		dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);
//		dc.TransparentBlt(0, 0, w,h, &dcMem, 0, 0,w,h, SRCCOPY);
		bmpMem.DeleteObject();
		dcMem.DeleteDC(); 
	}

}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CFaceIdentificationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CFaceIdentificationDlg::MyInit()//初始化设置
{
	COLORREF clr = RGB(225,225,225);
	//SetDialogBkColor(clr/*, FALSE*/);

	m_dialog_w=960;
	m_dialog_h=611;//540+上边菜单栏71

	CString path; 
	GetModuleFileName(NULL,path.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	path.ReleaseBuffer(); 
	int pos = path.ReverseFind('\\'); 
	CurrentPath = path.Left(pos);

	int iWidth = GetSystemMetrics(SM_CXSCREEN); 
	int iHeight = GetSystemMetrics(SM_CYSCREEN); 
	int x,y;

	x=(iWidth-m_dialog_w)/2;
	y=(iHeight-m_dialog_h)/2;

	if (iWidth<m_dialog_w||iHeight<m_dialog_h)
	{
		ShowWindow(SW_MAXIMIZE);
	}
	else
	{
		this->SetWindowPos(0,x,y,m_dialog_w,m_dialog_h,SWP_SHOWWINDOW);
	}

	CRect clrect;
	CvSize dst_cvsize;


	GetClientRect(&clrect);

	m_client_w=clrect.Width();
	m_client_h=clrect.Height();

	dst_cvsize.width=clrect.Width();
	dst_cvsize.height=clrect.Height();

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	m_dspbuf=NULL;
	m_imageBack=NULL;
	cvReleaseImage(&m_dspbuf);
	m_dspbuf=cvCreateImage(dst_cvsize,8,4);
	GetDlgItem(IDC_STATIC_BACKGROUND)->SetWindowPos(0,0,0,dst_cvsize.width,dst_cvsize.height,SWP_SHOWWINDOW);

	CreateNewDir(CurrentPath+"\\Record");
	CreateNewDir(CurrentPath+"\\DB");
 	rect alg;
 	alg.xmin = 11, alg.xmax = 160, alg.ymin = 45, alg.ymax = 45 + 25;
	string arr_path = CurrentPath+"\\resource\\arrow.bmp";
//	m_recog_alg.init(alg, arr_path, m_dspbuf);

	m_baseListX = 11, m_baseListY = 180;
	GetDlgItem(IDC_LISTBASE)->MoveWindow(m_baseListX, m_baseListY,dst_cvsize.width-11, dst_cvsize.height-200, TRUE);
	((CListCtrl *)GetDlgItem(IDC_LISTBASE))->SetBkColor(clr);
	m_list.SetTextBkColor(clr);

	m_db_image = cvCreateImage(cvSize(145, 145), 8, 3);
	InitPictureColour(m_db_image,0,m_db_image->width,0,m_db_image->height,240,240,240);//初始化背景色
	m_del_btn_x = 680, m_del_btn_y = 220;//210;
	m_ok_btn_x = 780, m_ok_btn_y = 220;//210;
	m_del_all_x = 780, m_del_all_y = m_client_h - 70;
	int sx = 330, ex = 380;
	m_sid.MoveWindow(sx, 100, 35, 15);
	m_sname.MoveWindow(sx, 138, 35, 15);
//	m_ssex.MoveWindow(sx, 176, 35, 15);
	m_sage.MoveWindow(sx+200, 100, 35, 15);

	m_id.MoveWindow(ex,  120,80,30);
	m_name.MoveWindow(ex,158,80,30);
	m_sex.MoveWindow(ex, 196,80,30);
	m_age.MoveWindow(ex+200, 120, 80, 30);
	m_group.MoveWindow(290, 80, 600, 170);
	m_list.MoveWindow(11, 280, m_client_w-22, 230);

	m_img_list.Create(35,35, ILC_COLOR|ILC_COLOR8|ILC_COLOR24/*|ILC_COLOR32*/,100,100);

	m_list.InsertColumn(0, "图片", 0, 50);
	m_list.InsertColumn(1, "id号", 0, 50);
	m_list.InsertColumn(2, "姓名", 0, 100);
	m_list.InsertColumn(3, "性别", 0, 50);
	m_list.InsertColumn(4, "年龄", 0, 50);
	m_list.InsertColumn(5, "是否关注", 0, 70);
	
	m_battention = 1, m_attention_x = 530, m_attention_y = 165, m_attention_l = 30;

	m_nid = "", m_strname = "", m_bsex = "男", m_nage = "";
	g_dlg = this->m_hWnd;

	UpdateData(FALSE);

	m_list.SetImageList(&m_img_list, /*LVSIL_NORMAL*/LVSIL_SMALL );
	g_init_model_hdl = (HANDLE)_beginthreadex(NULL, 0, init_detect_model_thread, NULL, NULL, NULL); // lbp 识别算法初始化
	InitializeCriticalSection(&g_csf);

	CvSize size;
	size.height = m_dspbuf->height, size.width = m_dspbuf->width;
	cvReleaseImage(&m_imageBack);
	m_imageBack=  cvCreateImage(size, m_dspbuf->depth, 4);

	InitSetValue();

	BasicBackGround();
	ClickMenu(0);//默认选择 检索

	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	Invalidate(FALSE);

	size.height = 17, size.width = 30;
	cvReleaseImage(&play_speed);
	play_speed=  cvCreateImage(size, m_dspbuf->depth, 4);
	SetDspbufPic(play_speed,CurrentPath+"\\resource\\play_speed.jpg", 0, 0, 17,15, 0,0, false);
	SetDspbufPic(play_speed,CurrentPath+"\\resource\\play_speed.jpg", 30, 0, 17,15, 0,0, true);
}

void CFaceIdentificationDlg::SetPointColour(IplImage* dst,int x,int y,int r,int g,int b)
{
	dst->imageData[ y* dst->widthStep + dst->nChannels * x + 2 ] =(BYTE)r;
	dst->imageData[ y* dst->widthStep + dst->nChannels * x + 1 ] =(BYTE)g;
	dst->imageData[ y* dst->widthStep + dst->nChannels *x  ] =(BYTE)b;
}
void CFaceIdentificationDlg::SetPointColour(IplImage* dst,int x,int y,IplImage* src,int p_x,int p_y)
{
	if (x<dst->width&&y<dst->height&&p_x<src->width&&p_y<src->height)
	{
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 2 ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x + 2 ];
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 1 ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x + 1 ];
		dst->imageData[ y* dst->widthStep + dst->nChannels *x  ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x  ];
	}

}

void CFaceIdentificationDlg::ShowImageInCenter(IplImage* dst, IplImage* src,bool f)
{
	int width,height;
	width=src->width;
	height=src->height;

	double rate=((double)dst->width)/width;
	if (rate<1)
	{
		width=dst->width;
		height=src->height*rate;
	}

	rate=((double)dst->height)/height;
	if (rate<1)
	{
		width=width*rate;
		height=dst->height;
	}
	if (f==true)
	{
			InitPictureColour(dst,0,dst->width,0,dst->height,215,215,215);//初始化背景色
	}


	rate=((double)src->width)/width;
	int x,y;
	x=(dst->width-width)/2.0;
	y=(dst->height-height)/2.0;

	int i,j;
	int i1,j1;

	for (i=x;i<(x+width);i++)
	{
		for (j=y;j<(y+height);j++)
		{
			i1=rate*(i-x);
			j1=rate*(j-y);
			SetPointColour(dst,i,j,src,i1,j1);
		}
	}
}
void CFaceIdentificationDlg::ShowImageInCenter(IplImage* dst, IplImage* src,bool f,ALLFACE face)
{
	int width,height;
	width=src->width;
	height=src->height;

	double rate=((double)dst->width)/width;
	if (rate<1)
	{
		width=dst->width;
		height=src->height*rate;
	}

	rate=((double)dst->height)/height;
	if (rate<1)
	{
		width=width*rate;
		height=dst->height;
	}
	if (f==true)
	{
		InitPictureColour(dst,0,dst->width,0,dst->height,215,215,215);//初始化背景色
	}


	rate=((double)src->width)/width;
	int x,y;
	x=(dst->width-width)/2.0;
	y=(dst->height-height)/2.0;

	int i,j;
	int i1,j1;

	for (i=x;i<(x+width);i++)
	{
		for (j=y;j<(y+height);j++)
		{
			i1=rate*(i-x);
			j1=rate*(j-y);
			SetPointColour(dst,i,j,src,i1,j1);
		}
	}

	if (face.name.size()>0)
	{
		CvFont font;//以下用文字标识说明
		double hScale=1, vScale=1;
		int lineWidth=2;
		cvInitFont(&font,CV_FONT_HERSHEY_PLAIN  |CV_FONT_HERSHEY_COMPLEX /*CV_FONT_ITALIC*/,hScale,vScale,1,lineWidth,8);
		for (i=0;i<(int)face.name.size();i++)
		{
			char *buf;
			if (face.name[i].GetLength()>0)
			{
				buf = (LPSTR)(LPCTSTR)face.name[i];
				int point_x=face.face.faces[i].xmin/rate+x;
				int point_y=face.face.faces[i].ymin/rate+y;
				cvPutText(dst, buf, cvPoint(point_x, point_y - 5), &font, CV_RGB(255,0,0));
			}
			
		}
	}
}
void CFaceIdentificationDlg::ShowImageInCenter(IplImage* dst,int x,int y,int w,int h, IplImage* src)
{
	int width,height;
	width=src->width;
	height=src->height;

	double rate=((double)w)/width;
	if (rate<1)
	{
		width=w;
		height=src->height*rate;
	}

	rate=((double)h)/height;
	if (rate<1)
	{
		width=width*rate;
		height=h;
	}

//	InitPictureColour(dst,x,x+w,y,y+h,215,215,215);//初始化背景色

	rate=((double)src->width)/width;

	x=x+(w-width)/2.0;
	y=y+(h-height)/2.0;

	int i,j;
	int i1,j1;

	for (i=x;i<(x+width);i++)
	{
		for (j=y;j<(y+height);j++)
		{
			i1=rate*(i-x);
			j1=rate*(j-y);
			SetPointColour(dst,i,j,src,i1,j1);
		}
	}

	if (picture_video_flag==true&&videoState==false)
	{
		showVideoPlayAndStop(playPath,play_r);
	}

	if (MenuNum==0)
	{
		int x1,y1;
		x1=m_client_w/2.0;
		y1=ImageDisplayArea_h/2.0+openFile_y+27;

		int r2=(mousePoint.x-x1)*(mousePoint.x-x1)+(mousePoint.y-y1)*(mousePoint.y-y1);

		if ((videoState[MenuNum]==true&&r2<stop_r*stop_r))
		{
			showVideoPlayAndStop(stopPath,stop_r);
		}
	}

//	Invalidate(FALSE);
	UpdateMyRectangle(0,ImageDisplayArea_w,ImageDisplayArea_y,ImageDisplayArea_h+ImageDisplayArea_y);
}

void CFaceIdentificationDlg::DrawFace(int num, IplImage* src,CString AnalysisTime)
{
	CString Path;//文件路径
	char *filePath;
	FILE *File=NULL;

	CString tmp;
	tmp.Format("%09d",num);
	Path=CurrentPath+"\\Record\\"+AnalysisTime+"\\"+tmp+".txt";
	filePath=Path.GetBuffer(Path.GetLength());

	int xmin,ymin,xmax,ymax;

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);

		for (int i=0;i<n;i++)
		{	
			fread(&xmin,sizeof(int),1,File);	
			fread(&ymin,sizeof(int),1,File);	
			fread(&xmax,sizeof(int),1,File);	
			fread(&ymax,sizeof(int),1,File);	

			myDrawrectangular( src,xmin,ymin,xmax,ymax,255,255,255);
		}
		fclose(File);
	}
}
void CFaceIdentificationDlg::DrawFace( IplImage* src,ALLFACE face)
{
	int n=(int)face.face.faces.size();

	for (int i=0;i<n;i++)
	{	
		if (face.name[i]!="")
		{
			myDrawrectangular( src,face.face.faces[i].xmin,face.face.faces[i].ymin,face.face.faces[i].xmax,face.face.faces[i].ymax,255,0,0);
		}
		else
		{
			myDrawrectangular( src,face.face.faces[i].xmin,face.face.faces[i].ymin,face.face.faces[i].xmax,face.face.faces[i].ymax,255,255,255);
		}
		
	}


}
void CFaceIdentificationDlg::GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos)
{
	int i,j,i2,j2;
	int linebyte = dspbuf->widthStep;
	int linebytesclae = imagescale->widthStep;
	char *temp_dsp = dspbuf->imageData, *temp_scale = imagescale->imageData;
	UINT *dsp = (UINT *)temp_dsp, *scale = (UINT *)temp_scale;

	if( imagescale->width <= dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width <= dspbuf->width && imagescale->height > dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = 0;
		for( i = 0; i < dspbuf->height; i++)
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = 0, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = 0; j < dspbuf->width; j++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height > dspbuf->height )
	{
		DWORD oldtime = GetTickCount();
		for( i = 0; i < dspbuf->height; i++)
			for( j = 0; j < dspbuf->width; j++ )
			{
				if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j+hpos)];
				}
			}
			DWORD time = GetTickCount() - oldtime;
			int kkk = 0;
	}
	temp_dsp = NULL, temp_scale = NULL;
	dsp = NULL,scale = NULL;
}
void CFaceIdentificationDlg::InitPictureColour(IplImage* image,int l,int r,int t,int b,int R,int G,int B)//初始化图片一个区域
{
	int i,j;
	for(i = t; i <b; i++)
		for(j = l; j <r; j++)
		{
			SetPointColour(image,j ,i,R,G,B);
		}
}
void CFaceIdentificationDlg::BasicBackGround()
{
	InitPictureColour(m_imageBack,0,m_imageBack->width,0,m_imageBack->height,240,240,240);//初始化背景色

	SetBackPic(CurrentPath+"\\resource\\logo.jpg",MenuLeft_x+MenuLeft_w*2+MenuLength+50,5,30,30,0,0,false);
	SetBackPic(CurrentPath+"\\resource\\title.jpg",MenuLeft_x+MenuLeft_w*2+MenuLength+80,5,160,30,0,0,false);

	InitMenu();
}
void CFaceIdentificationDlg::InitMenu()
{
	int i,j;
	CString pathname;

	pathname=CurrentPath+"\\resource\\title_left01.jpg";

	SetBackPic(pathname,MenuLeft_x,MenuLeft_y,MenuLeft_w,MenuLeft_h,2,0,false);
	SetBackPic(pathname,MenuLeft_x+MenuLeft_w+MenuLength-1,MenuLeft_y,MenuLeft_w,MenuLeft_h,2,0,true);

	for (i=(MenuLeft_x+MenuLeft_w);i<(MenuLeft_x+MenuLength);i++)
	{
		for (j=MenuLeft_y;j<BlueLine;j++)
		{
			SetPointColour(m_imageBack,i,j,m_imageBack,MenuLeft_x+MenuLeft_w-1,j);
		}
	}

	for (i=0;i<m_client_w;i++)
	{
		for (j=BlueLine;j<(BlueLine+3);j++)
		{
			SetPointColour(m_imageBack,i,j,58,120,179);//菜单下蓝色线
		}		
	}

	CRect rect;
	for (i=0;i<4;i++)
	{	
		((CStatic *)GetDlgItem(1012+i))->GetWindowRect(&rect);
		int x,y;
		x=MenuLeft_x+(MenuLeft_w+MenuSingleLength)*i+(MenuLeft_w+MenuSingleLength-rect.Width())/2;
		y=MenuLeft_y+(MenuLeft_h-rect.Height())/2+3;
		((CStatic *)GetDlgItem(1012+i))->SetWindowPos(0,x,y,rect.Width(),rect.Height(),SWP_SHOWWINDOW);
	}
}
void CFaceIdentificationDlg::ClickMenu(int num)//初始化菜单按钮
{
	if (num==MenuNum)
	{
		return;
	}
	showPictureFlag=true;
	
	MenuFlag=true;
	bool flag=false;
	if (MenuNum==0)
	{		
		if (g_thread_detection)
		{
			flag=true;
			SuspendThread(g_thread_detection->m_hThread);
		}
	}
	bool flag_r=false;
	if (MenuNum==1)
	{		
		if (g_thread_Recognition)
		{
			flag_r=true;
			SuspendThread(g_thread_Recognition->m_hThread);
		}
	}
	CHeaderCtrl* pHeaderCtrl = m_list.GetHeaderCtrl();
	int Column=pHeaderCtrl->GetItemCount();

	if (Column>1)
	{
		for   (int  j=0;j < Column;j++)   
		{   
			m_list.DeleteColumn(0);   
		}
	}
	int iIndex=0;
	iIndex =m_list.GetItemCount();
	for (int j=0;j<iIndex;j++)//删除所有行
	{
		m_list.DeleteItem(0);
	}

	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_HIDE);
	int i,j;
	if (MenuNum==2)
	{
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(SW_HIDE);
	}

	//if( MenuNum != 3 )
		ShowSettingsCtrl(FALSE);

	InitPictureColour(m_imageBack,0,m_imageBack->width,BlueLine+3,ImageDisplayArea_y,240,240,240);//初始化背景色



	if (num==0)//第一个菜单
	{
		SetBackPic(MenuLeft_blue,MenuLeft_x,MenuLeft_y,MenuLeft_w,MenuLeft_h,0,0,false);
	} 
	else
	{
		SetBackPic(MenuLeft_b,MenuLeft_x+(MenuLeft_w+MenuSingleLength)*num,MenuLeft_y,MenuLeft_w,MenuLeft_h,0,0,false);
	}

	if (num==3)//第4个菜单
	{
		SetBackPic(MenuLeft_blue,MenuLeft_x+MenuLeft_w+MenuLength-1,MenuLeft_y,MenuLeft_w,MenuLeft_h,0,0,true);
	} 
	else
	{
		SetBackPic(MenuLeft_b,MenuLeft_x+(MenuLeft_w+MenuSingleLength)*(num+1)-1,MenuLeft_y,MenuLeft_w,MenuLeft_h,0,0,true);
	}


	for (i=(MenuLeft_x+MenuLeft_w+(MenuLeft_w+MenuSingleLength)*num);i<(MenuLeft_x+(MenuLeft_w+MenuSingleLength)*num+MenuSingleLength);i++)
	{
		for (j=MenuLeft_y;j<BlueLine;j++)
		{
			SetPointColour(m_imageBack,i,j,m_imageBack,MenuLeft_x+MenuLeft_w+(MenuLeft_w+MenuSingleLength)*num-1,j);
		}
	}


	if ((MenuNum==0||MenuNum==1)&&CurrentFilePath.GetLength()>1)
	{
		if (picture_video_f[MenuNum]==true)
		{
			if (videoState[MenuNum]==true)
			{
				videoState[MenuNum]=false;
				if (g_thread_VideoPlay[MenuNum])
				{
					SuspendThread(g_thread_VideoPlay[MenuNum]->m_hThread);
				}			
				Invalidate(FALSE);
			}
			
		}
//		MyCopyImage(LoadedPictureCopy[MenuNum], LoadedPicture);
		MyCopyImage(LoadedPictureCopy[MenuNum],m_dspbuf,0,ImageDisplayArea_y,ImageDisplayArea_w,ImageDisplayArea_h);
		if (num==2)
		{
			toCensusFlag=true;
		}
	}//切换菜单前 先暂停正在播放

	MenuNum=num;
	
	if (MenuNum==0||MenuNum==1)
	{
		if (CurrentFilePath.GetLength()<1)
		{
			ShowWelcome();
		}
		else
		{
			InitPictureColour(m_imageBack,0,m_imageBack->width,ImageDisplayArea_y,m_imageBack->height,215,215,215);//初始化背景色
		}
		
	}
	else
	{
		InitPictureColour(m_imageBack,0,m_imageBack->width,ImageDisplayArea_y,m_imageBack->height,240,240,240);//初始化背景色
	}

	if (MenuNum==0)
	{
		Detection_Recognition_flag=false;

		CurrentFilePath=DetectionFilePath;
		CurrentFileName=DetectionFileName;
		picture_video_flag=picture_video_f[MenuNum];
		SetBackPic(openFile,openFile_x,openFile_y,myButton_w,myButton_h,0,0,false);
//		SetBackPic(beginDect,beginDect_x,beginDect_y,myButton_w,myButton_h,0,0,false);

		if (DetectionOver==false)
		{
			((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_SHOWNA);
		}
	}
	else if (MenuNum==1)
	{
		Detection_Recognition_flag=true;

		CurrentFilePath=RecognitionFilePath;
		CurrentFileName=RecognitionFileName;
		picture_video_flag=picture_video_f[MenuNum];
		SetBackPic(openFile,openFile_x,openFile_y,myButton_w,myButton_h,0,0,false);
		SetBackPic(beginRecognition,beginDect_x,beginDect_y,myButton_w,myButton_h,0,0,false);
		if (RecognitionOver==false)
		{
			((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_SHOWNA);
		}
	}
	else if (MenuNum==2)
	{
		SetBackPic(detectionQuery,openFile_x,openFile_y,myButton_w,myButton_h,0,0,false);
		SetBackPic(recognitionQuery,beginDect_x,beginDect_y,myButton_w,myButton_h,0,0,false);	
	}
	else if( MenuNum == 3 )
	{
		m_list.InsertColumn(0, "图片", 0, 50);
		m_list.InsertColumn(1, "id号", 0, 50);
		m_list.InsertColumn(2, "姓名", 0, 100);
		m_list.InsertColumn(3, "性别", 0, 50);
		m_list.InsertColumn(4, "年龄", 0, 50);
		m_list.InsertColumn(5, "是否关注", 0, 70);
		//SetBackPic(m_detParamBtmPath,openFile_x,openFile_y,myButton_w,myButton_h,0,0,false);
		//SetBackPic(m_recogParamBtmPath,beginDect_x,beginDect_y,myButton_w,myButton_h,0,0,false);
// 		SetBackPic(m_baseBtmPath, m_baseBtnX,m_baseBtnY,myButton_w,myButton_h,0,0,false);
// 		SetBackPic(m_createDBPath, m_createDBX, m_createDBY, myButton_w, myButton_h, 0,0,false);
// 		SetBackPic(m_detParamBtmPath, m_detParamX, m_detParamY, myButton_w,myButton_h, 0,0, false);
// 		SetBackPic(m_recogParamBtmPath, m_recogParamX, m_recogParamY, myButton_w,myButton_h, 0,0, false);

		ShowSettingsCtrl(TRUE);
		SetBackPic(AlgorithmTitle, 10, m_recogParamY, 100,25, 0,0, false);
		SetBackPic(AlgorithmPath[m_sel_algorithm], 110, m_recogParamY, /*150*/90,25, 0,0, false);
		SetBackPic(drop_down, drop_down_x, drop_down_y, drop_down_w,drop_down_h, 0,0, false);
		SetBackPic(drop_down, m_dbDropDownX, m_dbDropDownY, drop_down_w,drop_down_h, 0,0, false);
	}

	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	

	if ((MenuNum==0&&DetectionFilePath.GetLength()>1)||(MenuNum==1&&RecognitionFilePath.GetLength()>1))
	{
//		stopPlayVideo(g_thread_VideoPlay[MenuNum]);
		if (picture_video_f[MenuNum]==true)
		{
			MyCopyImage(LoadedPicture, LoadedPictureCopy[MenuNum]);
			showCurrentPicture();
		}
		else
		{
			SetWindowText("FaceIdentification - "+CurrentFileName);
			showOpenfile();
//			showPicture();
		}
		
//		showCurrentPicture();//直接从其他菜单切换回来时，自动显示之前打开的图片，或者视频的某一帧
	}
	else if (MenuNum==2)
	{
		InitPictureColour(m_dspbuf,0,ImageDisplayArea_w,ImageDisplayArea_y,ImageDisplayArea_h+ImageDisplayArea_y,240,240,240);//初始化背景色

		if (Detection_Recognition_flag==false)//从检测界面直接跳转到统计界面
		{
			GetAnalysisTime(DetectionAnalysisTime,DetectionTxtPath);
			if (DetectionAnalysisTime!="#####")
			{
				ShowCensusData(DetectionFilePath,DetectionAnalysisTime);
			}
		}
		else if (Detection_Recognition_flag==true)//从识别界面直接跳转到统计界面
		{
			GetAnalysisTime(RecognitionAnalysisTime,RecognitionTxtPath[m_sel_algorithm]);
			if (RecognitionAnalysisTime=="#####")
			{
				GetAnalysisTime(RecognitionAnalysisTime,RecognitionTxtPath[1-m_sel_algorithm]);
			}
			if (RecognitionAnalysisTime!="#####")
			{
				ShowRecognitionCensusData(RecognitionFilePath,RecognitionAnalysisTime);
			}
			else if (RecognitionOver==false)//正在识别
			{
				CurrentRecognitionVideoCensusData();
			}
		}

	}
	else if( MenuNum == 3 )
	{
		if (m_db_image)
		{
			MyCopyImage(m_dspbuf,30,100,m_db_image);
		}
		m_list.MoveWindow(11, 280, m_client_w-22, 230);
		if (m_db_info.size()>0)
		{
			originalDBImages();
		}
	}

	Invalidate(FALSE);
	MenuFlag=false;
	if (flag==true)
	{
		ResumeThread(g_thread_detection->m_hThread);
	}
	if (flag_r==true)
	{
		ResumeThread(g_thread_Recognition->m_hThread);
	}

	showPictureFlag=false;
}
void CFaceIdentificationDlg::InitSetValue()//初始化设置变量的值
{
	LOGFONT   lf;   
	memset(&lf,   0,   sizeof(LOGFONT));   
	lf.lfHeight   =20;         //字体的高
	lf.lfWidth    =10;
//	lf.lfWeight   = 700;         //粗体
	lstrcpy(lf.lfFaceName, _T("宋体"));
	font.CreateFontIndirect(&lf);
//	font.CreatePointFont(100,_T("Verdana"),NULL);//设置字体

	MenuLeft_x=10;
	MenuLeft_y=5;
	MenuLeft_w=18;
	MenuLeft_h=34;
	MenuSingleLength=120;
	MenuLength=MenuSingleLength*4+MenuLeft_w*3;

	BlueLine=MenuLeft_y+MenuLeft_h;

	MenuLeft_gray=CurrentPath+"\\resource\\title_left01.jpg";
	MenuLeft_b=CurrentPath+"\\resource\\title_left02.jpg";//3d凸显蓝色边缘
	MenuLeft_blue=CurrentPath+"\\resource\\title_left03.jpg";//无3d效果蓝色边缘

	openFile=CurrentPath+"\\resource\\open.jpg";
//	beginDect=CurrentPath+"\\resource\\beginDect.jpg";
	beginRecognition=CurrentPath+"\\resource\\beginRecognition.jpg";
	openFile_x=10,openFile_y=BlueLine+5,myButton_w=90,myButton_h=25;
	beginDect_x=120,beginDect_y=BlueLine+5;
	detectionQuery=CurrentPath+"\\resource\\DetectionQuery.jpg";
	recognitionQuery=CurrentPath+"\\resource\\RecognitionQuery.jpg";
	m_detParamBtmPath = CurrentPath+"\\resource\\det_param.jpg";
	m_recogParamBtmPath = CurrentPath+"\\resource\\recog_param.jpg";
	m_createDBPath = CurrentPath+"\\resource\\create_db.jpg";
	m_baseBtmPath = CurrentPath + "\\resource\\base_dir.jpg";
	m_dbImgPath = CurrentPath + "\\resource\\create_blacklist.jpg";
	m_dbTxtPath = CurrentPath + "\\resource\\sel_txt.jpg";

	m_del_path = CurrentPath + "\\resource\\del_img.jpg";
	m_ok_path = CurrentPath + "\\resource\\info_ok.jpg";
	m_del_all_path = CurrentPath + "\\resource\\delete_all.jpg";
	m_test_db_path = CurrentPath + "\\resource\\test_db.jpg";
	m_black_list_npath = CurrentPath + "\\resource\\black_list_n.bmp";
	m_black_list_ypath = CurrentPath + "\\resource\\black_list_y.bmp";
	m_fsex_path = CurrentPath + "\\resource\\woman.jpg";
	m_msex_path = CurrentPath + "\\resource\\man.jpg";
	
	m_baseBtnX=300, m_baseBtnY=BlueLine+5;
	m_dbDropDownX = m_baseBtnX + myButton_w, m_dbDropDownY = BlueLine + 5;
	m_createDBX = m_baseBtnX+150, m_createDBY = BlueLine+5;
	m_detParamX = m_createDBX+110, m_detParamY = BlueLine+5;
	m_recogParamX = m_detParamX+110, m_recogParamY = BlueLine+5;
	m_testDBX = m_recogParamX+110, m_testDBY = BlueLine+5;
	m_sex_x = 380, m_sex_y = 196;

	playPath=CurrentPath+"\\resource\\play.png";
	stopPath=CurrentPath+"\\resource\\stop.jpg";
	play_r=104;
	stop_r=50;

	ImageDisplayArea_x=0;
	ImageDisplayArea_y=openFile_y+27;
	ImageDisplayArea_w=m_client_w;
	ImageDisplayArea_h=m_client_h-ImageDisplayArea_y;

	//CurrentAlgorithm=0;
	m_sel_algorithm = HD_LBP;//sparse_coding;
	AlgorithmPath[0]=CurrentPath+"\\resource\\HD_LBP.jpg";//算法图片路径
	AlgorithmPath[1]=CurrentPath+"\\resource\\SparseCoding.jpg";//算法图片路径
	AlgorithmPath[2]=CurrentPath+"\\resource\\HD_LBP2.jpg";//算法图片路径
	AlgorithmPath[3]=CurrentPath+"\\resource\\SparseCoding2.jpg";//算法图片路径

	AlgorithmTitle=CurrentPath+"\\resource\\AlgorithmTitle.jpg";//算法图片路径
	drop_down=CurrentPath+"\\resource\\drop_down.jpg";//算法图片路径
	drop_down_x=200,drop_down_y=BlueLine+5,drop_down_w=30,drop_down_h=25;
	rightbottom=CurrentPath+"\\resource\\rightbottom.jpg";
	unknown=CurrentPath+"\\resource\\unknown.jpg";

	DetectionTxtPath=CurrentPath+"\\detection.txt";
	RecognitionTxtPath[0]=CurrentPath+"\\recognition.txt";
	RecognitionTxtPath[1]=CurrentPath+"\\recognition_H.txt";

	m_alg_dropdown_rect.xmin=100;
	m_alg_dropdown_rect.xmax=270;
	m_alg_dropdown_rect.ymin=m_recogParamY+25;
	m_alg_dropdown_rect.ymax=m_recogParamY+90;

	int dw = m_alg_dropdown_rect.xmax - m_alg_dropdown_rect.xmin;
	int dh = m_alg_dropdown_rect.ymax - m_alg_dropdown_rect.ymin;
	m_alg_dropdown_bk = cvCreateImage(cvSize(dw, dh), 8, 3);
	m_db_dropdown_bk = cvCreateImage(cvSize(120, 50), 8, 3);
	m_sex_bk = cvCreateImage(cvSize(100, 50), 8, 3);
}
void CFaceIdentificationDlg::SetBackPic(CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f)//f==false原图，f==true镜像对称
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	int i,j,i1,j1,i2,j2;

	if (f==false)
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x+j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(m_imageBack,j1,i1,temp,j2,i2);
			}
	}
	else if (f==true)//右边镜像对称
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x-j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(m_imageBack,j1,i1,temp,j2,i2);
			}
	}
	cvReleaseImage(&temp);
}

void CFaceIdentificationDlg::SetBackPic(IplImage *image,int x,int y,int w,int h,int p_x,int p_y,bool f)//f==false原图，f==true镜像对称
{

	int i,j,i1,j1,i2,j2;

	if (f==false)
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x+j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(m_imageBack,j1,i1,image,j2,i2);
			}
	}
	else if (f==true)//右边镜像对称
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x-j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(m_imageBack,j1,i1,image,j2,i2);
			}
	}
}

void CFaceIdentificationDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int l,r,t,b;
	int i;

	for (i=0;i<4;i++)
	{
		l=MenuLeft_x +(MenuLeft_w+MenuSingleLength)*i;
		t=MenuLeft_y;
		r=MenuLeft_w+MenuSingleLength+MenuLeft_w+l;
		b=BlueLine;
		if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
		{
			InitMenu();
			ClickMenu(i);
			break;
		}
	}



	if (MenuNum==0)
	{
		MyDetection(point);
	}
	else if (MenuNum==1)
	{
		MyRecognition(point);
	} 
	else if (MenuNum==2)
	{
		MyCensus(point);
	} 
	else if (MenuNum==3)
	{
		OnMySettings(point);
		//Invalidate(FALSE);
	} 

	CDialogEx::OnLButtonDown(nFlags, point);
}

HBRUSH CFaceIdentificationDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性
	if (nCtlColor == CTLCOLOR_STATIC)//控件属性设置  Simple为 true
	{
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SetBkMode(TRANSPARENT);

	}
	if (pWnd->GetDlgCtrlID() ==IDC_STATIC_TITLE)//控件属性设置  Simple为 true
	{
		pDC->SetTextColor(RGB(255,255,255));
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(0,150,255));
	}
	if (pWnd->GetDlgCtrlID() ==IDC_STATIC_PRO)//控件属性设置  Simple为 true
	{
		pDC->SetTextColor(RGB(255,255,255));
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(0,150,255));
	}

	if( pWnd->GetDlgCtrlID() == IDC_DBINFOGROUP || pWnd->GetDlgCtrlID() == IDC_SID ||
		pWnd->GetDlgCtrlID() == IDC_SNAME || pWnd->GetDlgCtrlID() == IDC_SSEX ||
		pWnd->GetDlgCtrlID() == IDC_SAGE )
	{
// 		pDC->SetTextColor(RGB(0,0,0));
// 		pDC->SetBkMode(TRANSPARENT);
		//pDC->SetBkColor(RGB(240,240,240));
	}
	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}

void CFaceIdentificationDlg::OpenAndPlay(CPoint point)
{
	int l,r,t,b;

	l=openFile_x;
	t=openFile_y;
	r=myButton_w+l;
	b=myButton_h+t;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
	{
		myOpenfile(CurrentFilePath,CurrentFileName);
		if (CurrentFileName.GetLength()>0)
		{
			if (MenuNum==0&&DetectionOver==false)
			{
				int result=MessageBox("是否确定停止当前检测？","停止检测",MB_OKCANCEL|MB_ICONQUESTION);
				if (IDOK==result)
				{
					FinishDetection();
					MessageBox("当前检测已经停止！\n\n");
				}
				else if (IDCANCEL==result)
				{
					//			MessageBox("请等待当前检测结束！");
					return;
				}		
			}
			else if (MenuNum==1&&RecognitionOver==false)
			{
				int result=MessageBox("是否确定停止当前识别？","停止识别",MB_OKCANCEL|MB_ICONQUESTION);
				if (IDOK==result)
				{
					FinishRecognition();
					MessageBox("当前识别已经停止！\n\n");
				}
				else if (IDCANCEL==result)
				{
					return;
				}		
			}

			if (MenuNum==0)
			{
				DetectionFilePath=CurrentFilePath;
				DetectionFileName=CurrentFileName;
				if (RecognitionFilePath.GetLength()<1)
				{
					RecognitionFilePath=CurrentFilePath;
					RecognitionFileName=CurrentFileName;
				}
			}
			else if (MenuNum==1)
			{
				RecognitionFilePath=CurrentFilePath;
				RecognitionFileName=CurrentFileName;
				if (DetectionFilePath.GetLength()<1)
				{
					DetectionFilePath=CurrentFilePath;
					DetectionFileName=CurrentFileName;
				}
			}
			SetWindowText("FaceIdentification - "+CurrentFileName);
			showOpenfile();
		}
		else
		{
			if (MenuNum==0)
			{
				CurrentFilePath=DetectionFilePath;
				CurrentFileName=DetectionFileName;
			}
			else if (MenuNum==1)
			{
				CurrentFilePath=RecognitionFilePath;
				CurrentFileName=RecognitionFileName;
			}
		}
		

		return;
	}

	if (LoadedPicture)
	{
		int x1,y1;
		x1=m_client_w/2.0;
		y1=LoadedPicture->height/2.0+openFile_y+27;

		int r2=(point.x-x1)*(point.x-x1)+(point.y-y1)*(point.y-y1);

		if ((r2<play_r*play_r&&videoState[MenuNum]==false)||(r2<stop_r*stop_r&&videoState[MenuNum]==true))
		{
			if (MenuNum==1&&RecognitionOver==false)
			{
				DetectionPauseAndResume();
//				MessageBox("正在识别！！");
				return;
			}
			if (picture_video_f[MenuNum]==true)//视频
			{
				playVideo(g_thread_VideoPlay[MenuNum]);
			}
			
		}
		else
		{
			l=0;
			t=ImageDisplayArea_y;
			r=ImageDisplayArea_w;
			b=m_client_h-10;
			if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
			{
				DetectionPauseAndResume();
			}
		}

	}
}

void CFaceIdentificationDlg::MyDetection(CPoint point)
{
	int l,r,t,b;


	OpenAndPlay(point);


	l=1;
	t=m_client_h-10;
	r=m_client_w-1;
	b=m_client_h-1;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
	{
		if (videoState[MenuNum]==true/*&&DetectionOver==true*/)//正在播放,不能是正在检测中
		{
			FastForwardRewindNum=(double)(point.x-l)/(r-24-l);
			FastForwardRewindFlag=true;
		}
	}

// 	l=beginDect_x;
// 	t=beginDect_y;
// 	r=myButton_w+l;
// 	b=myButton_h+t;
// 	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
// 	{
// 		StartDetection();
// 	}
}
void CFaceIdentificationDlg::MyRecognition(CPoint point)
{
	int l,r,t,b;
	
	OpenAndPlay(point);

	RecognitionFilePath=CurrentFilePath;
	RecognitionFileName=CurrentFileName;

	l=beginDect_x;
	t=beginDect_y;
	r=myButton_w+l;
	b=myButton_h+t;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
	{
// 		int matrix_length;
// 		if( m_db_status == changed )
// 		{
// 			delete[] g_dict;
// 			g_dict = get_dict_matrix(g_dirpathes, g_AT, g_ATA, 30, 30, matrix_length,  g_class_flag);
// 			m_db_status = unchanged;
// 		}
		if (RecognitionOver==true)
		{
			StartRecognition();
		}
		
		return;
	}

	l=1;
	t=m_client_h-10;
	r=m_client_w-1;
	b=m_client_h-1;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
	{
		if (videoState[MenuNum]==true/*&&DetectionOver==true*/)//正在播放,不能是正在检测中
		{
			FastForwardRewindNum=(double)(point.x-l)/(r-24-l);
			FastForwardRewindFlag=true;
		}
	}

}
void CFaceIdentificationDlg::MyCensus(CPoint point)
{
	int l,r,t,b;

	l=openFile_x;
	t=openFile_y;
	r=myButton_w+l;
	b=myButton_h+t;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
	{
		
		SetDspbufPic(m_dspbuf,CurrentPath+"\\resource\\02.jpg",openFile_x,openFile_y,myButton_w,myButton_h,0,0,false);
		UpdateMyRectangle(openFile_x,openFile_x+myButton_w,openFile_y,openFile_y+myButton_h);

		showPictureFlag=true;
		
		QueryFlag=false;
		DetectionQuery dq;

		dq.DoModal();
		SetDspbufPic(m_dspbuf,detectionQuery,openFile_x,openFile_y,myButton_w,myButton_h,0,0,false);
		UpdateMyRectangle(openFile_x,openFile_x+myButton_w,openFile_y,openFile_y+myButton_h);

		showPictureFlag=false;
		return;
	}

	l=beginDect_x;
	t=beginDect_y;
	r=myButton_w+l;
	b=myButton_h+t;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
	{
		SetDspbufPic(m_dspbuf,CurrentPath+"\\resource\\01.jpg",beginDect_x,beginDect_y,myButton_w,myButton_h,0,0,false);	
		UpdateMyRectangle(beginDect_x,beginDect_x+myButton_w,beginDect_y,beginDect_y+myButton_h);
		QueryFlag=true;
		DetectionQuery dq;

		dq.DoModal();

		SetDspbufPic(m_dspbuf,recognitionQuery,beginDect_x,beginDect_y,myButton_w,myButton_h,0,0,false);	
		UpdateMyRectangle(beginDect_x,beginDect_x+myButton_w,beginDect_y,beginDect_y+myButton_h);
		return;
	}

	if (CensusFlag==true&&Detection_Recognition_flag==false)
	{
		((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(SW_SHOWNA);
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->ShowWindow(SW_SHOWNA);
	}
}

void CFaceIdentificationDlg::myOpenfile(CString &path,CString &filename)
{
	CFileDialog fdlg(TRUE, NULL,NULL, OFN_NOCHANGEDIR);

	if( fdlg.DoModal() == IDOK )
	{
		path = fdlg.GetPathName();
//		filename = fdlg.GetFileName();

		int pos = path.ReverseFind('\\'); 
		filename = path.Right(path.GetLength()-pos-1);

	}
	else
	{
		path ="";
		filename ="";
	}
}

void CFaceIdentificationDlg::showOpenfile()
{
	stopPlayVideo(g_thread_VideoPlay[MenuNum]);

	

	int filetype=testOpenfile();
	if (filetype==0)
	{
		MessageBox("打开的格式不正确！");
		return;
	}
	if (filetype==1)//图片
	{
		
		if (MenuNum==0)
		{
			pictureDetection();
		}
		else
		{
			showPicture();
		}
		
	}

	if (filetype==2)//视频
	{
		showVideo();
		if (MenuNum==0)
		{
			DetectionSchedule=1;
			StartDetection();
 			playVideo(g_thread_VideoPlay[MenuNum]);
		}
		else if (MenuNum==1)
		{
				StartDetectionForRecognition();//专门为识别检测		
		}
		
	}
	
	
}
int CFaceIdentificationDlg::testOpenfile(CString FilePath,bool flag)
{
	if (FilePath=="")
	{
		FilePath=CurrentFilePath;
	}
	CString picture[8]={"jpg","png","jpeg","bmp","JPG","PNG","JPEG","BMP"};
	CString video[10]={"mp4","avi","wmv","rm","rmvb","mov","flv","mkv","vob","mpeg"};

	CString format=FilePath; 
	int pos = FilePath.ReverseFind('.'); 
	format = FilePath.Right(FilePath.GetLength()-pos-1);

	int i;

	for (i=0;i<8;i++)
	{
		if (format==picture[i])
		{
			if (flag==false)
			{
				picture_video_flag=false;
				picture_video_f[MenuNum]=false;
			}
			
			return 1;//图片文件
		}
	}

	for (i=0;i<10;i++)
	{
		if (format==video[i])
		{
			if (flag==false)
			{
				picture_video_flag=true;
				picture_video_f[MenuNum]=true;
			}
			
			return 2;//视频文件
		}
	}

	return 0;//其他文件
}

void CFaceIdentificationDlg::showPicture()
{
	if (LoadedPictureCopy[MenuNum])
	{
		MyCopyImage(LoadedPicture, LoadedPictureCopy[MenuNum]);
	}
	else
	{
		int i,j;

		IplImage* image=NULL;
		myLoadPictureBy4channels(CurrentFilePath, image);
		if (!image)
		{
			return;
		}

		cvReleaseImage(&LoadedPicture);
		LoadedPicture=cvCreateImage(cvSize(ImageDisplayArea_w,ImageDisplayArea_h),8,4);

		//		GetImageDisplayData(LoadedPicture, image, 0, 0);
		ShowImageInCenter(LoadedPicture, image);
		cvReleaseImage(&image);
	}

	

	showCurrentPicture();
}

void CFaceIdentificationDlg::showVideo()
{
	InitPictureColour(m_dspbuf,0,ImageDisplayArea_w,m_client_h-17,m_client_h,240,240,240);
	stopPlayVideo(g_thread_VideoPlay[MenuNum]);

	int i,j;
	char *path = (LPSTR)(LPCTSTR)CurrentFilePath;
	CvCapture* pCapture = cvCaptureFromFile(path);

	IplImage* temp = NULL;

	if (!pCapture)
	{
		return;
	}

	for (i=0;i<70;i++)
	{
		temp = cvQueryFrame( pCapture );
	}
	

	IplImage* image=cvCreateImage(cvSize(temp->width,temp->height),8,4);
	for(i =0; i <temp->height; i++)
		for(j =0; j <temp->width; j++)
		{
			SetPointColour(image,j,i,temp,j,i);
		}

		cvReleaseCapture(&pCapture);

//		cvReleaseImage(&temp);

		cvReleaseImage(&LoadedPicture);
		
		if (MenuNum==0)
		{
			LoadedPicture=cvCreateImage(cvSize(m_client_w,(m_client_h-openFile_y-27-10)),8,4);
		}
		else
		{
			LoadedPicture=cvCreateImage(cvSize(m_client_w,(m_client_h-openFile_y-27)),8,4);
		}

//		GetImageDisplayData(LoadedPicture, image, 0, 0);
		ShowImageInCenter(LoadedPicture, image);
		cvReleaseImage(&image);

		showCurrentPicture();

}

void CFaceIdentificationDlg::showCurrentPicture()
{
	int i,j;
	if (LoadedPicture)
	{
		for (i=ImageDisplayArea_x;i<ImageDisplayArea_x+ImageDisplayArea_w;i++)
		{
			for (j=ImageDisplayArea_y;j<ImageDisplayArea_y+ImageDisplayArea_h;j++)
			{
				SetPointColour(m_dspbuf,i,j,LoadedPicture,i,j-ImageDisplayArea_y);
			}
		}

		if ((picture_video_f[MenuNum]==true&&videoState[MenuNum]==false)||(g_thread_VideoPlay[MenuNum]&&toCensusFlag==true))
		{
			toCensusFlag=false;
			showVideoPlayAndStop(playPath,play_r);
		}
		
		if (MenuNum==0||MenuNum==1)
		{
			int x1,y1;
			x1=m_client_w/2.0;
			y1=ImageDisplayArea_h/2.0+ImageDisplayArea_y;

			int r2=(mousePoint.x-x1)*(mousePoint.x-x1)+(mousePoint.y-y1)*(mousePoint.y-y1);

			if ((videoState[MenuNum]==true&&r2<stop_r*stop_r))
			{
				showVideoPlayAndStop(stopPath,stop_r);
			}
		}

		CRect tmp;

		tmp.left=0;
		tmp.right=m_client_w;
		tmp.top=ImageDisplayArea_y;
		tmp.bottom=m_client_h;
		InvalidateRect(tmp,FALSE);
//		Invalidate(FALSE);
		
	}
	
}

void CFaceIdentificationDlg::showVideoPlayAndStop(CString buttonPath,int r)
{
	int i,j;
	char *path = (LPSTR)(LPCTSTR)buttonPath;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	int x,y;
	x=temp->width/2.0;
	y=temp->height/2.0;

	int x1,y1;
	x1=m_client_w/2.0-x;
	y1=ImageDisplayArea_h/2.0+ImageDisplayArea_y-y;

	int R,G,B;
	BYTE R1,G1,B1;
	double rate;

	for(i =0; i <temp->height; i++)
		for(j =0; j <temp->width; j++)
		{
			int r2=(j-x)*(j-x)+(i-y)*(i-y);

			rate=(double)r2/(r*r);
			if (r2<r*r)
			{
				R1=m_dspbuf->imageData[ (y1+j)* m_dspbuf->widthStep + m_dspbuf->nChannels * (x1+i) + 2 ] ;
				R=(int)R1;
				R1=temp->imageData[j* temp->widthStep + temp->nChannels * i + 2 ];
				R=R*rate+(int)R1;

				G1=m_dspbuf->imageData[ (y1+j)* m_dspbuf->widthStep + m_dspbuf->nChannels * (x1+i) + 1 ];
				G=(int)G1;
				G1=temp->imageData[j* temp->widthStep + temp->nChannels * i + 1 ];
				G=G*rate+(int)G1;

				B1=m_dspbuf->imageData[ (y1+j)* m_dspbuf->widthStep + m_dspbuf->nChannels * (x1+i)  ];
				B=(int)B1;
				B1=temp->imageData[j* temp->widthStep + temp->nChannels * i  ];
				B=B*rate+(int)B1;

				SetPointColour(m_dspbuf,x1+i,y1+j,R/2,G/2,B/2);
//				SetPointColour(m_dspbuf,x1+i,y1+j,temp,i,j);
			}
			
		}
		cvReleaseImage(&temp);
}

UINT PlayVideo(LPVOID pParam)
{
	theApp.pFaceIdentificationDlg->Playing();
	return 1;
}
void CFaceIdentificationDlg::playVideo(CWinThread *&thread )
{
	if (thread==NULL)
	{
		videoState[MenuNum]=true;
		thread = AfxBeginThread((AFX_THREADPROC)PlayVideo,NULL);
	}
	else
	{
		if (videoState[MenuNum]==true)
		{
			videoState[MenuNum]=false;		
			SuspendThread(thread->m_hThread);
			showVideoPlayAndStop(playPath,play_r);
			Invalidate(FALSE);
		}
		else
		{
			videoState[MenuNum]=true;
			ResumeThread(thread->m_hThread);
		}
	}
	
}

void CFaceIdentificationDlg::OnClose()
{
	stopThreadFlag=true;
	stopPlayVideo(g_thread_VideoPlay[0]);
	stopPlayVideo(g_thread_VideoPlay[1]);

	FinishDetection();
	FinishRecognition();

	ShowWindow(SW_HIDE);

	Sleep(5000);
	CDialogEx::OnClose();
}

void CFaceIdentificationDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	int l,r,t,b;
	if (MenuNum==0||MenuNum==1)
	{

		if (nFlags==MK_LBUTTON&&currentPos>0)
		{
			l=currentPos;
			r=l+28;
			t=m_client_h-10;
			b=m_client_h-1;
			if (point.y>t&&point.y<b)
			{
				draftingFlag=true;
				OnLButtonDown(nFlags,  point);			
			}
		}
// 		else
// 		{
// 			draftingFlag=false;
// 		}
	}
	mousePoint=point;
	CDialogEx::OnMouseMove(nFlags, point);
}
void CFaceIdentificationDlg::stopPlayVideo(CWinThread *&thread)
{
	if (thread)
	{
		TerminateThread(thread->m_hThread, 0);
		CloseHandle(thread->m_hThread);
	}
	thread = NULL;
	videoState[MenuNum]=false;
}
void CFaceIdentificationDlg::stopDectVideo()
{
	if (g_thread_detection)
	{
		DWORD code; 
		GetExitCodeThread(g_thread_detection-> m_hThread, &code); 
		if(code == STILL_ACTIVE) 
		{   
			TerminateThread(g_thread_detection-> m_hThread,   0); 
		}
	}
	
	g_thread_detection = NULL;
}

void CFaceIdentificationDlg::Playing()
{
	if (MenuNum==0)
	{
		DetectionPlaying();
		return;
	}
	CString AnalysisTime;
	if (MenuNum==0)
	{
		GetAnalysisTime(AnalysisTime,DetectionTxtPath);
	}
	else if (MenuNum==1)
	{
		GetAnalysisTime(AnalysisTime,RecognitionTxtPath[m_sel_algorithm]);
	}

	int i, frames = 0, frameid = 0;
	double vframes;

	CString vname =CurrentFilePath;
	CvCapture* pCapture = cvCaptureFromFile(vname);
	vframes = cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_COUNT);


	IplImage* pFrame = NULL;

	if( !pCapture)
	{
		AfxMessageBox("无效文件!");
		return ;
	}

	vector<ALLFACE> face;
	if (MenuNum==1)
	{
		if (AnalysisTime=="#####")
		{
			GetAnalysisTime(AnalysisTime,RecognitionTxtPath[1-m_sel_algorithm]);
		}
		if (AnalysisTime!="#####")
		{
			ReadRecognitionResults(CurrentPath+"\\Record\\"+AnalysisTime+".txt",face);
		}
		
	}
	int curframeid = 0;

	while( pCapture && curframeid < vframes)
	{
		curframeid++;
		pFrame = cvQueryFrame( pCapture );

		float rate=curframeid/vframes;
		RateOfAdvance(m_dspbuf,1,m_client_w-1,m_client_h-10,m_client_h-1);
		RateOfAdvance(m_dspbuf,1,m_client_w-1,m_client_h-10,rate);
		UpdateMyRectangle(0,m_client_w,m_client_h-8,m_client_h);

		Sleep(20);
		if( pFrame )
		{
			if (MenuNum==0)
			{
				DrawFace(curframeid, pFrame,AnalysisTime);
				ShowImageInCenter(LoadedPicture, pFrame,false);	
			}
			else if (MenuNum==1)
			{
				if (curframeid>0&&curframeid<face.size())
				{
					DrawFace( pFrame,face[curframeid]);
					ShowImageInCenter(LoadedPicture, pFrame,false,face[curframeid]);
				}
				else
				{
					ShowImageInCenter(LoadedPicture, pFrame,false);
				}
				
			}
					
			if (curframeid==1)
			{
				InitPictureColour(m_dspbuf,0,ImageDisplayArea_w,ImageDisplayArea_y,m_client_h,215,215,215);//初始化背景色
			}
				
			ShowImageInCenter(m_dspbuf,0,ImageDisplayArea_y,ImageDisplayArea_w-1,ImageDisplayArea_h-10, LoadedPicture);
//			showCurrentPicture();

			if (FastForwardRewindFlag==true)
			{
				FastForwardRewindFlag=false;
				if (FastForwardRewindNum>0/*&&FastForwardRewindNum*vframes<DetectionSchedule*/)
				{
					cvSetCaptureProperty(pCapture, CV_CAP_PROP_POS_FRAMES, FastForwardRewindNum*vframes);
					curframeid=FastForwardRewindNum*vframes;
				}
				FastForwardRewindNum=-1;
			}
		}
	}

	cvReleaseCapture(&pCapture);

	g_thread_VideoPlay[MenuNum]=NULL;

	showVideo();
}

void CFaceIdentificationDlg::CreateNewDir(CString path)//新建一个目录
{
	if(!PathFileExists(path))
	{
		_mkdir(path);
	}
}
void CFaceIdentificationDlg::Detection()
{
	int framNum=-1;

	if (m_DetectionAnalysisTime!="#####")
	{
		GetBreakOffRecord(CurrentPath+"\\BreakOffD.txt",CurrentFilePath,framNum);
		if (framNum<0)
		{
			g_thread_detection=NULL;
			DetectionOver=true;
			return;
		}
		
	}

	if (framNum<0)//第一次检测时，才写入检测时间
	{
		MyGetTime(m_DetectionAnalysisTime);
		CreateNewDir(CurrentPath+"\\Record\\"+m_DetectionAnalysisTime);
		SetAnalysisTime(DetectionTxtPath,m_DetectionAnalysisTime);
	}
	

	CString Path;//文件路径
	CString tmp;

	////////////////////////////////////////
	double vframes;

	CString vname =CurrentFilePath;
	CvCapture* pCapture = cvCaptureFromFile(vname);
	vframes = cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_COUNT);

	IplImage* pFrame = NULL;

	if( !pCapture)
	{
		AfxMessageBox("无效视频文件!");
		return ;
	}

	DetectionOver=false;
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText("  检测进度 ：0.000 %");
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_SHOWNA);
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowPos(0,m_client_w-260,beginDect_y+5,260,15,SWP_SHOWWINDOW);

	CString cascade_path=CurrentPath+"\\face.xml";
	char *path = (LPSTR)(LPCTSTR)cascade_path;
	CascadeClassifier cascade;
	cascade.load(path);

	FACE face;

	int curframeid = 0;
	if (framNum>0)//第一次检测，此处＜0，如果是中断后的第二次检测，此处大于0
	{
		cvSetCaptureProperty(pCapture, CV_CAP_PROP_POS_FRAMES, framNum);
		curframeid=framNum-1;
	}
	pFrame = cvQueryFrame( pCapture );

	while( pFrame && curframeid <( vframes-3))
	{	
		if (FinishDetectionFlag==true)
		{
			DetectionOver=true;
			break;
		}
		curframeid++;
		DetectionSchedule=curframeid;
		
		if (pFrame)
		{
// 			if (curframeid>0)
// 			{
// 				DrawFace( pFrame,face);
// //				ShowImageInCenter(LoadedPicture, pFrame,false);
// 			}
// 
// 			ShowImageInCenter(m_dspbuf,0,openFile_y+27,m_client_w-1,m_client_h-openFile_y-27-1, pFrame);

			tmp.Format("%09d",curframeid);
			Path=CurrentPath+"\\Record\\"+m_DetectionAnalysisTime+"\\"+tmp+".txt";
			if (!PathFileExists(Path))
			{
				myDetectionForSingle(pFrame,cascade,face);
				WriteDetectionResults(Path,face);
			}


		}
		if (MenuFlag==true)
		{
			Sleep(100);
		}
		
		if (MenuNum==0)
		{			
			if (MenuFlag==false)
			{
				ShowDetection(curframeid,vframes);
			}
		}
		else if(!(MenuNum==1&&RecognitionOver==false))
		{
			((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_HIDE);
		}
		

		


		if ( curframeid < vframes)
		{
			pFrame = cvQueryFrame( pCapture );
		}
		else
		{
			pFrame = NULL;

		}
	}

	cvReleaseCapture(&pCapture);
	DetectionSchedule=vframes;
	

	if (FinishDetectionFlag==false)
	{
		if (framNum>0)//第一次检测，此处＜0，如果是中断后的第二次检测，此处大于0
		{
			SetBreakOffRecord(CurrentPath+"\\BreakOffD.txt",DetectionFilePath,-1);
		}
		
		MessageBox("检测完毕");
	}
	else if (FinishDetectionFlag==true)//中途停止的情况
	{
		SetBreakOffRecord(CurrentPath+"\\BreakOffD.txt",DetectionFilePath,curframeid+1);
	}

	FinishDetectionFlag=false;

	g_thread_detection=NULL;
	DetectionOver=true;

	InitPictureColour(m_dspbuf,beginDect_x,m_client_w-159,beginDect_y,beginDect_y+26,240,240,240);//初始化背景色
	UpdateMyRectangle(beginDect_x,m_client_w-160,beginDect_y,beginDect_y+25);
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_HIDE);

}
UINT startVideoDetection(LPVOID pParam)
{
	theApp.pFaceIdentificationDlg->Detection();
	return 1;
}

void CFaceIdentificationDlg::StartDetection()//检测部分
{
	if (CurrentFilePath.GetLength()<1)
	{
		MessageBox("请打开图片或视频！");
		return;
	}

	GetAnalysisTime(m_DetectionAnalysisTime,DetectionTxtPath);

	if (picture_video_f[0]==true)//视频
	{

		if (g_thread_detection==NULL)
		{
			DetectionState=true;
			g_thread_detection = AfxBeginThread((AFX_THREADPROC)startVideoDetection,NULL);
		}
		else
		{
			if (videoState[MenuNum]==false&&g_thread_VideoPlay[MenuNum])
			{
				videoState[MenuNum]=true;
				ResumeThread(g_thread_VideoPlay[MenuNum]->m_hThread);
			}
		}

		Invalidate(FALSE);
	}
// 	else
// 	{
// 		pictureDetection();//检测图片
// 	}

}
UINT startVideoRecognition(LPVOID pParam)
{
	theApp.pFaceIdentificationDlg->Recognition();
	return 1;
}
void CFaceIdentificationDlg::StartRecognition()//识别部分
{
	if (CurrentFilePath.GetLength()<1)
	{
		MessageBox("请打开图片或视频！");
		return;
	}

	if( !g_attention_pathes.size() )
	{
//		MessageBox("请先添加黑名单!");
		int result=MessageBox("请先添加黑名单!","提示",MB_OKCANCEL|MB_ICONINFORMATION );
		if (IDOK==result)
		{
			InitMenu();
			ClickMenu(3);
		}
		return;
	}

	GetAnalysisTime(m_RecognitionAnalysisTime,RecognitionTxtPath[m_sel_algorithm]);

	if (picture_video_f[1]==true)//视频
	{
		if (g_thread_Recognition==NULL)
		{
			RecognitionState=true;
			g_thread_Recognition = AfxBeginThread((AFX_THREADPROC)startVideoRecognition,NULL);
		}
// 		else
// 		{
// 			if (videoState[MenuNum]==false)
// 			{
// 				videoState[MenuNum]=true;
// 				ResumeThread(g_thread_VideoPlay[MenuNum]->m_hThread);
// 			}
// 		}
// 
// 		Invalidate(FALSE);
	}
	else
	{
		PictureRecognition();//识别图片
	}

}
void CFaceIdentificationDlg::WriteCStringR(CString s,FILE *fp)//自动换行
{
	char *a;
	int j=0;
	int k;//存储着数组长度
	s=s+"\r\n";
	a=s.GetBuffer(s.GetLength());
	k=(int)strlen(a);
	char c;
	for (j=0;j<k;j++)
	{
		c=a[j];
		fwrite(&a[j],sizeof(char),1,fp);
	}
}
void CFaceIdentificationDlg::ReadCString(CString &s,FILE *fp)
{
	char p[1000];
	fgets(p,1000,fp);
	CString str;
	str.Format(_T("%s"), p);
	s=str.Left(str.GetLength()-2);//去除 \r\n
}
void CFaceIdentificationDlg::ClearCStringVector(vector<CString> &data)
{
	if (!data.empty())
	{
		data.clear();
		vector<CString>().swap(data);
	}
}
void CFaceIdentificationDlg::pictureDetection()
{
	int i,j;

	IplImage* image=NULL;
	myLoadPictureBy4channels(CurrentFilePath, image);
	if (!image)
	{
		return;
	}

	CString cascade_path=CurrentPath+"\\face.xml";
	char *path = (LPSTR)(LPCTSTR)cascade_path;
	CascadeClassifier cascade;
	cascade.load(path);

	FACE face;

	myDetectionForSingle(image,cascade,face);

	int n= face.faces.size();

	for (i=0;i<n;i++)
	{	
		myDrawrectangular( image,face.faces[i].xmin,face.faces[i].ymin,face.faces[i].xmax,face.faces[i].ymax,255,255,255);
	}

	cvReleaseImage(&LoadedPicture);
	LoadedPicture=cvCreateImage(cvSize(m_client_w,(m_client_h-openFile_y-27)),8,4);

	ShowImageInCenter(LoadedPicture, image);
	cvReleaseImage(&image);

	showCurrentPicture();

	GetAnalysisTime(DetectionAnalysisTime,DetectionTxtPath);
	if (DetectionAnalysisTime=="#####")
	{
		MyGetTime(DetectionAnalysisTime);
		SetAnalysisTime(DetectionTxtPath,DetectionAnalysisTime);
		WriteDetectionResults(CurrentPath+"\\Record\\"+DetectionAnalysisTime+"\\000000000.txt",face);
	}

}

void CFaceIdentificationDlg::myLoadPicture(CString picturePath,IplImage*& image)
{
	char *path = (LPSTR)(LPCTSTR)picturePath;
	image = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!image)
	{
		image=NULL;
		return;
	}
}

void CFaceIdentificationDlg::myLoadPictureBy4channels(CString picturePath,IplImage*& image)
{
	int i,j;
	char *path = (LPSTR)(LPCTSTR)picturePath;
	IplImage* temp =NULL;

	temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	cvReleaseImage(&image);
	image=cvCreateImage(cvSize(temp->width,temp->height),8,4);
	for(i =0; i <temp->height; i++)
		for(j =0; j <temp->width; j++)
		{
			SetPointColour(image,j,i,temp,j,i);
		}

	cvReleaseImage(&temp);

}
void CFaceIdentificationDlg::myDrawrectangular(IplImage* image,int l,int t,int r,int b,BYTE R,BYTE G,BYTE B)
{
	int i,j;
	for (i=l;i<r;i++)
	{
		for (j=t;j<b;j++)
		{
			if (abs(i-l)<3||abs(r-i)<3||abs(j-t)<3||abs(j-b)<3)
			{
				SetPointColour(image,i,j,R,G,B);
			}
		}
	}
}
void CFaceIdentificationDlg::WriteDetectionResults(CString txtPath,FACE &face)
{
	int i;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());

	FILE *File=NULL;
	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n= face.faces.size();
		fwrite(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			fwrite(&face.faces[i].xmin,sizeof(int),1,File);	
			fwrite(&face.faces[i].ymin,sizeof(int),1,File);	
			fwrite(&face.faces[i].xmax,sizeof(int),1,File);	
			fwrite(&face.faces[i].ymax,sizeof(int),1,File);	
		}
		fclose(File);
	}

	File=NULL;
}

void CFaceIdentificationDlg::ReadAnalysisTime(vector<CString> &paths,CString txtPath)
{
	CString Path;//文件路径
	char *filePath;
	FILE *File=NULL;
	Path=txtPath;

//	vector<CString> paths;//存储路径以及编号
	ClearCStringVector(paths);
	filePath=Path.GetBuffer(Path.GetLength());
	CString tmp;

	if (PathFileExists(Path))
	{
		fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			while(!feof(File))//(feof返回1，说明到文件结尾)
			{
				//路径
				ReadCString(tmp,File);
				if ("Cognivisual Technologies.Inc"==tmp)
				{
					break;
				}

				paths.push_back(tmp);
				//日期
				ReadCString(tmp,File);
				paths.push_back(tmp);

			}	

		}
		fclose(File);
		File=NULL;
	}
}
void CFaceIdentificationDlg::GetAnalysisTime(CString &AnalysisTime,CString txtPath)
{
	AnalysisTime="#####";

	vector<CString> paths;//存储路径以及编号
	ReadAnalysisTime(paths,txtPath);

	int n=(int)paths.size();
	int i=0;

	if (n<2)
	{
		return;
	}

	bool f=false;
	for (i=0;i<n/2;i++)
	{	
		if (paths[2*i]==CurrentFilePath)
		{
			f=true;
			AnalysisTime=paths[2*i+1];
			break;
		}		
	}

}
void CFaceIdentificationDlg::SetAnalysisTime(CString Path,CString AnalysisTime)
{
	vector<CString> paths;//存储路径以及编号
	ReadAnalysisTime(paths,Path);

	if (MenuNum==0)
	{
		paths.push_back(DetectionFilePath);	
	}
	else if (MenuNum==1)
	{
		paths.push_back(RecognitionFilePath);	
	}
//	paths.push_back(CurrentFilePath);	
	paths.push_back(AnalysisTime);

	CreateNewDir(CurrentPath+"\\Record\\"+AnalysisTime);

	//文件路径
	char *filePath;
	FILE *File=NULL;

	filePath=Path.GetBuffer(Path.GetLength());
	CString tmp;

	int i;
	int n=(int)paths.size();

	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{

		for (i=0;i<n;i++)
		{	
			WriteCStringR(paths[i],File);		
		}
		WriteCStringR("Cognivisual Technologies.Inc",File);
	}

	fclose(File);
	File=NULL;

	ClearCStringVector(paths);
}
void CFaceIdentificationDlg::ShowCensusData(CString path,CString AnalysisTime)
{
	m_list.MoveWindow(0,0,0,0,SWP_HIDEWINDOW);
	int fileType=testOpenfile(path,true);
	InitPictureColour(m_dspbuf,70,ImageDisplayArea_w-70,ImageDisplayArea_y+30,ImageDisplayArea_y+50,240,240,240);//初始化背景色
	UpdateMyRectangle(70,ImageDisplayArea_w-70,ImageDisplayArea_y+30,ImageDisplayArea_y+50);
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetFont(&font);

	CString title;
	int pos = path.ReverseFind('\\'); 
	title = path.Right(path.GetLength()-pos-1);
	title=title+"  检测信息统计结果";
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowText(title);

	CurrentDetectionCensusFilePath=path;
	if (fileType==1)//图片
	{
		CensusFlag=true;
		PictureCensusData(path,AnalysisTime);
	}
	else if (fileType==2)//视频
	{
		CensusFlag=true;
		VideoCensusData(path,AnalysisTime);
	}
}
void CFaceIdentificationDlg::PictureCensusData(CString filepath,CString AnalysisTime)
{
	int i;
	CString Path=CurrentPath+"\\Record\\"+AnalysisTime+"\\000000000.txt";
	ReadFaceData(Path,pictureCensusData);

	CWnd* pWnd = GetDlgItem(IDC_LIST_DETE);
	ListView_SetExtendedListViewStyle(((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->m_hWnd, ((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetStyle() | LVS_EX_GRIDLINES);
	pWnd->MoveWindow(ImageDisplayArea_x,ImageDisplayArea_y+50,ImageDisplayArea_w,ImageDisplayArea_h-50,TRUE);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemState(0,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetExtendedStyle(LVS_EX_GRIDLINES |LVS_EX_FULLROWSELECT);//显示选中的一行

	CString title[6]=
	{
		"图片路径",
		"检测到人脸数",
		"坐标x",
		"坐标y",
		"人脸区域长度",
		"人脸区域宽度"
	};

	CHeaderCtrl* pHeaderCtrl = ((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetHeaderCtrl();
	int Column=pHeaderCtrl->GetItemCount();

	if (Column>1)
	{
		for   (int  j=1;j < Column;j++)   
		{   
			((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->DeleteColumn(1);   
		}
	}

	Column=pHeaderCtrl->GetItemCount();
	if (Column<1)
	{
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertColumn(0,"   ", LVCFMT_CENTER,30);
	}

	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertColumn(1,title[0], LVCFMT_CENTER,300);
	for (i=1;i<6;i++)
	{
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertColumn(i+1,title[i], LVCFMT_CENTER,100);
	}

	int iIndex=0;
	iIndex =((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetItemCount();


	for (int j=0;j<iIndex;j++)//删除所有行
	{
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->DeleteItem(0);
	}

	iIndex=0;

	

	for (i=0;i<(int)pictureCensusData.faces.size();i++)
	{
		CString tmp_s;
		tmp_s.Format("%d",iIndex+1);

		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertItem(iIndex,tmp_s);

		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(0,1,filepath);

		tmp_s.Format("%d",(int)pictureCensusData.faces.size());

		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(0,2,tmp_s);


		tmp_s.Format("%d",(int)pictureCensusData.faces[iIndex].xmin);
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(iIndex,3,tmp_s);
		tmp_s.Format("%d",(int)pictureCensusData.faces[iIndex].ymin);
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(iIndex,4,tmp_s);
		tmp_s.Format("%d",(int)(pictureCensusData.faces[iIndex].xmax-pictureCensusData.faces[iIndex].xmin));
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(iIndex,5,tmp_s);
		tmp_s.Format("%d",(int)(pictureCensusData.faces[iIndex].ymax-pictureCensusData.faces[iIndex].ymin));
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(iIndex,6,tmp_s);



		iIndex++;
		

	}

	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowPos(0,0,ImageDisplayArea_y+10,ImageDisplayArea_w,20,SWP_SHOWWINDOW);
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(SW_SHOWNA);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->ShowWindow(SW_SHOWNA);

}
UINT showVideoData(LPVOID pParam)
{
	theApp.pFaceIdentificationDlg->insertData();
	return 1;
}

void CFaceIdentificationDlg::insertData()
{
	int count=1;
	CString IndexNum;
	IndexNum.Format("%09d",count);

	CString Path=CurrentPath+"\\Record\\"+fileAnalysisTime+"\\"+IndexNum+".txt";
	CString tmp_s;

	int iIndex=1;

	int faceCounts;

	while (PathFileExists(Path))
	{
		ReadFaceData(Path,faceCounts);


		if (faceCounts>0)
		{
			tmp_s.Format("%d",iIndex+1);

			((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertItem(iIndex,tmp_s);

			tmp_s.Format("%d",(int)count);
			((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(iIndex-1,1,tmp_s);
			tmp_s.Format("%d",(int)faceCounts);
			((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(iIndex-1,2,tmp_s);
			iIndex++;
		}

		count++;
		IndexNum.Format("%09d",count);
		Path=CurrentPath+"\\Record\\"+fileAnalysisTime+"\\"+IndexNum+".txt";
	}

// 	tmp_s.Format("%d",(int)(count-1));
// 	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(0,2,tmp_s);

	TerminateThread(m_thread_showVideoData->m_hThread, 0);
	CloseHandle(m_thread_showVideoData->m_hThread);
	m_thread_showVideoData = NULL;

}
void CFaceIdentificationDlg::VideoCensusData(CString path,CString AnalysisTime)
{
	fileAnalysisTime=AnalysisTime;
	int i;

	CWnd* pWnd = GetDlgItem(IDC_LIST_DETE);
	ListView_SetExtendedListViewStyle(((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->m_hWnd, ((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetStyle() | LVS_EX_GRIDLINES);
	pWnd->MoveWindow(ImageDisplayArea_x,ImageDisplayArea_y+50,ImageDisplayArea_w,ImageDisplayArea_h-50,FALSE);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemState(0,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetExtendedStyle(LVS_EX_GRIDLINES |LVS_EX_FULLROWSELECT);//显示选中的一行


	CString title[2]=
	{
// 		"视频路径",
// 		"帧数",
		"帧序号",
		"检测到人脸数"
	};

	CHeaderCtrl* pHeaderCtrl = ((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetHeaderCtrl();
	int Column=pHeaderCtrl->GetItemCount();

	if (Column>0)
	{
		for   (int  j=0;j < Column;j++)   
		{   
			((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->DeleteColumn(0);   
		}
	}

	Column=pHeaderCtrl->GetItemCount();
// 	if (Column<1)
// 	{
// 		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertColumn(0,"   ", LVCFMT_CENTER,50);
// 	}
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertColumn(0,"   ", LVCFMT_CENTER,0);

	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertColumn(1,title[0], LVCFMT_CENTER,466);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertColumn(2,title[1], LVCFMT_CENTER,466);

	int iIndex=0;
	iIndex =((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetItemCount();


	for (int j=0;j<iIndex;j++)//删除所有行
	{
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->DeleteItem(0);
	}

	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowPos(0,0,ImageDisplayArea_y+10,ImageDisplayArea_w,20,SWP_HIDEWINDOW);

// 	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->InsertItem(0,"1");

// 	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemText(0,1,path);
	


	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(SW_SHOWNA);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->ShowWindow(SW_SHOWNA);

	if (m_thread_showVideoData)
	{
		TerminateThread(m_thread_showVideoData->m_hThread, 0);
		CloseHandle(m_thread_showVideoData->m_hThread);
		m_thread_showVideoData = NULL;
	}
	m_thread_showVideoData = AfxBeginThread((AFX_THREADPROC)showVideoData,NULL);

}
void CFaceIdentificationDlg::ReadFaceData(CString Path,FACE &faceData)
{
	if (!faceData.faces.empty())
	{
		faceData.faces.clear();
		vector<struct rect>().swap(faceData.faces);
	}

	char *filePath;
	FILE *File=NULL;

	filePath=Path.GetBuffer(Path.GetLength());

	struct rect tmp;

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);

		for (int i=0;i<n;i++)
		{	
			fread(&tmp.xmin,sizeof(int),1,File);	
			fread(&tmp.ymin,sizeof(int),1,File);	
			fread(&tmp.xmax,sizeof(int),1,File);	
			fread(&tmp.ymax,sizeof(int),1,File);	
			faceData.faces.push_back(tmp);
		}
		fclose(File);
	}
}
void CFaceIdentificationDlg::ReadFaceData(CString Path,int &faceCounts)
{
	char *filePath;
	FILE *File=NULL;

	filePath=Path.GetBuffer(Path.GetLength());

	struct rect tmp;

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);
		faceCounts=n;
		fclose(File);
	}
}

void CFaceIdentificationDlg::OnLvnItemchangedListDete(NMHDR *pNMHDR, LRESULT *pResult)
{
	if (showPictureFlag==true)
	{
		return;
	}
	if (QueryFlag==true)
	{
		return;
	}
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(LVIF_STATE == pNMListView->uChanged && (pNMListView->uNewState & LVIS_SELECTED))
	{
		DWORD dwPos = GetMessagePos();
		CPoint point( LOWORD(dwPos), HIWORD(dwPos) );
		((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->ScreenToClient(&point); 

		LVHITTESTINFO lvinfo;
		lvinfo.pt = point;
		lvinfo.flags = LVHT_ABOVE;

		int nItem =((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SubItemHitTest(&lvinfo);

		if(nItem != -1)
		{
			CString strtemp;
			strtemp.Format("单击的是第%d行第%d列", lvinfo.iItem, lvinfo.iSubItem);
//			strtemp=((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetItemText(0, 1);
//			MessageBox(strtemp);

			ShowDetectionSelectedData( lvinfo.iItem);
		}
	}	
	

	*pResult = 0;
}
void CFaceIdentificationDlg::MyCopyImage(IplImage* &dst, IplImage* src)
{
	int i,j;

	cvReleaseImage(&dst);
	dst=cvCreateImage(cvSize(src->width,src->height),src->depth,src->nChannels);
	for(i =0; i <src->height; i++)
		for(j =0; j <src->width; j++)
		{
			SetPointColour(dst,j,i,src,j,i);
		}
}

int CFaceIdentificationDlg::GetBlackListType(string path)
{
	int l = path.rfind(".");
	string format;
	if( l >= 0 )
		format = path.substr(l, path.length());
	else
		format = "";

	if( format == ".bmp" || format == ".jpg" || format == ".png" || format == ".jpeg" )
		return pic;

	if( GetFileAttributes(path.c_str()) == FILE_ATTRIBUTE_DIRECTORY )
		return dir;

	if( format == ".txt" )
		return txt;

	return 0;
}

void CFaceIdentificationDlg::DrawEditBlackListImage(CString path)
{
	IplImage *tmp = cvLoadImage(path);
	cvResize(tmp, m_db_image);

	int sx = 30, sy = 100;
	CRect rpic;
	rpic.left = sx, rpic.right = sx + m_db_image->width;
	rpic.top = sy, rpic.bottom = sy + m_db_image->height;
	for(int i = 0; i < m_db_image->height; i++)
		for(int j = 0; j < m_db_image->width; j++)
		{
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ] = m_db_image->imageData[ i*m_db_image->widthStep + j*m_db_image->nChannels + 0 ];
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ] = m_db_image->imageData[ i*m_db_image->widthStep + j*m_db_image->nChannels + 1 ];
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ] = m_db_image->imageData[ i*m_db_image->widthStep + j*m_db_image->nChannels + 2 ];
		}
		//			MyCopyImage(m_dspbuf,sx,sy,m_db_image);
	cvReleaseImage(&tmp);
	InvalidateRect(&rpic,FALSE);
}

void CFaceIdentificationDlg::SetPathAndName()
{
	g_dirpathes.clear(), g_face_names.clear();// = m_db_pathes;
	g_attention_names.clear(), g_attention_pathes.clear();
	for(int i = 0; i < m_db_info.size(); i++)
	{
		g_dirpathes.push_back( m_db_info[i].img_path );
		g_face_names.push_back( m_db_info[i].name.GetBuffer() );

		if( m_db_info[i].battention )
		{
			g_attention_names.push_back( m_db_info[i].name.GetBuffer() );
			g_attention_pathes.push_back( m_db_info[i].img_path );
		}
	}
}

void CFaceIdentificationDlg::OnMySettings(CPoint point)
{
	if( point.x > m_baseBtnX && point.x < m_baseBtnX + myButton_w &&
		point.y > m_baseBtnY && point.y < m_baseBtnY + myButton_h )
	{
// 		CFileDialog dlg(TRUE);
// 		if( dlg.DoModal() == IDOK )
// 		{
// 			m_CurDBPath = dlg.GetPathName();
// 			DrawEditBlackListImage(m_CurDBPath);
// 		}
	}

// 	if( point.x > m_detParamX && point.x < m_detParamX + myButton_w && point.y > m_detParamY && point.y < m_detParamY + myButton_h )
// 	{
// 		CDetParam dp;
// 		dp.DoModal();
// 	}
// 
// 	if( point.x > m_recogParamX && point.x < m_recogParamX + myButton_w && point.y > m_recogParamY && point.y < m_recogParamY + myButton_h )
// 	{
// 		CRecogParam rp;
// 		rp.DoModal();
// 	}

	if( point.x > m_sex_x + 70 && point.x < m_sex_x + 100 && point.y > m_sex_y && point.y < m_sex_y + 25 )
	{
		if( !m_sexDropDownFlag )
		{
			vector<string> pic_path;
			pic_path.push_back(m_msex_path.GetBuffer()), pic_path.push_back(m_fsex_path.GetBuffer());
			ShowDropDown(pic_path, m_sex_bk, m_sex_x, m_sex_y+25, 100, 25);
			//cvSaveImage("sex_bg.bmp", m_sex_bk);
		}
		else
			ClearDropDownList(m_sex_bk, m_sex_x, m_sex_y+25);

		m_sexDropDownFlag = !m_sexDropDownFlag;
	}
	else if( m_sexDropDownFlag )
	{
		if( point.x > m_sex_x && point.x < m_sex_x + 70 && point.y > m_sex_y + 25 && point.y < m_sex_y + 75 )
		{
			if( point.y < m_sex_y + 50 )
			{
				m_bsex = "男";
				SetBackPic(m_msex_path, m_sex_x, m_sex_y, 70, 25, 0, 0, false);
			}
			else
			{
				m_bsex = "女";
				SetBackPic(m_fsex_path, m_sex_x, m_sex_y, 70, 25, 0, 0, false);
			}

			GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);

			CRect r;
			r.left = m_sex_x, r.right = m_sex_x+70, r.top = m_sex_y, r.bottom = m_sex_y+25;
			InvalidateRect(r,FALSE);
		}
		ClearDropDownList(m_sex_bk, m_sex_x, m_sex_y+25);
		m_sexDropDownFlag = false;
	}

// 	if( point.x > m_createDBX && point.x < m_createDBX + myButton_w && point.y > m_createDBY && point.y < m_createDBY + myButton_h )
// 	{
// 		BROWSEINFO bInfo;
// 		ZeroMemory(&bInfo, sizeof(bInfo));
// 		bInfo.hwndOwner =GetSafeHwnd();
// 		bInfo.lpszTitle = _T("请选择数据库目录: ");
// 		bInfo.ulFlags = BIF_BROWSEINCLUDEFILES;//BIF_BROWSEINCLUDEFILES;
// 
// 		LPITEMIDLIST lpDlist; //用来保存返回信息的IDList
// 		lpDlist = SHBrowseForFolder(&bInfo) ; //显示选择对话框
// 		if(lpDlist != NULL) //用户按了确定按钮
// 		{
// 			TCHAR chPath[MAX_PATH]; //用来存储路径的字符串
// 			SHGetPathFromIDList(lpDlist, chPath);//把项目标识列表转化成字符串
// 
// 			m_CurDBPath = chPath;
// 			int type = GetBlackListType(chPath);
// 			if( type )
// 			{
// 				g_dirpathes.clear();
// 				g_face_names.clear();
// 				g_attention_pathes.clear();
// 				g_attention_names.clear();
// 			}
// 
// 			if( type == dir || type == txt )
// 			{
// 				if( type == dir )
// 				{
// 					//get_path_files2(chPath, g_dirpathes);
// 					get_image_pathes(chPath, g_dirpathes);
// 					LoadDBImages(g_dirpathes);
// 				}
// 				
// 				if( type == txt )
// 					LoadDBImages(chPath);
// 
// 				if( g_dirpathes.size() )
// 					m_db_status = changed;
// 
// 				if( m_sel_algorithm == HD_LBP )
// 					g_extract_lbps_hdl = (HANDLE)_beginthreadex(NULL, 0, extract_feature_lbps_thread, NULL, NULL, NULL);
// 
// 				if( m_sel_algorithm == sparse_coding )
// 					g_extract_sps_hdl = (HANDLE)_beginthreadex(NULL, 0, extract_feature_sps_thread, NULL, NULL, NULL);
// 			}
// 
// 			if( type == pic )
// 				DrawEditBlackListImage(chPath);
// 		}
// 	}
	
	if( point.x > m_attention_x && point.x < m_attention_x + m_attention_l && point.y > m_attention_y && point.y < m_attention_y + m_attention_x )
	{
		m_battention = !m_battention;
		SetAttentionCheck(m_battention);
	}

	if( point.x > m_ok_btn_x && point.x < m_ok_btn_x + myButton_w && point.y > m_ok_btn_y && point.y < m_ok_btn_y + myButton_h )
	{
		UpdateData();
		EditDBInfo(m_CurDBPath);
	}

	if( point.x > m_del_btn_x && point.x < m_del_btn_x + myButton_w && point.y > m_del_btn_y && point.y < m_del_btn_y + myButton_h )
	{
		if( !RecognitionOver )
		{
			MessageBox("正在识别，不能删除黑名单!");
			return;
		}

		UpdateData();
		g_del_idx = DeleteDBInfo(atoi(m_nid));
	}

	if( point.x > m_del_all_x && point.x < m_del_all_x + myButton_w && point.y > m_del_all_y && point.y < m_del_all_y + myButton_h )
	{
		if( !RecognitionOver )
		{
			MessageBox("正在识别，不能删除黑名单!");
			return;
		}
		DeleteDB();

		if( m_sel_algorithm == HD_LBP )
			_beginthreadex(NULL, 0, delete_feature_lbps_thread, NULL, NULL, NULL);

		if( m_sel_algorithm == sparse_coding )
			_beginthreadex(NULL, 0, delete_feature_sps_thread, NULL, NULL, NULL);
	}

// 	if( point.x > m_testDBX && point.x < m_testDBX + myButton_w && point.y > m_testDBY && point.y < m_testDBY + myButton_h )
// 	{
// 		CTestDB dp;
// 		dp.DoModal();
// 	}

	if( point.x > m_dbDropDownX && point.x < m_dbDropDownX + 30 && point.y > m_dbDropDownY && point.y < m_dbDropDownY + myButton_h )
	{
		if( !m_dbDrowDownFlag )
		{
			int sx = m_baseBtnX, sy = m_baseBtnY + myButton_h;
			for(int i = 0; i < m_db_dropdown_bk->height; i++)
				for(int j = 0; j < m_db_dropdown_bk->width; j++)
				{
					m_db_dropdown_bk->imageData[ i * m_db_dropdown_bk->widthStep + j * m_db_dropdown_bk->nChannels + 0 ] = 
						m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ];

					m_db_dropdown_bk->imageData[ i * m_db_dropdown_bk->widthStep + j * m_db_dropdown_bk->nChannels + 1 ] = 
						m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ];

					m_db_dropdown_bk->imageData[ i * m_db_dropdown_bk->widthStep + j * m_db_dropdown_bk->nChannels + 2 ] = 
						m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ];
				}

			ShowDBDropDown();
		}
		else
		{
			ClearDBDropDown();
		}

		return;
	}

	int l,r,t,b;
	l=drop_down_x;
	t=drop_down_y;
	r=drop_down_w+l;
	b=drop_down_h+t;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
	{
		if (ShowDropDownListFlag==false)
		{
			int sx = m_alg_dropdown_rect.xmin, sy = m_alg_dropdown_rect.ymin;
			for(int i = 0; i < m_alg_dropdown_bk->height; i++)
				for(int j = 0; j < m_alg_dropdown_bk->width; j++)
				{
					m_alg_dropdown_bk->imageData[ i * m_alg_dropdown_bk->widthStep + j * m_alg_dropdown_bk->nChannels + 0 ] = 
						m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ];

					m_alg_dropdown_bk->imageData[ i * m_alg_dropdown_bk->widthStep + j * m_alg_dropdown_bk->nChannels + 1 ] = 
						m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ];

					m_alg_dropdown_bk->imageData[ i * m_alg_dropdown_bk->widthStep + j * m_alg_dropdown_bk->nChannels + 2 ] = 
						m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ];
				}
				
	//		MyCopyImage(image_settings,m_dspbuf,100,m_recogParamY+25,170,100);
			ShowDropDownList();
		}
		else
		{
			ClearDropDownList();
		}
		return;
	}

	if (ShowDropDownListFlag==true)
	{
		l=110, t=m_recogParamY+30, r=200, b=25+t;
		if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
			m_sel_algorithm = HD_LBP;//sparse_coding;

		l=110, t=m_recogParamY+55, r=200, b=25+t;
		if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
			m_sel_algorithm = sparse_coding;//HD_LBP;

		ShowCurrentAlgorithm();
		ClearDropDownList();
	}

	if( m_dbDrowDownFlag )
	{
		ClearDBDropDown();
		CRect sel_img,sel_txt;
		sel_img.left = m_baseBtnX, sel_img.right = m_baseBtnX + 120;
		sel_img.top = m_baseBtnY + myButton_h, sel_img.bottom = m_baseBtnY + 2*myButton_h;
		sel_txt.left = m_baseBtnX, sel_txt.right = m_baseBtnX + 120;
		sel_txt.top = m_baseBtnY + 2*myButton_h, sel_txt.bottom = m_baseBtnY + 3*myButton_h;

		if( sel_img.PtInRect(point) )
		{
			CFileDialog dlg(TRUE);
			if( dlg.DoModal() == IDOK )
			{
				m_CurDBPath = dlg.GetPathName();
				DrawEditBlackListImage(m_CurDBPath);
			}

// 			BROWSEINFO bInfo;
// 			ZeroMemory(&bInfo, sizeof(bInfo));
// 			bInfo.hwndOwner =GetSafeHwnd();
// 			bInfo.lpszTitle = _T("请选择一幅图片: ");
// 			bInfo.ulFlags = BIF_RETURNONLYFSDIRS;//BIF_BROWSEINCLUDEFILES;
// 
// 			LPITEMIDLIST lpDlist; //用来保存返回信息的IDList
// 			lpDlist = SHBrowseForFolder(&bInfo) ; //显示选择对话框
// 			if(lpDlist != NULL) //用户按了确定按钮
// 			{
// 				TCHAR chPath[MAX_PATH]; //用来存储路径的字符串
// 				SHGetPathFromIDList(lpDlist, chPath);//把项目标识列表转化成字符串
// 
// 				//get_path_files2(chPath, g_dirpathes);
// 				get_image_pathes(chPath, g_dirpathes);
// 				LoadDBImages(g_dirpathes);
// 
// 				if( g_dirpathes.size() )
// 					m_db_status = changed;
// 
// 				if( m_sel_algorithm == HD_LBP )
// 					g_extract_lbps_hdl = (HANDLE)_beginthreadex(NULL, 0, extract_feature_lbps_thread, NULL, NULL, NULL);
// 
// 				if( m_sel_algorithm == sparse_coding )
// 					g_extract_sps_hdl = (HANDLE)_beginthreadex(NULL, 0, extract_feature_sps_thread, NULL, NULL, NULL);
// 			}
		}

		if( sel_txt.PtInRect(point) )
		{
			CFileDialog dlg(TRUE);
			if( dlg.DoModal() == IDOK )
			{
				string txt_path = dlg.GetPathName();

				LoadDBImages(txt_path);
				if( g_dirpathes.size() )
					m_db_status = changed;

				if( m_sel_algorithm == HD_LBP )
					g_extract_lbps_hdl = (HANDLE)_beginthreadex(NULL, 0, extract_feature_lbps_thread, NULL, NULL, NULL);

				if( m_sel_algorithm == sparse_coding )
					g_extract_sps_hdl = (HANDLE)_beginthreadex(NULL, 0, extract_feature_sps_thread, NULL, NULL, NULL);
			}
		}
	}
}

void CFaceIdentificationDlg::SetAttentionCheck(int flag)
{
	CRect ar;
	ar.left = m_attention_x, ar.right = m_attention_x + m_attention_l;
	ar.top = m_attention_y, ar.bottom = m_attention_y + m_attention_l;
	
	if( flag )
		SetBackPic(m_black_list_ypath, 530, 165, 30, 30, 0,0, false);
	else
		SetBackPic(m_black_list_npath, 530, 165, 30, 30, 0,0, false);

	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	InvalidateRect(&ar,FALSE);
}

void CFaceIdentificationDlg::LoadDBImages(vector<string> &pathes)
{
	int max_id = -1;
	for(int i = 0; i < m_db_info.size(); i++)
	{
		if( m_db_info[i].id > max_id )
			max_id = m_db_info[i].id;
	}

	for(int i = 0; i < pathes.size(); i++)
	{
		string path = pathes[i];
		int l = path.rfind("\\");
		string dir = path.substr(0, l+1);
		string img_name = path.substr(l+1, path.length());
		string tmp_name = "tmp_";

		int r = img_name.rfind(".");
		string face_name = img_name.substr(0,r);
//		g_face_names.push_back(face_name);
		tmp_name += face_name + ".bmp";
		string tmp_path = dir + tmp_name;

		IplImage *img = cvLoadImage(pathes[i].c_str());
		IplImage *tmp_img = cvCreateImage(cvSize(35, 35), 8, 3);
		cvResize(img, tmp_img);
		cvSaveImage(tmp_path.c_str(), tmp_img);
		cvReleaseImage(&img);
		cvReleaseImage(&tmp_img);

		DBINFO db;
		db.id = ++max_id;//atoi(m_nid);
		db.age = 0;//atoi(m_nage);
		db.name = face_name.c_str();//m_strname;
		db.sex = "";//m_bsex;
		db.img_path = pathes[i];
		db.battention = 1;

		m_db_info.push_back(db);
		int index = m_db_info.size() - 1;

		HBITMAP hBitmap;
		CBitmap *pBitmap;
		pBitmap = new   CBitmap;
		hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), /*img_path*/tmp_path.c_str(),IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		pBitmap->Attach(hBitmap);
		int a = m_img_list.Add(pBitmap, RGB(0,0,0));

		char sid[16];
		sprintf(sid, "%d", max_id);

		m_list.InsertItem(a,"",a);
		m_list.SetItemText(index, 1, sid);
		m_list.SetItemText(index, 2, db.name);
		m_list.SetItemText(index, 3, db.sex);
		m_list.SetItemText(index, 4, "");
		m_list.SetItemText(index, 5, "是");

		DeleteFile(tmp_path.c_str());
	}

	SetPathAndName();
}

void CFaceIdentificationDlg::LoadDBImages(string txt_path)
{
	int max_id = 0;//-1;
	for(int i = 0; i < m_db_info.size(); i++)
	{
		if( m_db_info[i].id > max_id )
			max_id = m_db_info[i].id;
	}

	vector<string> records, tmp_records;
	ifstream ff(txt_path);
	string temp;
	while(getline(ff, temp))
		records.push_back(temp);

	tmp_records = records;

	int flag = 1;
	for(int kk = 0; kk < 6; kk++)
	{
		int pr = tmp_records[0].find(" ");
		string str;

		if( pr >= 0 )
		{
			str = tmp_records[0].substr(0,pr);
			tmp_records[0] = tmp_records[0].substr(pr+1, tmp_records[0].length());
		}
		else
			str = "";

		if( pr < 0 && kk < 5 )
		{
			flag = 0;
			break;
		}
	}

	if( !flag )
	{
		MessageBox("文本格式不正确!");
		return;
	}

	for(int i = 0; i < records.size(); i++)
	{
		vector<string> info;
		string path;//,sid,name,sex,age;
		for(int k = 0; k < 6; k++)
		{
			int pr = records[i].find(" ");
			string str = records[i].substr(0,pr);
			records[i] = records[i].substr(pr+1, records[i].length());

			info.push_back(str);
		}

		path = info[0];
		//g_dirpathes.push_back(path);
		int l = path.rfind("\\");
		string dir = path.substr(0, l+1);
		string img_name = path.substr(l+1, path.length());
		string tmp_name = "tmp_";

		int r = img_name.rfind(".");
		string face_name = img_name.substr(0,r);
		//g_face_names.push_back(face_name);
		tmp_name += face_name + ".bmp";
		string tmp_path = dir + tmp_name;

		IplImage *img = cvLoadImage(path.c_str());
		IplImage *tmp_img = cvCreateImage(cvSize(35, 35), 8, 3);
		cvResize(img, tmp_img);
		cvSaveImage(tmp_path.c_str(), tmp_img);
		cvReleaseImage(&img);
		cvReleaseImage(&tmp_img);

		DBINFO db;
		db.id = max_id + atoi(info[1].c_str());//atoi(m_nid);
		db.name = info[2].c_str();//m_strname;
		db.sex = info[3].c_str();
		db.age = atoi(info[4].c_str());
		db.img_path = path;//pathes[i];
		if( info[5] == "是" )
			db.battention = 1;
		if( info[5] == "否" )
			db.battention = 0;

		m_db_info.push_back(db);

		int index = m_db_info.size() - 1;

		HBITMAP hBitmap;
		CBitmap *pBitmap;
		pBitmap = new   CBitmap;
		hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), /*img_path*/tmp_path.c_str(),IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		pBitmap->Attach(hBitmap);
		int a = m_img_list.Add(pBitmap, RGB(0,0,0));

		char sid[16], sage[16];
		sprintf(sid, "%d", db.id);
		sprintf(sage, "%d", db.age);

		m_list.InsertItem(a,"",a);
		m_list.SetItemText(index, 1, sid);
		m_list.SetItemText(index, 2, db.name);
		m_list.SetItemText(index, 3, db.sex);
		m_list.SetItemText(index, 4, sage);

		CString attention[2] = {"否","是"};
		m_list.SetItemText(index, 5, attention[db.battention]);

		DeleteFile(tmp_path.c_str());
	}

	SetPathAndName();
}

void CFaceIdentificationDlg::EditDBInfo(CString img_path)
{
	if( img_path.IsEmpty() )
		return;
	int IndexNum;
	
	int i = 0, id = atoi(m_nid);
	for(i = 0; i < m_db_info.size(); i++)
	{
		if( m_db_info[i].id == id )
		{
			IndexNum=i;
			m_db_info[i].age = atoi(m_nage);
			m_db_info[i].name = m_strname;
			m_db_info[i].sex = m_bsex;
			m_db_info[i].battention = m_battention;
			m_db_info[i].img_path = img_path;

			int cnt = m_list.GetItemCount();
			for(int j = 0; j < cnt; j++)
			{
				CString id = m_list.GetItemText(j, 1);

				if( m_nid == id )
				{
					m_list.SetItemText(j, 2, m_strname);
					m_list.SetItemText(j, 3, m_bsex);
					m_list.SetItemText(j, 4, m_nage);

					CString battention[2] = {"否","是"};
					m_list.SetItemText(j, 5, battention[m_battention]);
					//battention.Format("%d", m_nage);
					break;
				}
			}

			break;
		}
	}

	if( i >= m_db_info.size() )
	{
		DBINFO db;
		db.id = atoi(m_nid);
		db.age = atoi(m_nage);
		db.name = m_strname;
		db.sex = m_bsex;
		db.img_path = img_path;
		db.battention = m_battention;

		string path = img_path;
		int l = path.rfind("\\");
		string dir = path.substr(0, l+1);
		string img_name = path.substr(l+1, path.length());
		string tmp_name = "tmp_";
		
		int r = img_name.rfind(".");
		string face_name = img_name.substr(0,r);
// 		g_face_names.push_back(db.name.GetBuffer());
// 		g_dirpathes.push_back(img_path.GetBuffer());
		tmp_name += face_name + ".bmp";
		string tmp_path = dir + tmp_name;

		IplImage *img = cvLoadImage(img_path);
		IplImage *tmp_img = cvCreateImage(cvSize(35, 35), 8, 3);
		cvResize(img, tmp_img);
		cvSaveImage(tmp_path.c_str(), tmp_img);
		cvReleaseImage(&img);
		cvReleaseImage(&tmp_img);

		m_db_info.push_back(db);
		int index = m_db_info.size() - 1;

		HBITMAP hBitmap;
		CBitmap *pBitmap;
		pBitmap = new   CBitmap;
		hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), /*img_path*/tmp_path.c_str(),IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		pBitmap->Attach(hBitmap);
		int a = m_img_list.Add(pBitmap, RGB(0,0,0));

		m_list.InsertItem(a,"",a);
		m_list.SetItemText(index, 1, m_nid);
		m_list.SetItemText(index, 2, m_strname);
		m_list.SetItemText(index, 3, m_bsex);
		m_list.SetItemText(index, 4, m_nage);

		CString battention[2] = {"否","是"};
		m_list.SetItemText(index, 5, battention[m_battention]);

		int cnt = m_img_list.GetImageCount();

		DeleteFile(tmp_path.c_str());

		m_db_status = changed;
		IndexNum=m_db_info.size() - 1;
	}
	SetPathAndName();
	WriteDBinfo(m_db_info[IndexNum]);
}

int CFaceIdentificationDlg::DeleteDBInfo(int id)
{
	if( !m_db_info.size() )
		return -1;

	m_list.DeleteAllItems();

	int cnt = m_img_list.GetImageCount();
	for(int i = 0; i < cnt; i++)
	{
		m_img_list.Remove(0);
	}

	int index;
	for(int i = 0; i < m_db_info.size(); i++)
	{
		if( m_db_info[i].id == id )
		{
			m_db_info.erase(m_db_info.begin() + i);
			index = i;
			break;
		}
	}

	for(int i = 0; i < m_db_info.size(); i++)
	{
		string path = m_db_info[i].img_path;
		int l = path.rfind("\\");
		string dir = path.substr(0, l+1);
		string img_name = path.substr(l+1, path.length());
		string tmp_name = "tmp_";

		int r = img_name.rfind(".");
		string face_name = img_name.substr(0,r);
		tmp_name += face_name + ".bmp";
		string tmp_path = dir + tmp_name;

		IplImage *img = cvLoadImage(path.c_str());
		IplImage *tmp_img = cvCreateImage(cvSize(35, 35), 8, 3);
		cvResize(img, tmp_img);
		cvSaveImage(tmp_path.c_str(), tmp_img);
		cvReleaseImage(&img);
		cvReleaseImage(&tmp_img);

		CString id, age;
		id.Format("%d", m_db_info[i].id);
		age.Format("%d", m_db_info[i].age);

		HBITMAP hBitmap;
		CBitmap *pBitmap;
		pBitmap = new   CBitmap;
		hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), tmp_path.c_str(),IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		pBitmap->Attach(hBitmap);
		int a = m_img_list.Add(pBitmap, RGB(0,0,0));

		m_list.InsertItem(a,"",a);
		m_list.SetItemText(i, 1, id);
		m_list.SetItemText(i, 2, m_db_info[i].name);
		m_list.SetItemText(i, 3, m_db_info[i].sex);
		m_list.SetItemText(i, 4, age);

		CString battention[2] = {"否","是"};
		m_list.SetItemText(i, 5, battention[m_db_info[i].battention]);

		DeleteFile(tmp_path.c_str());
	}

	SetPathAndName();
	if( !m_db_info.size() )
	{
		m_nid = "";
		m_nage = "";
		m_strname = "";
		m_bsex = "";
		UpdateData(FALSE);

		int sx = 30, sy = 100;
		for(int i = 0; i < m_db_image->height; i++)
			for(int j = 0; j < m_db_image->width; j++)
			{
				m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ] = 240;
				m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ] = 240;
				m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ] = 240;
			}
	}

	m_db_status = changed;

	return index;
}

void CFaceIdentificationDlg::DeleteDB()
{
	g_dirpathes.clear();
	m_db_info.clear();
	m_list.DeleteAllItems();

	int cnt = m_img_list.GetImageCount();
	for(int i = 0; i < cnt; i++)
	{
		m_img_list.Remove(0);
	}

	m_nid = "";
	m_nage = "";
	m_strname = "";
	m_bsex = "";
	UpdateData(FALSE);

	int sx = 30, sy = 100;
	for(int i = 0; i < m_db_image->height; i++)
		for(int j = 0; j < m_db_image->width; j++)
		{
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ] = 240;
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ] = 240;
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ] = 240;
		}
	InitPictureColour(m_db_image,0,m_db_image->width,0,m_db_image->height,240,240,240);//初始化背景色
	m_db_status = changed;
	SetPathAndName();
}

void CFaceIdentificationDlg::ShowSettingsCtrl(int flag)
{
	if( flag )
	{
		SetBackPic(m_baseBtmPath, m_baseBtnX,m_baseBtnY,myButton_w,myButton_h,0,0,false);
// 		SetBackPic(m_createDBPath, m_createDBX, m_createDBY, myButton_w, myButton_h, 0,0,false);
// 		SetBackPic(m_detParamBtmPath, m_detParamX, m_detParamY, myButton_w,myButton_h, 0,0, false);
// 		SetBackPic(m_recogParamBtmPath, m_recogParamX, m_recogParamY, myButton_w,myButton_h, 0,0, false);
//		SetBackPic(m_test_db_path, m_testDBX, m_testDBY, myButton_w, myButton_h, 0, 0, false);
		SetBackPic(m_del_path, m_del_btn_x, m_del_btn_y, myButton_w, myButton_h, 0, 0, false);
		SetBackPic(m_ok_path, m_ok_btn_x, m_ok_btn_y, myButton_w, myButton_h, 0, 0, false);
		SetBackPic(m_del_all_path, m_del_all_x, m_del_all_y, myButton_w, myButton_h, 0, 0, false);
		SetBackPic(m_msex_path, m_sex_x, m_sex_y, 70, 25, 0, 0, false);
		SetBackPic(drop_down, m_sex_x+70, m_sex_y, 30, 25, 0, 0, false);

		PutText("编号", m_imageBack, 330, 140, 0.5, CV_RGB(0,0,255));
		PutText("姓名", m_imageBack, 330, 178, 0.5, CV_RGB(0,0,255));
		PutText("性别", m_imageBack, 330, 216, 0.5, CV_RGB(0,0,255));
		PutText("年龄", m_imageBack, 530, 140, 0.5, CV_RGB(0,0,255));
		DrawGroupCtrl("人脸信息", m_imageBack, 290, 95, 600, 170, 0.25);

		SetBackPic(m_black_list_ypath, 530, 165, 30, 30, 0,0, false);
		PutText("是否关注", m_imageBack, 565, 189, 0.5, CV_RGB(0,0,255));
	}

	m_id.ShowWindow(flag);
	m_name.ShowWindow(flag);
	//m_sex.ShowWindow(flag);
	m_age.ShowWindow(flag);

//	m_group.ShowWindow(flag);
	m_list.ShowWindow(flag);
}

void CFaceIdentificationDlg::PutText(CString str, IplImage *image, int x, int y, float scale, CvScalar color)
{
//	IplImage *img = cvLoadImage("li.bmp");
	CvxText text("C:\\WINDOWS\\Fonts\\simsun.ttc");//arialuni.ttf
//	const char *msg = "在OpenCV中输出汉字！"; 

//	float p = 0.5; 
	text.setFont(NULL, NULL, NULL, &scale);   // 透明处理 
	text.putText(image, str, cvPoint(x, y), /*CV_RGB(20,20,20)*/color);
}

void CFaceIdentificationDlg::DrawGroupCtrl(CString str, IplImage *image, int x, int y, int w, int h, float scale)
{
	int len = str.GetLength();
	CvScalar color;
	color = CV_RGB(212,208,200);
	int thickness = 2;

	cvLine(image, cvPoint(x,y), cvPoint(x,y+h-1), color, thickness );
	cvLine(image, cvPoint(x,y+h-1), cvPoint(x+w-1,y+h-1), color, thickness);
	cvLine(image, cvPoint(x+w-1,y), cvPoint(x+w-1,y+h-1), color, thickness);

	cvLine(image, cvPoint(x,y), cvPoint(x+8,y), color, thickness);
	PutText(str, image, x+13, y+2, 0.25, CV_RGB(0,0,0));
	cvLine(image, cvPoint(x+len*12,y), cvPoint(x+w-1,y), color, thickness);
}

void CFaceIdentificationDlg::Recognition()
{
	if (videoState[1]==true)
	{
		stopPlayVideo(g_thread_VideoPlay[1]);
	}
	

	int sel_algorithm = m_sel_algorithm;
 	if (m_RecognitionAnalysisTime!="#####")
 	{
		if (g_thread_VideoPlay[MenuNum]==NULL)
		{
			videoState[MenuNum]=true;
			g_thread_VideoPlay[MenuNum] = AfxBeginThread((AFX_THREADPROC)PlayVideo,NULL);
		}
		else
		{
			if (videoState[MenuNum]==false)
			{
				videoState[MenuNum]=true;
				ResumeThread(g_thread_VideoPlay[MenuNum]->m_hThread);
			}
		}
		return;
 	}
 	
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText("  识别进度 ：0.000 %");
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_SHOWNA);
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowPos(0,m_client_w-260,beginDect_y+5,260,15,SWP_SHOWWINDOW);
	CString DetectionRecord;
	GetAnalysisTime(DetectionRecord,DetectionTxtPath);
	CString Path;//文件路径
	CString tmp;

	////////////////////////////////////////
	int i, frames = 0, frameid = 0;
	double vframes;

	CString vname =CurrentFilePath;
	CvCapture* pCapture = cvCaptureFromFile(vname);
	vframes = cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_COUNT);
	for(int k = 0; k < g_recog_flag.size(); k++)
		g_recog_flag[k].resize(vframes,0);

	IplImage* pFrame = NULL;

	if( !pCapture)
	{
		AfxMessageBox("无效视频文件!");
		return ;
	}

	MyGetTime(m_RecognitionAnalysisTime);
	if (DetectionRecord=="#####")
	{
		MyGetTime(m_DetectionAnalysisTime);
		CreateNewDir(CurrentPath+"\\Record\\"+m_DetectionAnalysisTime);
	}

	RecognitionOver=false;
	CString cascade_path=CurrentPath+"\\face.xml";
	char *path = (LPSTR)(LPCTSTR)cascade_path;
	CascadeClassifier cascade;
	cascade.load(path);

	ALLFACE face;
	vector<ALLFACE> allfaces;
	int curframeid = 0, nattention = 0;

	g_recog_flag.resize(g_face_names.size());

	for(int i = 0; i < m_db_info.size(); i++)
	{
		if( m_db_info[i].battention )
		{
// 		    g_attention_names.push_back(g_face_names[i]);
// 			g_attention_pathes.push_back(g_dirpathes[i]);
			nattention++;
		}
	}

	WaitForSingleObject(g_init_model_hdl, INFINITE);
	if( sel_algorithm == HD_LBP )
	{
		WaitForSingleObject(g_extract_lbps_hdl, INFINITE);

		EnterCriticalSection(&g_csf);
		for(int i = 0; i < g_last_db_size; i++)
			delete[] g_base_features[i];

		delete[] g_base_features;

		g_base_features = new int *[ nattention ];//g_tmp_bfeatures.size()
		if( g_tmp_bfeatures.size() )
		{
			for(int i = 0; i < g_tmp_bfeatures.size(); i++)
			{
				if( !m_db_info[i].battention )
					continue;

				g_base_features[i] = new int[totalDim];
				for(int j = 0; j < totalDim; j++)
					g_base_features[i][j] = g_tmp_bfeatures[i][j];

				g_tmp_bfeatures[i].clear();
			}
			g_tmp_bfeatures.clear();
		}
		else
		{
			for(int i = 0; i < g_attention_pathes.size(); i++)
			{
				string dir = get_dir_from_path(g_attention_pathes[i]);//path.substr(0, l+1);
				string name = get_name_from_path(g_attention_pathes[i]);//path.substr(l+1, r);
				string ft = "_lbp";

				string txt_path = dir + name + ft + ".txt";
				g_base_features[i] = new int[totalDim];
				if( _access(txt_path.c_str(),0) != -1 )
				{
					ifstream ff(txt_path);
					for(int j = 0; j < totalDim; j++)
						ff >> g_base_features[i][j];

					ff.close();
				}
				else
				{
					IplImage *image = cvLoadImage(g_attention_pathes[i].c_str());
					get_face_feature(image, g_base_features[i]);
					cvReleaseImage(&image);
					ofstream ff(txt_path);
					for(int j = 0; j < totalDim; j++)
					{
						ff<<g_base_features[i][j];
						if( j < totalDim-1 )
							ff<<endl;
					}
					ff.close();
				}
			}
		}
		
		g_last_db_size = nattention;//g_tmp_bfeatures.size();
		LeaveCriticalSection(&g_csf);
	}

	if( sel_algorithm == sparse_coding )
	{
		WaitForSingleObject(g_extract_sps_hdl, INFINITE);

		EnterCriticalSection(&g_csf);
		delete[] g_dict;

		int flen = g_samplew * g_sampleh;
		g_dict = new float[ flen * nattention ];//g_face_names.size()

		if( g_sp_bfeatures.size() )
		{
			for(int i = 0; i < g_face_names.size(); i++)
			{
				if( !m_db_info[i].battention )
					continue;

				for(int j = 0; j < flen; j++)
					g_dict[ j * g_face_names.size() + i ] = g_sp_bfeatures[i][j];

				g_sp_bfeatures[i].clear();
			}

			g_sp_bfeatures.clear();
		}
		else
		{
			for(int i = 0; i < g_attention_pathes.size(); i++)
			{
				string dir = get_dir_from_path(g_attention_pathes[i]);//path.substr(0, l+1);
				string name = get_name_from_path(g_attention_pathes[i]);//path.substr(l+1, r);
				string ft = "_sp";

				string txt_path = dir + name + ft + ".txt";
				if( _access(txt_path.c_str(),0) != -1 )
				{
					ifstream ff(txt_path);
					for(int j = 0; j < flen; j++)
						ff>>g_dict[ j * g_face_names.size() + i ];
				}
				else
				{
					IplImage *image = cvLoadImage(g_attention_pathes[i].c_str());
					vector<float> face_data;
					detect_alignment(image, face_data);
					for(int j = 0; j < flen; j++)
						g_dict[ j * g_face_names.size() + i ] = face_data[j];//g_sp_bfeatures[i][j];
					cvReleaseImage(&image);

					ofstream ff(txt_path);
					for(int j = 0; j < face_data.size(); j++)
					{
						ff<<face_data[j];
						if( j < face_data.size()-1 )
							ff<<endl;
					}
					ff.close();
				}
			}
		}
		
		LeaveCriticalSection(&g_csf);
	}

	ClearCStringVector(sigleData);

	while( pCapture && curframeid <( vframes-3))
	{	
		if (FinishRecognitionFlag==true)
		{
			RecognitionOver=true;
			break;
		}

		curframeid++;
		if ( curframeid < vframes)
		{
			pFrame = cvQueryFrame( pCapture );
		}
		else
		{
			pFrame = NULL;
			break;
		}
		if (!pFrame)
		{
			break;
		}

		if (DetectionRecord!="#####")
		{
			tmp.Format("%09d",curframeid);
			Path=CurrentPath+"\\Record\\"+DetectionRecord+"\\"+tmp+".txt";

			if (PathFileExists(Path))
			{
				GetFaces(curframeid,DetectionRecord,face.face);
			}
			else
			{
				myDetectionForSingle(pFrame,cascade,face.face);

				WriteDetectionResults(Path,face.face);
			}
			
		}
		else
		{
			myDetectionForSingle(pFrame,cascade,face.face);
		}
		
		IplImage* tmp = NULL;
		MyCopyImage(tmp, pFrame);
		vector<int> recog_idx;
		if (m_sel_algorithm==sparse_coding)
		{
			recog_idx = myRecognitionForSingle(pFrame,cascade,face);//识别单个图片
		}
		else if (m_sel_algorithm==HD_LBP)
		{
			recog_idx = myRecognitionLBP(pFrame,cascade,face);//识别单个图片
		}
		allfaces.push_back(face);

		GetHeadPic(tmp,face);
		SaveFrameNum(curframeid,face);
		cvReleaseImage(&tmp);
		
		if( pFrame )
		{
			if (DetectionRecord=="#####")
			{
				CString tmp_s;
				tmp_s.Format("%09d",curframeid);
				Path=CurrentPath+"\\Record\\"+m_DetectionAnalysisTime+"\\"+tmp_s+".txt";
				WriteDetectionResults(Path,face.face);
			}
			
//			ShowImageInCenter(LoadedPicture, pFrame,false);
			if (MenuNum==1)
			{
				if (curframeid>0)
				{
					DrawFace( pFrame,face);
					ShowImageInCenter(LoadedPicture, pFrame,false,face);
				}

				ShowImageInCenter(m_dspbuf,0,openFile_y+27,m_client_w-1,m_client_h-openFile_y-27-1, LoadedPicture);
			}		
//			showCurrentPicture();
		}

		if (MenuFlag==true)
		{
			Sleep(100);
		}
		
		if (MenuNum==1)
		{			
			if (MenuFlag==false)
			{
				ShowRecognition(curframeid,vframes);
			}
		}
		else if(!(MenuNum==0&&DetectionOver==false))
		{
			((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_HIDE);
		}
	}


	cvReleaseCapture(&pCapture);

	g_thread_Recognition=NULL;

	Path=CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+"_Census.txt";
	WriteRecognitionCensusResults(Path,allfaces);

	Path=CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+".txt";
	WriteRecognitionResults(Path,allfaces);

	SetAnalysisTime(RecognitionTxtPath[m_sel_algorithm],m_RecognitionAnalysisTime);
	if (DetectionRecord=="#####")
	{
		SetAnalysisTime(DetectionTxtPath,m_DetectionAnalysisTime);
	}
	
	if (FinishRecognitionFlag==false)
	{
		MessageBox("识别完毕");
	}
	else
	{
		showVideo();
	}

	FinishRecognitionFlag=false;

	RecognitionOver=true;

	InitPictureColour(m_dspbuf,beginDect_x+100,m_client_w-159,beginDect_y,beginDect_y+26,240,240,240);//初始化背景色
	UpdateMyRectangle(beginDect_x+100,m_client_w-160,beginDect_y,beginDect_y+25);
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->ShowWindow(SW_HIDE);

	g_thread_Recognition=NULL;
}

void CFaceIdentificationDlg::GetFaces(int num,CString AnalysisTime,FACE &face)
{
	if (!face.faces.empty())
	{
		face.faces.clear();
		vector<rect>().swap(face.faces);
	}

	CString Path;//文件路径
	char *filePath;
	FILE *File=NULL;

	CString tmp;
	tmp.Format("%09d",num);
	Path=CurrentPath+"\\Record\\"+AnalysisTime+"\\"+tmp+".txt";
	filePath=Path.GetBuffer(Path.GetLength());

	struct rect faces;

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);

		for (int i=0;i<n;i++)
		{	
			fread(&faces.xmin,sizeof(int),1,File);	
			fread(&faces.ymin,sizeof(int),1,File);	
			fread(&faces.xmax,sizeof(int),1,File);	
			fread(&faces.ymax,sizeof(int),1,File);	
			face.faces.push_back(faces);
		}
		fclose(File);
		
	}
}

void CFaceIdentificationDlg::ShowDropDownList()
{
	ShowDropDownListFlag=true;
	DrawMyRectangle(110,200,m_recogParamY+32,m_recogParamY+82);

	SetDspbufPic(m_dspbuf,AlgorithmPath[2], 110, m_recogParamY+32, /*150*/90,25, 0,0, false);
	SetDspbufPic(m_dspbuf,AlgorithmPath[3], 110, m_recogParamY+57, /*150*/90,25, 0,0, false);

	InitPictureColour(m_dspbuf,110,/*260*/200,m_recogParamY+57,m_recogParamY+58,0,0,0);//初始化背景色

	CRect tmp;
	tmp.left=90;
	tmp.right=260/*90*/;
	tmp.top=m_recogParamY+25;
	tmp.bottom=m_recogParamY+100;
	InvalidateRect(tmp,FALSE);
}

void CFaceIdentificationDlg::ShowDBDropDown()
{
	m_dbDrowDownFlag = true;
	SetDspbufPic(m_dspbuf,m_dbImgPath, m_baseBtnX, m_baseBtnY+myButton_h, 120,25, 0,0, false);
	SetDspbufPic(m_dspbuf,m_dbTxtPath, m_baseBtnX, m_baseBtnY+2*myButton_h, 120,25, 0,0, false);

	CRect tmp;
	tmp.left=m_baseBtnX;
	tmp.right=m_baseBtnX+120;
	tmp.top=m_baseBtnY+myButton_h;
	tmp.bottom=m_baseBtnY+myButton_h+2*25;
	InvalidateRect(&tmp,FALSE);
//	Invalidate(FALSE);
}

void CFaceIdentificationDlg::ShowDropDown(vector<string> pic_path, IplImage *bg, int x, int y, int w, int h)
{
	for(int i = y; i < y + pic_path.size()*h; i++)
		for(int j = x; j < x + w; j++)
		{
			bg->imageData[(i-y)*bg->widthStep + (j-x)*bg->nChannels + 0] = m_dspbuf->imageData[i*m_dspbuf->widthStep+j*m_dspbuf->nChannels+0];
			bg->imageData[(i-y)*bg->widthStep + (j-x)*bg->nChannels + 1] = m_dspbuf->imageData[i*m_dspbuf->widthStep+j*m_dspbuf->nChannels+1];
			bg->imageData[(i-y)*bg->widthStep + (j-x)*bg->nChannels + 2] = m_dspbuf->imageData[i*m_dspbuf->widthStep+j*m_dspbuf->nChannels+2];
		}

	for(int i = 0; i < pic_path.size(); i++)
		SetBackPic(pic_path[i].c_str(), x, y+i*h, w,h,0,0,false);

	CRect rect;
	rect.left = x, rect.right = x + w;
	rect.top = y, rect.bottom = y + pic_path.size() * h;
	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	InvalidateRect(&rect, FALSE);
	//Invalidate(FALSE);
}

void CFaceIdentificationDlg::ClearDropDownList(IplImage *bg, int x, int y)
{
	for(int i = 0; i < bg->height; i++)
		for(int j = 0; j < bg->width; j++)
		{
			m_dspbuf->imageData[(i+y)*m_dspbuf->widthStep+(j+x)*m_dspbuf->nChannels+0] = bg->imageData[i*bg->widthStep+j*bg->nChannels+0];
			m_dspbuf->imageData[(i+y)*m_dspbuf->widthStep+(j+x)*m_dspbuf->nChannels+1] = bg->imageData[i*bg->widthStep+j*bg->nChannels+1];
			m_dspbuf->imageData[(i+y)*m_dspbuf->widthStep+(j+x)*m_dspbuf->nChannels+2] = bg->imageData[i*bg->widthStep+j*bg->nChannels+2];
		}

	CRect rect;
	rect.left = x, rect.right = x + bg->width;
	rect.top = y, rect.bottom = y + bg->height;
	InvalidateRect(rect, FALSE);
}

void CFaceIdentificationDlg::ClearDBDropDown()
{
	m_dbDrowDownFlag=false;

	CRect tmp;
	tmp.left=m_baseBtnX;
	tmp.right=m_baseBtnX+120;
	tmp.top=m_baseBtnY+myButton_h;
	tmp.bottom=m_baseBtnY+myButton_h+50;

	int sx = m_baseBtnX, sy = m_baseBtnY+myButton_h;
	for(int i = 0; i < m_db_dropdown_bk->height; i++)
		for(int j = 0; j < m_db_dropdown_bk->width; j++)
		{
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ] = 
				m_db_dropdown_bk->imageData[ i * m_db_dropdown_bk->widthStep + j * m_db_dropdown_bk->nChannels + 0 ];

			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ] = 
				m_db_dropdown_bk->imageData[ i * m_db_dropdown_bk->widthStep + j * m_db_dropdown_bk->nChannels + 1 ];

			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ] = 
				m_db_dropdown_bk->imageData[ i * m_db_dropdown_bk->widthStep + j * m_db_dropdown_bk->nChannels + 2 ];
		}

	InvalidateRect(tmp,FALSE);
}

void CFaceIdentificationDlg::SetDspbufPic(IplImage* &dst,CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f)//f==false原图，f==true镜像对称
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	int i,j,i1,j1,i2,j2;

	if (f==false)
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x+j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(dst,j1,i1,temp,j2,i2);
			}
	}
	else if (f==true)//右边镜像对称
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x-j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(dst,j1,i1,temp,j2,i2);
			}
	}
	cvReleaseImage(&temp);
}
void CFaceIdentificationDlg::ClearDropDownList()
{
	ShowDropDownListFlag=false;

	CRect tmp;
	tmp.left=100;
	tmp.right=270;
	tmp.top=m_recogParamY+25;
	tmp.bottom=m_recogParamY+90;
	
//	InitPictureColour(m_dspbuf,tmp.left,tmp.right,tmp.top,tmp.bottom,240,240,240);//初始化背景色
//	MyCopyImage(m_dspbuf,100,m_recogParamY+25,image_settings);
	int sx = m_alg_dropdown_rect.xmin, sy = m_alg_dropdown_rect.ymin;
	for(int i = 0; i < m_alg_dropdown_bk->height; i++)
		for(int j = 0; j < m_alg_dropdown_bk->width; j++)
		{
			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ] = 
				m_alg_dropdown_bk->imageData[ i * m_alg_dropdown_bk->widthStep + j * m_alg_dropdown_bk->nChannels + 0 ];

			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ] = 
				m_alg_dropdown_bk->imageData[ i * m_alg_dropdown_bk->widthStep + j * m_alg_dropdown_bk->nChannels + 1 ];

			m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ] = 
				m_alg_dropdown_bk->imageData[ i * m_alg_dropdown_bk->widthStep + j * m_alg_dropdown_bk->nChannels + 2 ];
		}

	InvalidateRect(tmp,FALSE);

	int pic_sx = 30, pic_sy = 100;
	CRect rpic;
	rpic.left = pic_sx, rpic.right = pic_sx + m_db_image->width;
	rpic.top = pic_sy, rpic.bottom = pic_sy + m_db_image->height;
	InvalidateRect(rpic,FALSE);
}
void CFaceIdentificationDlg::ShowCurrentAlgorithm()
{
	SetDspbufPic(m_dspbuf,AlgorithmPath[m_sel_algorithm], 110, m_recogParamY, /*150*/90,25, 0,0, false);

	CRect tmp;
	tmp.left=110;
	tmp.right=200;
	tmp.top=m_recogParamY;
	tmp.bottom=m_recogParamY+25;
	InvalidateRect(tmp,FALSE);
}

void CFaceIdentificationDlg::DrawMyRectangle(int l,int r,int t,int b)
{
	SetDspbufPic(m_dspbuf,rightbottom, r-16, b-16, 25,25, 0,0, false);
	SetDspbufPic(m_dspbuf,rightbottom, l+15, b-16, 23,23, 0,0, true);
	CopyInHorizontal(m_dspbuf, l+15,r-16,b,b+7, l+15-1);
	CopyImageInVerticalByMirror(m_dspbuf,l-7,r+10,b-15,b+6,t-7);
	CopyInVertical(m_dspbuf,l-7,l,t+10,b-10,t+9);
	CopyInVertical(m_dspbuf,r,r+7,t+10,b-10,t+9);
}

void CFaceIdentificationDlg::CopyInHorizontal(IplImage* image,int l,int r,int t,int b,int x)
{
	int i,j;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<b;j++)
		{
			SetPointColour(image,i,j,image,x,j);
		}
	}
}
void CFaceIdentificationDlg::CopyImageInVerticalByMirror(IplImage* image,int l,int r,int t,int b,int y)
{
	int i,j;

	for (i=l;i<r;i++)
	{
		for (j=0;j<=(b-t);j++)
		{
			SetPointColour(image,i,(y+j),image,i,(b-j));
		}
	}
}
void CFaceIdentificationDlg::CopyInVertical(IplImage* image,int l,int r,int t,int b,int y)
{
	int i,j;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			SetPointColour(image,i,j,image,i,y);
		}
	}
}
void CFaceIdentificationDlg::RateOfAdvance(IplImage* image,int l,int r,int t,int b)//画一个进度条
{
	int i,j;
	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			if (i==l||i==r||j==t||j==b)
			{
				SetPointColour(image,i ,j,0,0,0);
			}
		}
	}
}
void CFaceIdentificationDlg::RateOfAdvance(IplImage* image,int l,int r,int t,int b,float rate)//画一个进度条
{
	int i,j;
	int k=(int)((r-l)*rate);
	int g;
	for (i=l+1;i<(l+k);i++)
	{
		g=180;
		for (j=t+1;j<b;j++)
		{
			if (j<=(t+b)/2)
			{
				g=g+5;
			}
			else
			{
				g=g-5;
			}
			SetPointColour(image,i ,j,0,g,0);
		}
	}
}
void CFaceIdentificationDlg::UpdateMyRectangle(int l,int r,int t,int b)
{
	CRect tmp;
	tmp.left=l;
	tmp.right=r+1;
	tmp.top=t;
	tmp.bottom=b+1;
	InvalidateRect(tmp,FALSE);
}

void CFaceIdentificationDlg::OnNMClickListbase(NMHDR *pNMHDR, LRESULT *pResult)
{
	if (MenuNum!=3)
	{
		return;
	}
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	int item = pNMItemActivate->iItem;
	int subitem = pNMItemActivate->iSubItem;

	if( item != -1 )
	{
		CString sid = m_list.GetItemText(item, 1);
		CString sname = m_list.GetItemText(item, 2);
		CString ssex = m_list.GetItemText(item, 3);
		CString sage = m_list.GetItemText(item, 4);

		if( m_nid != sid )
		{
			m_nid = sid;
			m_strname = sname;
			m_bsex = ssex;
			m_nage = sage;
			UpdateData(FALSE);

			string path;
			for(int i = 0; i < m_db_info.size(); i++)
			{
				if( m_db_info[i].id == atoi(m_nid) )
				{
					path = m_db_info[i].img_path;
					SetAttentionCheck(m_db_info[i].battention);

					if( m_bsex != m_db_info[i].sex )
					{
						if( m_db_info[i].sex == "男" )
							SetBackPic(m_msex_path, m_sex_x, m_sex_y, 70, 25, 0, 0, false);
						else
							SetBackPic(m_fsex_path, m_sex_x, m_sex_y, 70, 25, 0, 0, false);

						GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);

						CRect r;
						r.left = m_sex_x, r.right = m_sex_x + 70, r.top = m_sex_y, r.bottom = m_sex_y + 25;
						InvalidateRect(r, FALSE);

						m_bsex = m_db_info[i].sex;
					}
					break;
				}
			}

			IplImage *tmp = cvLoadImage(path.c_str());
			cvResize(tmp, m_db_image);

			int sx = 30, sy = 100;
			CRect rpic;
			rpic.left = sx, rpic.right = sx + m_db_image->width;
			rpic.top = sy, rpic.bottom = sy + m_db_image->height;
			for(int i = 0; i < m_db_image->height; i++)
				for(int j = 0; j < m_db_image->width; j++)
				{
					m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 0 ] = m_db_image->imageData[ i*m_db_image->widthStep + j*m_db_image->nChannels + 0 ];
					m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 1 ] = m_db_image->imageData[ i*m_db_image->widthStep + j*m_db_image->nChannels + 1 ];
					m_dspbuf->imageData[ (i+sy)*m_dspbuf->widthStep + (j+sx)*m_dspbuf->nChannels + 2 ] = m_db_image->imageData[ i*m_db_image->widthStep + j*m_db_image->nChannels + 2 ];
				}

			cvReleaseImage(&tmp);
			InvalidateRect(&rpic,FALSE);
			//Invalidate(FALSE);
			m_CurDBPath=path.c_str();
		}
	}

	*pResult = 0;
}
void CFaceIdentificationDlg::WriteRecognitionResults(CString txtPath,vector<ALLFACE> &face)
{
	int i,j,n,k;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	CString filepath,name;

	FILE *File=NULL;
	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		n= face.size();
		fwrite(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			k=face[i].face.faces.size();
			fwrite(&k,sizeof(int),1,File);	
			if (k==0)
			{
				continue;
			}
			for (j=0;j<k;j++)
			{
				fwrite(&face[i].face.faces[j].xmin,sizeof(int),1,File);	
				fwrite(&face[i].face.faces[j].ymin,sizeof(int),1,File);	
				fwrite(&face[i].face.faces[j].xmax,sizeof(int),1,File);	
				fwrite(&face[i].face.faces[j].ymax,sizeof(int),1,File);
				GetDBName(face[i].name[j],filepath,name);
				WriteCStringR(name,File);
			}
			
		}
		fclose(File);
	}

	File=NULL;
}
void CFaceIdentificationDlg::ReadRecognitionResults(CString txtPath,vector<ALLFACE> &face)
{
	int i,j,n,k;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());

	ALLFACE f;
	struct rect temp;
	CString tmp;

	FILE *File=NULL;
	fopen_s(&File,filePath,"rb");
	if (File)//打开成功
	{
		fread(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			fread(&k,sizeof(int),1,File);	
			if (!f.face.faces.empty())
			{
				f.face.faces.clear();
				vector<rect>().swap(f.face.faces);
			}
			if (!f.name.empty())
			{
				f.name.clear();
				vector<CString>().swap(f.name);
			}
			if (k==0)
			{
				face.push_back(f);
				continue;
			}

			
			for (j=0;j<k;j++)
			{
				fread(&temp.xmin,sizeof(int),1,File);	
				fread(&temp.ymin,sizeof(int),1,File);	
				fread(&temp.xmax,sizeof(int),1,File);	
				fread(&temp.ymax,sizeof(int),1,File);	
				f.face.faces.push_back(temp);
				ReadCString(tmp,File);
				f.name.push_back(tmp);
			}
			face.push_back(f);
		}
		fclose(File);
	}

	File=NULL;
}

void CFaceIdentificationDlg::WriteDBinfo(DBINFO info)
{
	int i;
	char*filePath;
	CString txtPath=CurrentPath+"\\DBinfo.txt";
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	vector<DBINFO> db_info;
	vector<CString> db_time;
	FILE *File=NULL;
	if (PathFileExists(txtPath))
	{
		ReadDBinfo(db_info,db_time);//读取DBinfo
	}

	bool f=false;
	if (db_info.size()>0)
	{
		for (i=0;i<db_info.size();i++)
		{
			if (db_info[i].img_path==info.img_path)
			{
				db_info[i].age=info.age;
				db_info[i].sex=info.sex;
				db_info[i].name=info.name;
				db_info[i].id=info.id;
				f=true;
				break;
			}
			if (db_info[i].id==info.id)
			{
				db_info[i].id=info.id+1;
			}
		}
	}
	
	if (f==false)
	{
		db_info.push_back(info);
		SYSTEMTIME tmp_time;
		GetLocalTime(&tmp_time);
		CString AnalysisTime;
		AnalysisTime.Format("%4d%02d%02d%02d%02d%02d",tmp_time.wYear,tmp_time.wMonth,tmp_time.wDay,tmp_time.wHour,tmp_time.wMinute,tmp_time.wSecond);
		db_time.push_back(AnalysisTime);
		CreateNewDir(CurrentPath+"\\DB\\"+AnalysisTime);

		IplImage *img = cvLoadImage(info.img_path.c_str());
		IplImage *tmp_img = cvCreateImage(cvSize(35, 35), 8, 3);
		cvResize(img, tmp_img);
		cvSaveImage(CurrentPath+"\\DB\\"+AnalysisTime+"\\"+info.name+".bmp", tmp_img);
		cvReleaseImage(&img);
		cvReleaseImage(&tmp_img);
	}

		
	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n=db_info.size();
		fwrite(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			WriteCStringR(db_info[i].img_path.c_str(),File);
			WriteCStringR(db_time[i],File);
			fwrite(&db_info[i].age,sizeof(int),1,File);	
			fwrite(&db_info[i].id,sizeof(int),1,File);	
			WriteCStringR(db_info[i].name,File);
			WriteCStringR(db_info[i].sex,File);
		}
		fclose(File);
	}

	if (!db_info.empty())
	{
		db_info.clear();
		vector<DBINFO>().swap(db_info);
	}
	ClearCStringVector(db_time);
	

	File=NULL;
}
void CFaceIdentificationDlg::ReadDBinfo(vector<DBINFO>& db_info,vector<CString>&db_time)
{
	if (!db_info.empty())
	{
		db_info.clear();
		vector<DBINFO>().swap(db_info);
	}
	ClearCStringVector(db_time);

	int i;
	char*filePath;
	CString txtPath=CurrentPath+"\\DBinfo.txt";
	filePath=txtPath.GetBuffer(txtPath.GetLength());

	DBINFO tmp_db_info;
	CString tmp_db_time;

	FILE *File=NULL;

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			ReadCString(tmp_db_time,File);
			tmp_db_info.img_path=tmp_db_time.GetBuffer();
			ReadCString(tmp_db_time,File);
			fread(&tmp_db_info.age,sizeof(int),1,File);	
			fread(&tmp_db_info.id,sizeof(int),1,File);	
			ReadCString(tmp_db_info.name,File);
			ReadCString(tmp_db_info.sex,File);
			db_info.push_back(tmp_db_info);
			db_time.push_back(tmp_db_time);
		}
		fclose(File);
	}

	File=NULL;
}
void CFaceIdentificationDlg::GetDBName(CString &db_time,CString &filepath,CString &name)
{
	int i;
	char*filePath;
	CString txtPath=CurrentPath+"\\DBinfo.txt";
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	CString tmp;
	int tmp_int;
	FILE *File=NULL;
	name="";

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			ReadCString(tmp,File);
			if (tmp==db_time)
			{
				ReadCString(filepath,File);
				fread(&tmp_int,sizeof(int),1,File);	
				fread(&tmp_int,sizeof(int),1,File);	
				ReadCString(name,File);
				break;
			}
			ReadCString(tmp,File);
			fread(&tmp_int,sizeof(int),1,File);	
			fread(&tmp_int,sizeof(int),1,File);	
			ReadCString(tmp,File);
			ReadCString(tmp,File);
			
		}
		fclose(File);
	}

	File=NULL;
}
void CFaceIdentificationDlg::GetHeadPic(IplImage* image,ALLFACE &face)
{
	CreateNewDir(CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime);
	for(int i = 0; i < face.face.faces.size(); i++)
	{
		if (face.name[i].GetLength()<1)
		{
			continue;
		}
		CString filepath,name;
		GetDBName(face.name[i],filepath,name);
		CString Path=CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+"\\"+name+".bmp";
		if (PathFileExists(Path))
		{
			continue;
		}
		int w = face.face.faces[i].xmax - face.face.faces[i].xmin + 1;
		int h = face.face.faces[i].ymax - face.face.faces[i].ymin + 1;


		IplImage *fimg = cvCreateImage(cvSize(w,h), 8, 3);

		for (int j=0;j<w;j++)
		{
			for (int k=0;k<h;k++)
			{
				SetPointColour(fimg,j,k,image, face.face.faces[i].xmin +j,face.face.faces[i].ymin +k);
			}
		}
		cvSaveImage(Path,fimg);
		cvReleaseImage(&fimg);


//		AddSingleData(face.name[i],Path);
		sigleData.push_back(face.name[i]);
		sigleData.push_back(Path);
		if (QueryRecognitionFlag==false)
		{
			showCensusLive();
		}
		
	}
}
void CFaceIdentificationDlg::WriteRecognitionCensusResults(CString txtPath,vector<ALLFACE> &face)
{
	int i,j,n,k;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	CString filepath,name;
	vector<CString> names;

	n= face.size();

	for (i=0;i<n;i++)
	{	
		k=face[i].name.size();

		if (k==0)
		{
			continue;
		}
		for (j=0;j<k;j++)
		{
			if (names.size()<1)
			{
				if (face[i].name[j].GetLength()>0)
				{
					names.push_back(face[i].name[j]);
				}
			}
			else
			{
				int size=(int)names.size();
				bool f=false;
				for (int i1=0;i1<size;i1++)
				{
					if (face[i].name[j]==names[i1])
					{
						f=true;
						break;
					}
				}
				if (f==false)
				{
					if (face[i].name[j].GetLength()>0)
					{
						names.push_back(face[i].name[j]);
					}
					
				}
			}
		}

	}

	int age;
	CString gender;
	int id;

	FILE *File=NULL;
	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		n=names.size();
		fwrite(&n,sizeof(int),1,File);
		for (i=0;i<n;i++)
		{
			CString tmp_DBFile;
			GetDBinfo(names[i],name,age,gender,id,tmp_DBFile);
			WriteCStringR(names[i],File);
			WriteCStringR(name,File);
			WriteCStringR(CurrentPath+"\\DB\\"+tmp_DBFile+"\\"+name+".bmp",File);
			WriteCStringR(gender,File);
			fwrite(&age,sizeof(int),1,File);
			fwrite(&id,sizeof(int),1,File);
		}
		
		fclose(File);
	}

	File=NULL;

	vector<CString> tmp_info;
	CString Path;
	n=names.size();
	for (i=0;i<n;i++)
	{
		GetDBName(names[i],filepath,name);
		Path=CurrentPath+"\\DB\\"+filepath+"\\census.txt";
		ReadCensusResults(Path,tmp_info);

		bool f=false;
		if (tmp_info.size()>0)
		{
			for (int k=0;k<(int)tmp_info.size()/2;k++)
			{
				if (RecognitionFilePath==tmp_info[2*k])
				{
					tmp_info[2*k+1]=CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+"\\"+name+".bmp";
					f=true;
					break;
				}
			}
		}
		if (f==false)
		{
			tmp_info.push_back(RecognitionFilePath);
			tmp_info.push_back(CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+"\\"+name+".bmp");
		}
		WriteCensusResults(Path,tmp_info);
		ClearCStringVector(tmp_info);
	}
}

void CFaceIdentificationDlg::GetDBinfo(CString db_path,CString &name,int &age,CString &gender,int &id,CString &DBFile)
{
	int i;
	char*filePath;
	CString txtPath=CurrentPath+"\\DBinfo.txt";
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	CString tmp;
	int tmp_int;
	FILE *File=NULL;
	name="";

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n;
		fread(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			ReadCString(tmp,File);
			if (tmp==db_path)
			{
				ReadCString(DBFile,File);
				fread(&age,sizeof(int),1,File);	
				fread(&id,sizeof(int),1,File);	
				ReadCString(name,File);
				ReadCString(gender,File);
				break;
			}
			ReadCString(tmp,File);
			fread(&tmp_int,sizeof(int),1,File);	
			fread(&tmp_int,sizeof(int),1,File);	
			ReadCString(tmp,File);
			ReadCString(tmp,File);

		}
		fclose(File);
	}

	File=NULL;
}
void CFaceIdentificationDlg::ReadRecognitionCensusResults(CString txtPath,vector<CString> &tmp_info,vector<int> &tmp_inf)
{
	ClearCStringVector(tmp_info);
	if (!tmp_inf.empty())
	{
		tmp_inf.clear();
		vector<int>().swap(tmp_inf);
	}

	int i,j,n,k;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	CString tmp;

	int tmp_age;

	FILE *File=NULL;
	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		fread(&n,sizeof(int),1,File);
		for (i=0;i<n;i++)
		{
			ReadCString(tmp,File);//路径
			tmp_info.push_back(tmp);
			ReadCString(tmp,File);//姓名
			tmp_info.push_back(tmp);
			ReadCString(tmp,File);//数据库35*35图片
			tmp_info.push_back(tmp);
			ReadCString(tmp,File);//性别
			tmp_info.push_back(tmp);

			fread(&tmp_age,sizeof(int),1,File);//年龄
			tmp_inf.push_back(tmp_age);
			fread(&tmp_age,sizeof(int),1,File);//id
			tmp_inf.push_back(tmp_age);
		}

		fclose(File);
	}

	File=NULL;
}

void CFaceIdentificationDlg::ShowRecognitionCensusData(CString path,CString AnalysisTime)
{
	int fileType=testOpenfile(path,true);

	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetFont(&font);

	CString title;
	int pos = path.ReverseFind('\\'); 
	title = path.Right(path.GetLength()-pos-1);
	title=title+"  识别信息统计结果";
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowText(title);

	if (fileType==1)//图片
	{
		RecognitionVideoCensusData(path,AnalysisTime);
	}
	else if (fileType==2)//视频
	{
		if (RecognitionOver==true)
		{
			RecognitionVideoCensusData(path,AnalysisTime);
		}	
	}
}

void CFaceIdentificationDlg::RecognitionVideoCensusData(CString path,CString AnalysisTime)
{
	fileAnalysisTime=AnalysisTime;
	int i;

// 	CWnd* pWnd = GetDlgItem(IDC_LIST_DETE);
// 	ListView_SetExtendedListViewStyle(((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->m_hWnd, ((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetStyle() | LVS_EX_GRIDLINES);
// 	pWnd->MoveWindow(ImageDisplayArea_x,ImageDisplayArea_y+50,ImageDisplayArea_w,ImageDisplayArea_h-50,FALSE);
// 	m_list.SetItemState(0,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
 	m_list.SetExtendedStyle(LVS_EX_GRIDLINES |LVS_EX_FULLROWSELECT);//显示选中的一行


	CString title[4]=
	{
		"姓名",
		"性别",
		"年龄",
		"ID",
	};

	CHeaderCtrl* pHeaderCtrl = m_list.GetHeaderCtrl();
	int Column=pHeaderCtrl->GetItemCount();

	if (Column>1)
	{
		for   (int  j=0;j < Column;j++)   
		{   
			m_list.DeleteColumn(0);   
		}
	}

	Column=pHeaderCtrl->GetItemCount();
// 	if (Column<1)
// 	{
// 		m_list.InsertColumn(0,"   ", LVCFMT_CENTER,50);
// 	}

	m_list.InsertColumn(0,"头像", LVCFMT_CENTER,40);
	for (i=1;i<5;i++)
	{
		m_list.InsertColumn(i,title[i-1], LVCFMT_CENTER,70);
	}
	m_list.InsertColumn(5,"出现的时间范围", LVCFMT_CENTER,500);
	m_list.InsertColumn(6,"识别日期", LVCFMT_CENTER,150);
	int iIndex=0;
	iIndex =m_list.GetItemCount();


	for (int j=0;j<iIndex;j++)//删除所有行
	{
		m_list.DeleteItem(0);
	}

	vector<CString> tmp_info;
	vector<int> tmp_inf;

	CString txtPath=CurrentPath+"\\Record\\"+RecognitionAnalysisTime+"_Census.txt";
	ReadRecognitionCensusResults(txtPath,tmp_info,tmp_inf);

	 int sum = m_img_list.GetImageCount();
	 for(int i=0;i<sum;i++)
	 {
	  m_img_list.Remove(0);
	 } 
	 ClearCStringVector(m_info);
	 int k=tmp_inf.size()/2;
	 if (k>0)
	 {
		 for (i=0;i<k;i++)
		 {
			 m_info.push_back(tmp_info[4*i]);
			 m_info.push_back(CurrentPath+"\\Record\\"+RecognitionAnalysisTime+"\\"+tmp_info[4*i+1]+".bmp");

			 HBITMAP hBitmap;
			 CBitmap *pBitmap;
			 //CString FilePathName = "li2.bmp";
			 pBitmap = new   CBitmap;
			 // 从文件导入位图
			 hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), tmp_info[4*i+2],IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
			 pBitmap->Attach(hBitmap);
			 int a = m_img_list.Add(pBitmap, RGB(0,0,0));

			 m_list.InsertItem(a,"",a);
			 m_list.SetItemText(a, 1, tmp_info[4*i+1]);
			 m_list.SetItemText(a, 2, tmp_info[4*i+3]);
			 CString s;
			 s.Format("%d",tmp_inf[2*i]);
			 m_list.SetItemText(a, 3,s );
			 s.Format("%d",tmp_inf[2*i+1]);
			 m_list.SetItemText(a, 4, s);

			 CString tmp_s;
			 tmp_s=CurrentPath+"\\Record\\"+RecognitionAnalysisTime+"\\"+tmp_info[4*i+1]+".txt";
			 if(PathFileExists(tmp_s))
			 {
				 GetFrameNum(tmp_s,s);
				 m_list.SetItemText(a, 5, s);
			 }

			 MyGetTimeByAnalysisTime(RecognitionAnalysisTime,s);
			 m_list.SetItemText(a, 6, s);

			 int cnt = m_img_list.GetImageCount();
			 //		delete pBitmap;

// 			 if( cnt == 1 )
// 				 m_list.SetImageList(&m_img_list, /*LVSIL_NORMAL*/LVSIL_SMALL);
		 }
	 }
	

	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowPos(0,0,ImageDisplayArea_y+10,ImageDisplayArea_w,20,SWP_HIDEWINDOW);
	m_list.SetWindowPos(0,0,ImageDisplayArea_y+220,ImageDisplayArea_w,280,SWP_HIDEWINDOW);
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(true);
	m_list.ShowWindow(true);

}

void CFaceIdentificationDlg::OnChoose(NMHDR *pNMHDR, LRESULT *pResult)//统计界面选择一行
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	if (MenuNum==2)
	{
		NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
		if(LVIF_STATE == pNMListView->uChanged && (pNMListView->uNewState & LVIS_SELECTED))
		{
			DWORD dwPos = GetMessagePos();
			CPoint point( LOWORD(dwPos), HIWORD(dwPos) );
			m_list.ScreenToClient(&point); 

			LVHITTESTINFO lvinfo;
			lvinfo.pt = point;
			lvinfo.flags = LVHT_ABOVE;

			int nItem =m_list.SubItemHitTest(&lvinfo);

			if(nItem != -1)
			{
// 				CString strtemp;
// 				strtemp.Format("单击的是第%d行第%d列", lvinfo.iItem, lvinfo.iSubItem);
				ShowPicComparison( lvinfo.iItem);
			}
		}	

	}
	*pResult = 0;
}

void CFaceIdentificationDlg::ShowPicComparison(int index)
{
// 	SetDspbufPic(m_dspbuf,m_info[2*index], 0, ImageDisplayArea_y+100, 100,100, 0,0, false);
// 	SetDspbufPic(m_dspbuf,m_info[2*index+1], 100, ImageDisplayArea_y+100, 100,100, 0,0, false);

//	UpdateMyRectangle(0,m_client_w, ImageDisplayArea_y+100, ImageDisplayArea_y+150);
	DrawMyRectangle(90,240,ImageDisplayArea_y+50,ImageDisplayArea_y+200);
	SetDspbufPic(m_dspbuf,m_info[2*index],91,ImageDisplayArea_y+51,148,148);

	DrawMyRectangle(267,ImageDisplayArea_w-90,ImageDisplayArea_y+50,ImageDisplayArea_y+200);
	if (!PathFileExists(m_info[2*index+1]))
	{
		SetDspbufPic(m_dspbuf,unknown,268,ImageDisplayArea_y+51,148,148);
	}
	else
	{
		SetDspbufPic(m_dspbuf,m_info[2*index+1],268,ImageDisplayArea_y+51,148,148);
	}
	
	SetDspbufPic(m_dspbuf,unknown,417,ImageDisplayArea_y+51,148,148);
	SetDspbufPic(m_dspbuf,unknown,566,ImageDisplayArea_y+51,148,148);
	SetDspbufPic(m_dspbuf,unknown,715,ImageDisplayArea_y+51,148,148);
	Invalidate(FALSE);
}
void CFaceIdentificationDlg::WriteCensusResults(CString txtPath,vector<CString> &data)
{
	int i;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());

	FILE *File=NULL;
	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		int n= data.size()/2;
		fwrite(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			WriteCStringR(data[2*i],File);//视频路径
			WriteCStringR(data[2*i+1],File);//视频中的人脸路径
		}
		fclose(File);
	}

	File=NULL;
}
void CFaceIdentificationDlg::ReadCensusResults(CString txtPath,vector<CString> &data)
{
	ClearCStringVector(data);
	int i,n;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());

	CString tmp;

	FILE *File=NULL;
	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		fread(&n,sizeof(int),1,File);

		for (i=0;i<n;i++)
		{	
			ReadCString(tmp,File);//视频路径
			data.push_back(tmp);
			ReadCString(tmp,File);//视频中的人脸路径
			data.push_back(tmp);
		}
		fclose(File);
	}

	File=NULL;
}
void CFaceIdentificationDlg::RecognitionVideoCensusData(DBINFO db_info,CString AnalysisTime)
{
	fileAnalysisTime=AnalysisTime;
	int i;

	// 	CWnd* pWnd = GetDlgItem(IDC_LIST_DETE);
	// 	ListView_SetExtendedListViewStyle(((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->m_hWnd, ((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetStyle() | LVS_EX_GRIDLINES);
	// 	pWnd->MoveWindow(ImageDisplayArea_x,ImageDisplayArea_y+50,ImageDisplayArea_w,ImageDisplayArea_h-50,FALSE);
	// 	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->SetItemState(0,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES |LVS_EX_FULLROWSELECT);//显示选中的一行


	CString title[5]=
	{
		"头像",
		"姓名",
		"性别",
		"年龄",
		"ID"
	};

	CHeaderCtrl* pHeaderCtrl = m_list.GetHeaderCtrl();
	int Column=pHeaderCtrl->GetItemCount();

	if (Column>1)
	{
		for   (int  j=0;j < Column;j++)   
		{   
			m_list.DeleteColumn(0);   
		}
	}

	Column=pHeaderCtrl->GetItemCount();
// 	if (Column<1)
// 	{
// 		m_list.InsertColumn(0,"   ", LVCFMT_CENTER,50);
// 	}

	m_list.InsertColumn(0,"头像", LVCFMT_CENTER,40);
	for (i=1;i<5;i++)
	{
		m_list.InsertColumn(i,title[i], LVCFMT_CENTER,60);
	}
	m_list.InsertColumn(5,"视频 / 图片 路径", LVCFMT_CENTER,300);
	m_list.InsertColumn(6,"出现的时间范围", LVCFMT_CENTER,260);
	m_list.InsertColumn(7,"识别日期", LVCFMT_CENTER,150);
	int iIndex=0;
	iIndex =m_list.GetItemCount();


	for (int j=0;j<iIndex;j++)//删除所有行
	{
		m_list.DeleteItem(0);
	}


	vector<CString>tmp_info;
	CString Path=CurrentPath+"\\DB\\"+AnalysisTime+"\\census.txt";
	ReadCensusResults(Path,tmp_info);

	int sum = m_img_list.GetImageCount();
	for(int i=0;i<sum;i++)
	{
		m_img_list.Remove(0);
	} 
	ClearCStringVector(m_info);
	int k=tmp_info.size()/2;
	if (k>0)
	{
		for (i=0;i<k;i++)
		{
			CString tmp_path=CurrentPath+"\\DB\\"+AnalysisTime+"\\"+db_info.name+".bmp";
			m_info.push_back(db_info.img_path.c_str());
			m_info.push_back(tmp_info[2*i+1]);

			HBITMAP hBitmap;
			CBitmap *pBitmap;
			//CString FilePathName = "li2.bmp";
			pBitmap = new   CBitmap;
			// 从文件导入位图
			hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), tmp_path,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
			pBitmap->Attach(hBitmap);
			int a = m_img_list.Add(pBitmap, RGB(0,0,0));

			m_list.InsertItem(a,"",a);
			m_list.SetItemText(a, 1, db_info.name);
			m_list.SetItemText(a, 2, db_info.sex);
			CString s;
			s.Format("%d",db_info.age);
			m_list.SetItemText(a, 3,s );
			s.Format("%d",db_info.id);
			m_list.SetItemText(a, 4, s);
			m_list.SetItemText(a, 5, tmp_info[2*i]);
			int cnt = m_img_list.GetImageCount();
			//		delete pBitmap;

			CString tmp_s=tmp_info[2*i+1];
			tmp_s=tmp_s.Left(tmp_s.GetLength()-3);
			tmp_s=tmp_s+"txt";

			if(PathFileExists(tmp_s))
			{
				GetFrameNum(tmp_s,s);
				m_list.SetItemText(a, 6, s);
			}
			
			CString tmp_AnalysisTime=CurrentPath+"\\Record\\";
			tmp_AnalysisTime=tmp_info[2*i+1].Right(tmp_info[2*i+1].GetLength()-tmp_AnalysisTime.GetLength());
			tmp_AnalysisTime=tmp_AnalysisTime.Left(14);
			MyGetTimeByAnalysisTime(tmp_AnalysisTime,s);
			m_list.SetItemText(a, 7, s);

			if( cnt == 1 )
				m_list.SetImageList(&m_img_list, /*LVSIL_NORMAL*/LVSIL_SMALL);
		}
	}
	else
	{
		MessageBox("当前选择的数据库人员没有识别记录！");
	}


//	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowPos(0,0,ImageDisplayArea_y+10,ImageDisplayArea_w,20,SWP_HIDEWINDOW);
	m_list.SetWindowPos(0,0,ImageDisplayArea_y+220,ImageDisplayArea_w,280,SWP_HIDEWINDOW);
//	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(true);
	m_list.ShowWindow(true);

}
void CFaceIdentificationDlg::MyCopyImage(IplImage* &dst, IplImage* src,int x,int y,int w,int h)
{
	int i,j;

	cvReleaseImage(&dst);
	dst=cvCreateImage(cvSize(w,h),src->depth,src->nChannels);
	for(i =0; i <h; i++)
		for(j =0; j <w; j++)
		{
			SetPointColour(dst,j,i,src,x+j,y+i);
		}
}
void CFaceIdentificationDlg::MyCopyImage(IplImage* &dst,int x,int y, IplImage* src)
{
	int i,j;

	for(i =0; i <src->height; i++)
		for(j =0; j <src->width; j++)
		{
			SetPointColour(dst,x+j,y+i,src,j,i);
		}
}

int CFaceIdentificationDlg::IsValidString(CString str)
{
	if( str.Find('!')>=0 || str.Find('@')>=0 || str.Find('#')>=0 || str.Find('$')>=0 ||
		str.Find('%')>=0 || str.Find('&')>=0 || str.Find('.')>=0 || str.Find('^')>=0 )
		return 0;
	else
		return 1;
}
void CFaceIdentificationDlg::SetDspbufPic(IplImage* &dst,CString pathname,int x,int y,int w,int h)
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}
	IplImage* tem_image=cvCreateImage(cvSize(w,h),temp->depth,temp->nChannels);
	cvResize( temp, tem_image, CV_INTER_NN  );
	int i,j,i1,j1,i2,j2;

	for(i =0; i <h; i++)
		for(j =0; j < w; j++)
		{
			i1=y+i;
			j1=x+j;
			i2=i;
			j2=j;
			SetPointColour(dst,j1,i1,tem_image,j2,i2);
		}

	cvReleaseImage(&temp);
	cvReleaseImage(&tem_image);
}
void CFaceIdentificationDlg::MyGetTime(CString &AnalysisTime)
{
	SYSTEMTIME tmp_time;
	GetLocalTime(&tmp_time);
	AnalysisTime.Format("%4d%02d%02d%02d%02d%02d",tmp_time.wYear,tmp_time.wMonth,tmp_time.wDay,tmp_time.wHour,tmp_time.wMinute,tmp_time.wSecond);
}
void CFaceIdentificationDlg::originalDBImages()
{
	int cnt = m_img_list.GetImageCount();
	for(int i = 0; i < cnt; i++)
	{
		m_img_list.Remove(0);
	}
	for(int i = 0; i < m_db_info.size(); i++)
	{
		DBINFO db=m_db_info[i];

		string path = m_db_info[i].img_path;
		int l = path.rfind("\\");
		string dir = path.substr(0, l+1);
		string img_name = path.substr(l+1, path.length());
		string tmp_name = "tmp_";

		int r = img_name.rfind(".");
		string face_name = img_name.substr(0,r);
		tmp_name += face_name + ".bmp";
		string tmp_path = dir + tmp_name;

		IplImage *img = cvLoadImage(m_db_info[i].img_path.c_str());
		IplImage *tmp_img = cvCreateImage(cvSize(35, 35), 8, 3);
		cvResize(img, tmp_img);
		cvSaveImage(tmp_path.c_str(), tmp_img);
		cvReleaseImage(&img);
		cvReleaseImage(&tmp_img);

		int index = i;

		HBITMAP hBitmap;
		CBitmap *pBitmap;
		pBitmap = new   CBitmap;
		hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), /*img_path*/tmp_path.c_str(),IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		pBitmap->Attach(hBitmap);
		int a = m_img_list.Add(pBitmap, RGB(0,0,0));


		CString tmp;
	
		m_list.InsertItem(i,"",i);
		tmp.Format("%d",db.id);
		m_list.SetItemText(index, 1, tmp);
		m_list.SetItemText(index, 2, db.name);
		m_list.SetItemText(index, 3, db.sex);
		tmp.Format("%d",db.age);
		m_list.SetItemText(index, 4,tmp );

	}

}
void CFaceIdentificationDlg::QueryRecognitionCensusData(DBINFO db_info,CString AnalysisTime)
{
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetFont(&font);

	CString title;
	title=db_info.name +"  识别查询统计结果";
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowText(title);
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowPos(0,0,ImageDisplayArea_y+10,ImageDisplayArea_w,20,SWP_HIDEWINDOW);
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(true);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->MoveWindow(0,0,0,0,SWP_HIDEWINDOW);
	((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->ShowWindow(SW_HIDE);
	toCensusFlag=false;
	InitPictureColour(m_dspbuf,70,ImageDisplayArea_w-70,ImageDisplayArea_y+30,ImageDisplayArea_y+220,240,240,240);//初始化背景色
	UpdateMyRectangle(70,ImageDisplayArea_w-70,ImageDisplayArea_y+30,ImageDisplayArea_y+220);

	//设置 标志 区分当前统计界面是否是当前正在检测的视频
	QueryRecognitionFlag=true;
	RecognitionVideoCensusData(db_info,AnalysisTime);	
}
void CFaceIdentificationDlg::PictureRecognition()
{
	if (m_RecognitionAnalysisTime!="#####")
	{
		PictureRecognitionShow();
		return;
	}

	if (!g_dict)
	{
		MessageBox("请先创建数据库！");
		return;
	}
	int i,j;

	IplImage* image=NULL;
	myLoadPictureBy4channels(CurrentFilePath, image);
	if (!image)
	{
		return;
	}

	CString DetectionRecord;
	GetAnalysisTime(DetectionRecord,DetectionTxtPath);
	CString Path;//文件路径

	MyGetTime(m_RecognitionAnalysisTime);
	if (DetectionRecord=="#####")
	{
		MyGetTime(m_DetectionAnalysisTime);
		CreateNewDir(CurrentPath+"\\Record\\"+m_DetectionAnalysisTime);
	}

	CString cascade_path=CurrentPath+"\\face.xml";
	char *path = (LPSTR)(LPCTSTR)cascade_path;
	CascadeClassifier cascade;
	cascade.load(path);

	ALLFACE face;
	vector<ALLFACE> allfaces;

	if (DetectionRecord!="#####")
	{
		GetFaces(0,DetectionRecord,face.face);
	}
	else
	{
		myDetectionForSingle(image,cascade,face.face);
	}

	IplImage* tmp = NULL;
	MyCopyImage(tmp, image);
	if (m_sel_algorithm==sparse_coding)
	{
		myRecognitionForSingle(image,cascade,face);//识别单个图片
	}
	else if (m_sel_algorithm==HD_LBP)
	{
		myRecognitionLBP(image,cascade,face);//识别单个图片
	}

	GetHeadPic(tmp,face);
	cvReleaseImage(&tmp);
	allfaces.push_back(face);
	if( image )
	{
		if (DetectionRecord=="#####")
		{
			CString tmp_s;
			tmp_s.Format("%09d",0);
			Path=CurrentPath+"\\Record\\"+m_DetectionAnalysisTime+"\\"+tmp_s+".txt";
			WriteDetectionResults(Path,face.face);
		}

		if (MenuNum==1)
		{
			ShowImageInCenter(m_dspbuf,0,openFile_y+27,m_client_w-1,m_client_h-openFile_y-27-1, image);
		}		

	}


	Path=CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+"_Census.txt";
	WriteRecognitionCensusResults(Path,allfaces);

	Path=CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+".txt";
	WriteRecognitionResults(Path,allfaces);

	SetAnalysisTime(RecognitionTxtPath[m_sel_algorithm],m_RecognitionAnalysisTime);
	if (DetectionRecord=="#####")
	{
		SetAnalysisTime(DetectionTxtPath,m_DetectionAnalysisTime);
	}

	cvReleaseImage(&image);
}
void CFaceIdentificationDlg::PictureRecognitionShow()
{
	CString AnalysisTime;
	GetAnalysisTime(AnalysisTime,RecognitionTxtPath[m_sel_algorithm]);
	
	int i,j;

	IplImage* image=NULL;
	myLoadPictureBy4channels(CurrentFilePath, image);
	if (!image)
	{
		return;
	}

	vector<ALLFACE> face;
	if (AnalysisTime=="#####")
	{
		GetAnalysisTime(AnalysisTime,RecognitionTxtPath[1-m_sel_algorithm]);
	}
	if (AnalysisTime!="#####")
	{
		ReadRecognitionResults(CurrentPath+"\\Record\\"+AnalysisTime+".txt",face);
	}


	if( image )
	{
		if (face.size()>0)
		{
			DrawFace( image,face[0]);
			ShowImageInCenter(LoadedPicture, image,false,face[0]);
		}
		else
		{
			ShowImageInCenter(LoadedPicture, image,false);
		}

		showCurrentPicture();
	}
	cvReleaseImage(&image);

}
void CFaceIdentificationDlg::SaveFrameNum(int num,ALLFACE &face)
{
	int i;
	char*filePath;

	FILE *File=NULL;

	for(int i = 0; i < face.face.faces.size(); i++)
	{
		if (face.name[i].GetLength()<1)
		{
			continue;
		}

		CString filepath,name;
		GetDBName(face.name[i],filepath,name);
		face.name[i]=name;
		CString txtPath=CurrentPath+"\\Record\\"+m_RecognitionAnalysisTime+"\\"+name+".txt";
		filePath=txtPath.GetBuffer(txtPath.GetLength());
		if (PathFileExists(txtPath))
		{
			fopen_s(&File,filePath,"ab");
		}
		else
		{
			fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
		}
	
		if (File)//打开成功
		{
			fwrite(&num,sizeof(int),1,File);

			fclose(File);
		}
  		File=NULL;
	}
}
void CFaceIdentificationDlg::GetFrameNum(CString txtPath,CString& numString)
{
	numString="";
	int i,k;
	char*filePath;
	filePath=txtPath.GetBuffer(txtPath.GetLength());
	
	vector<int> num;
	FILE *File=NULL;
	fopen_s(&File,filePath,"rb");
	while(!feof(File))
	{ 
		fread(&k,sizeof(int),1,File);
		num.push_back(k);
	} 
	int begin,end;
	int n=num.size();
	CString tmp;
	begin=num[0];
	for (i=1;i<n;i++)
	{
		if (num[i]<=(num[i-1]+50))
		{
			continue;
		}
		else
		{
			end=num[i-1];
//			tmp.Format("%d",begin);
			NumToTime(tmp,begin);
			numString=numString+tmp+" ~ ";
//			tmp.Format("%d",end);
			NumToTime(tmp,end);
			numString=numString+tmp+"; ";
			begin=num[i];
		}
	}
	if (end<num[i-1])
	{
		end=num[i-1];
		NumToTime(tmp,begin);
		numString=numString+tmp+" ~ ";
		NumToTime(tmp,end);
		numString=numString+tmp+"; ";
	}
}
void CFaceIdentificationDlg::MyGetTimeByAnalysisTime(CString AnalysisTime,CString &mytime)
{
	CString tmp;
	tmp=AnalysisTime.Left(4)+"年";
	mytime=tmp;
	tmp=AnalysisTime.Right(10);
	tmp=tmp.Left(2)+"月";
	mytime=mytime+tmp;
	tmp=AnalysisTime.Right(8);
	tmp=tmp.Left(2)+"日";
	mytime=mytime+tmp;
}
void CFaceIdentificationDlg::DrawFace( IplImage* src,FACE face)
{
	int n=(int)face.faces.size();

	for (int i=0;i<n;i++)
	{	
		myDrawrectangular( src,face.faces[i].xmin,face.faces[i].ymin,face.faces[i].xmax,face.faces[i].ymax,255,255,255);
	}
}

void CFaceIdentificationDlg::DetectionPlaying()
{
	CString AnalysisTime;
	GetAnalysisTime(AnalysisTime,DetectionTxtPath);
	
	double vframes;

	CvCapture* pCapture = cvCaptureFromFile(DetectionFilePath);
	if( !pCapture)
	{
		AfxMessageBox("无效文件!");
		return ;
	}

	vframes = cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_COUNT);

	if (AnalysisTime!="#####")
	{
		DetectionSchedule=vframes;
	}

	int curframeid = 1;
	IplImage* pFrame = NULL;

	FACE face;

	pFrame = cvQueryFrame( pCapture );
	currentPos=-1;
	while( pFrame && curframeid < vframes)
	{

		if (MenuNum==0)//防止播放过程中 切换到其他界面
		{
			DrawFace(curframeid-1, pFrame,m_DetectionAnalysisTime);
		    ShowImageInCenter(m_dspbuf,ImageDisplayArea_x,ImageDisplayArea_y,ImageDisplayArea_w-1,ImageDisplayArea_h-10, pFrame,curframeid);
		}

		float rate=curframeid/vframes;
		RateOfAdvance(m_dspbuf,1,m_client_w-1,m_client_h-10,m_client_h-1);
		RateOfAdvance(m_dspbuf,1,m_client_w-1,m_client_h-10,rate);
		UpdateMyRectangle(0,m_client_w,m_client_h-8,m_client_h);


		Sleep(30);

		if (FastForwardRewindFlag==true)
		{
			FastForwardRewindFlag=false;
			if (FastForwardRewindNum>0&&FastForwardRewindNum*vframes<DetectionSchedule)
			{
				cvSetCaptureProperty(pCapture, CV_CAP_PROP_POS_FRAMES, FastForwardRewindNum*vframes);
				curframeid=FastForwardRewindNum*vframes;
			}
			FastForwardRewindNum=-1;
		}
		

		if (curframeid<DetectionSchedule)
		{
			pFrame = cvQueryFrame( pCapture );
			curframeid++;
		}
		
		
	}

	cvReleaseCapture(&pCapture);

	g_thread_VideoPlay[0]=NULL;
	currentPos=-1;
	showVideo();
}
void CFaceIdentificationDlg::RateOfAdvance(IplImage* image,int l,int r,int t,float rate)//画一个进度条
{
	int i,j;
	int k=(int)((r-l-24)*rate);
	currentPos=k;
	for (i=l+1;i<(l+k);i++)
	{
		for (j=t+1;j<(t+9);j++)
		{
			SetPointColour(image,i ,j,play_speed,3,(j-t+4));
		}
	}
	for (i=(l+k);i<r;i++)
	{
		for (j=t+1;j<(t+9);j++)
		{
			SetPointColour(image,i ,j,240,240,240);
		}
	}
	MyCopyImage(image,(l+k),t+1, play_speed,1,5,25,9);

}

void CFaceIdentificationDlg::NumToTime(CString &timeString,int  num)//将将帧数转化为时间
{
	int tmp;
	CString tmp_s;
	timeString="";

	tmp=num/9000;
	tmp_s.Format("%02d",tmp);
	timeString=tmp_s+":";

	num=num%9000;
	tmp=num/1500;
	tmp_s.Format("%02d",tmp);
	timeString=timeString+tmp_s+":";

	num=num%1500;
	tmp=num/25;
	tmp_s.Format("%02d",tmp);
	timeString=timeString+tmp_s;
}
void CFaceIdentificationDlg::MyCopyImage(IplImage* &dst,int x1,int y1, IplImage* src,int x,int y,int w,int h)
{
	int i,j;

	for(i =0; i <h; i++)
		for(j =0; j <w; j++)
		{
			SetPointColour(dst,x1+j,y1+i,src,x+j,y+i);
		}
}
void CFaceIdentificationDlg::ShowPlayaTime(int curframeid)
{
	InitPictureColour(m_dspbuf,6,90,m_client_h-40,m_client_h-20,240,240,240);
	CString tip;
	NumToTime(tip,curframeid);
	CvFont font;//以下用文字标识说明
	double hScale=1, vScale=1;
	int lineWidth=2;
	cvInitFont(&font,CV_FONT_HERSHEY_PLAIN  |CV_FONT_ITALIC,hScale,vScale,1,lineWidth,8);
	char *buf; 
	buf = (LPSTR)(LPCTSTR)tip;
	int point_x=10;
	int point_y=m_client_h-20;
	cvPutText(m_dspbuf, buf, cvPoint(point_x, point_y - 5), &font, CV_RGB(0,255,255));
}
void CFaceIdentificationDlg::ShowImageInCenter(IplImage* dst,int x,int y,int w,int h, IplImage* src,int curframeid)
{
	int width,height;
	width=src->width;
	height=src->height;

	double rate=((double)w)/width;
	if (rate<1)
	{
		width=w;
		height=src->height*rate;
	}

	rate=((double)h)/height;
	if (rate<1)
	{
		width=width*rate;
		height=h;
	}

	//	InitPictureColour(dst,x,x+w,y,y+h,215,215,215);//初始化背景色

	rate=((double)src->width)/width;

	x=x+(w-width)/2.0;
	y=y+(h-height)/2.0;

	int i,j;
	int i1,j1;

	for (i=x;i<(x+width);i++)
	{
		for (j=y;j<(y+height);j++)
		{
			i1=rate*(i-x);
			j1=rate*(j-y);
			SetPointColour(dst,i,j,src,i1,j1);
		}
	}

	if (picture_video_flag==true&&videoState==false)
	{
		showVideoPlayAndStop(playPath,play_r);
	}

	if (MenuNum==0)
	{
		int x1,y1;
		x1=m_client_w/2.0;
		y1=ImageDisplayArea_h/2.0+openFile_y+27;

		int r2=(mousePoint.x-x1)*(mousePoint.x-x1)+(mousePoint.y-y1)*(mousePoint.y-y1);

		if ((videoState[MenuNum]==true&&r2<stop_r*stop_r))
		{
			showVideoPlayAndStop(stopPath,stop_r);
		}
	}

	ShowPlayaTime(curframeid);
	UpdateMyRectangle(0,ImageDisplayArea_w,ImageDisplayArea_y,ImageDisplayArea_h+ImageDisplayArea_y);
}

void CFaceIdentificationDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (MenuNum==0)
	{
		if (draftingFlag==true)
		{
			draftingFlag=false;
		}
	}

	CDialogEx::OnLButtonUp(nFlags, point);
}
BOOL CFaceIdentificationDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_SPACE)
	{
		DetectionPauseAndResume();
	}
// 	else if(pMsg->message==WM_KEYUP)
// 	{
// 			
// 	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam=='S')
	{
		if (MenuNum==0&&DetectionOver==false)
		{
			if (IDOK==MessageBox("是否确定停止当前检测？","停止检测",MB_OKCANCEL|MB_ICONQUESTION))
			{
				FinishDetection();
				MessageBox("当前检测已经停止！");
			}
		}
		else if (MenuNum==1&&RecognitionOver==false)
		{
			if (IDOK==MessageBox("是否确定停止当前识别？","停止识别",MB_OKCANCEL|MB_ICONQUESTION))
			{
				FinishRecognition();
				MessageBox("当前识别已经停止！");
			}
		}


	}
	return CDialog::PreTranslateMessage(pMsg);
}
void CFaceIdentificationDlg::DetectionPauseAndResume()
{
	if (MenuNum==0&&DetectionOver==false)//检测界面，并且检测未结束
	{
		if (g_thread_detection)
		{
			if (DetectionState==true)
			{
				SuspendThread(g_thread_detection->m_hThread);
				DetectionState=false;

				((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText("                                                                 ");
				((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText("  检测已经暂停！");

				MessageBox("当前检测已经暂停！");

			}
			else
			{
				ResumeThread(g_thread_detection->m_hThread);
				DetectionState=true;
			}

		}
	}
	else if (MenuNum==1&&RecognitionOver==false)
	{
		if (g_thread_Recognition)
		{
			if (RecognitionState==true)
			{
				SuspendThread(g_thread_Recognition->m_hThread);
				RecognitionState=false;

				((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText("                                                                 ");
				((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText("  识别已经暂停！");

				MessageBox("当前识别已经暂停！");

			}
			else
			{
				ResumeThread(g_thread_Recognition->m_hThread);
				RecognitionState=true;
			}

		}
	}
}
void CFaceIdentificationDlg::FinishDetection()
{
	FinishDetectionFlag=false;

	if (MenuNum==0&&DetectionOver==false)//检测界面，并且检测未结束
	{
		if (g_thread_detection)
		{
			if (DetectionState==true)//当前检测处于运行
			{
				SuspendThread(g_thread_detection->m_hThread);//如果正在运行，就让他先暂停

			}

			FinishDetectionFlag=true;//设置结束检测标志，使检测结束循环

			ResumeThread(g_thread_detection->m_hThread);//继续检测会立即结束

			g_thread_detection=NULL;
		}
	}
	
}

void CFaceIdentificationDlg::SetBreakOffRecord(CString Path,CString currentfilePath,int framNum)//设置中途停止检测/识别记录
{
	vector<CString> paths;//存储路径以及编号
	ReadAnalysisTime(paths,Path);//停止检测/识别记录 文件

	int i;
	int n=(int)paths.size()/2;

	CString num;
	num.Format("%d",framNum);

	bool f=false;
	for (i=0;i<n;i++)
	{
		if (paths[2*i]==currentfilePath)
		{
			if (framNum<0)//清楚之前的中断记录
			{
				paths[2*i]==paths[n-2];
				paths[2*i+1]=paths[n-1];
			}
			else
			{
				paths[2*i+1]=num;
			}
			
			f=true;

			break;
		}
	}
	if (f==false)
	{
		paths.push_back(currentfilePath);//当前文件路径
		paths.push_back(num);//当前检测的帧
	}
	else if (f==true)
	{
		if (framNum<0)//清楚之前的中断记录
		{
			paths.pop_back();
			paths.pop_back();
		}
	}
	

	//文件路径
	char *filePath;
	FILE *File=NULL;

	filePath=Path.GetBuffer(Path.GetLength());
	CString tmp;

	
	n=(int)paths.size();

	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{

		for (i=0;i<n;i++)
		{	
			WriteCStringR(paths[i],File);		
		}
		WriteCStringR("Cognivisual Technologies.Inc",File);
	}

	fclose(File);
	File=NULL;

	ClearCStringVector(paths);
}

void CFaceIdentificationDlg::GetBreakOffRecord(CString Path,CString filePath,int &framNum)//获取中途停止检测/识别记录
{
	framNum=-1;

	vector<CString> paths;//存储路径以及编号
	ReadAnalysisTime(paths,Path);//停止检测/识别记录 文件

	int i;
	int n=(int)paths.size()/2;

	CString num;

	bool f=false;
	for (i=0;i<n;i++)
	{
		if (paths[2*i]==filePath)
		{
			num=paths[2*i+1];
			f=true;

			break;
		}
	}
	if (f==true)
	{
		framNum=atoi(num);
	}

	ClearCStringVector(paths);
}

void CFaceIdentificationDlg::FinishRecognition()
{
	FinishRecognitionFlag=false;

	if (MenuNum==1&&RecognitionOver==false)//识别界面，并且检测未结束
	{
		if (g_thread_Recognition)
		{
			if (RecognitionState==true)//当前检测处于运行
			{
				SuspendThread(g_thread_Recognition->m_hThread);//如果正在运行，就让他先暂停

			}

			FinishRecognitionFlag=true;//设置结束检测标志，使检测结束循环

			ResumeThread(g_thread_Recognition->m_hThread);//继续检测会立即结束

			g_thread_Recognition=NULL;
		}
	}

}
void CFaceIdentificationDlg::ShowRecognition(int curframeid,int vframes)
{
	double rate=(double)curframeid/( vframes-4);

	if (rate>=1.00)
	{
		rate=1.00;
	}

	CString title;

	title.Format("%f",(float)rate*100);
	title=title.Left(5);
	title="  识别进度 ："+title+" %";

	CString tmp_num,texts;
	tmp_num.Format("%d",curframeid);
	texts="已识别 "+tmp_num+"/";
	tmp_num.Format("%d",(int)(vframes-4));
	texts=texts+tmp_num+" 帧 ";
	title=texts+title;

	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText(title);
	RateOfAdvance(m_dspbuf,beginDect_x+100,m_client_w-270,beginDect_y,beginDect_y+25);
	RateOfAdvance(m_dspbuf,beginDect_x+100,m_client_w-270,beginDect_y,beginDect_y+25,rate);

	UpdateMyRectangle(beginDect_x+100,m_client_w-270,beginDect_y,beginDect_y+25);
}
void CFaceIdentificationDlg::ShowDetection(int curframeid,int vframes)
{
	double rate=(double)curframeid/( vframes-4);

	if (rate>=1.00)
	{
		rate=1.00;
	}
	CString title;

	title.Format("%f",(float)rate*100);
	title=title.Left(5);
	title="  检测进度 ："+title+" %";

	CString tmp_num,texts;
	tmp_num.Format("%d",curframeid);
	texts="已检测 "+tmp_num+"/";
	tmp_num.Format("%d",(int)(vframes-4));
	texts=texts+tmp_num+" 帧 ";
	title=texts+title;
	((CStatic *)GetDlgItem(IDC_STATIC_PRO))->SetWindowText(title);
	RateOfAdvance(m_dspbuf,beginDect_x,m_client_w-270,beginDect_y,beginDect_y+25);
	RateOfAdvance(m_dspbuf,beginDect_x,m_client_w-270,beginDect_y,beginDect_y+25,rate);			
	UpdateMyRectangle(beginDect_x,m_client_w-270,beginDect_y,beginDect_y+25);
}

UINT startDetectionForRecognition(LPVOID pParam)
{
	theApp.pFaceIdentificationDlg->DetectionForRecognition();
	return 1;
}

void CFaceIdentificationDlg::StartDetectionForRecognition()//专门为识别检测
{
	if (picture_video_f[1]==true)//视频识别
	{
		if (m_DetectionForRecognition==NULL)
		{
			m_DetectionForRecognition = AfxBeginThread((AFX_THREADPROC)startDetectionForRecognition,NULL);
		}
	}
}
void CFaceIdentificationDlg::DetectionForRecognition()//专门为识别检测
{
	if (RecognitionFilePath.GetLength()<1)//不用CurrentFilePath，这个线程只有重新打开识别，退出软件时，才终止，这个过程中会切换到检测界面，用CurrentFilePath会出现问题
	{
		return;
	}

	CString tmp_DetectionAnalysisTime;
	GetAnalysisTime(tmp_DetectionAnalysisTime,DetectionTxtPath);

	int framNum=-1;

	if (tmp_DetectionAnalysisTime!="#####")//已经检测过
	{
		GetBreakOffRecord(CurrentPath+"\\BreakOffD.txt",RecognitionFilePath,framNum);
		if (framNum<0)//没有中断过，已经完全检测完毕
		{
			m_DetectionForRecognition=NULL;
			return;
		}//否则从中断出继续检测

	}//没有检测过，或者检测中断过，继续
	
	if (framNum<0)//第一次检测时，才写入检测时间
	{
		MyGetTime(tmp_DetectionAnalysisTime);
		CreateNewDir(CurrentPath+"\\Record\\"+tmp_DetectionAnalysisTime);
		SetAnalysisTime(DetectionTxtPath,tmp_DetectionAnalysisTime);
	}


	CString Path;//文件路径
	CString tmp;

	////////////////////////////////////////
	double vframes;

	CvCapture* pCapture = cvCaptureFromFile(RecognitionFilePath);
	vframes = cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_COUNT);

	IplImage* pFrame = NULL;

	if( !pCapture)
	{
		return ;
	}


	CString cascade_path=CurrentPath+"\\face.xml";
	char *path = (LPSTR)(LPCTSTR)cascade_path;
	CascadeClassifier cascade;
	cascade.load(path);

	FACE face;

	int curframeid = 0;
	if (framNum>0)//第一次检测，此处＜0，如果是中断后的第二次检测，此处大于0
	{
		cvSetCaptureProperty(pCapture, CV_CAP_PROP_POS_FRAMES, framNum);
		curframeid=framNum-1;
	}
	pFrame = cvQueryFrame( pCapture );

	while( pFrame && curframeid <( vframes-3))
	{	
		if (FinishRecognitionFlag==true)
		{
			break;
		}
		curframeid++;

		if (pFrame)
		{
//			myDetectionForSingle(pFrame,cascade,face);
		
			tmp.Format("%09d",curframeid);
			Path=CurrentPath+"\\Record\\"+tmp_DetectionAnalysisTime+"\\"+tmp+".txt";

			if (!PathFileExists(Path))
			{
				myDetectionForSingle(pFrame,cascade,face);
				WriteDetectionResults(Path,face);
			}

		}


		if ( curframeid < vframes)
		{
			pFrame = cvQueryFrame( pCapture );
		}
		else
		{
			pFrame = NULL;

		}
	}

	cvReleaseCapture(&pCapture);


	if (FinishRecognitionFlag==false)
	{
		if (framNum>0)//第一次检测，此处＜0，如果是中断后的第二次检测，此处大于0
		{
			SetBreakOffRecord(CurrentPath+"\\BreakOffD.txt",RecognitionFilePath,-1);
		}

	}
	else if (FinishRecognitionFlag==true)//中途停止的情况
	{
		SetBreakOffRecord(CurrentPath+"\\BreakOffD.txt",RecognitionFilePath,curframeid+1);
	}

	m_DetectionForRecognition=NULL;
}

void CFaceIdentificationDlg::CurrentRecognitionVideoCensusData()//逐条增加当前识别信息
{
	QueryRecognitionFlag=false;
	int i;

	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetFont(&font);

	CString title;
	int pos = RecognitionFilePath.ReverseFind('\\'); 
	title = RecognitionFilePath.Right(RecognitionFilePath.GetLength()-pos-1);
	title=title+"  识别信息统计结果";
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowText(title);

	showCensusLive();
	

	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->SetWindowPos(0,0,ImageDisplayArea_y+10,ImageDisplayArea_w,20,SWP_HIDEWINDOW);
	m_list.SetWindowPos(0,0,ImageDisplayArea_y+220,ImageDisplayArea_w,280,SWP_HIDEWINDOW);
	((CStatic *)GetDlgItem(IDC_STATIC_TITLE))->ShowWindow(true);
	m_list.ShowWindow(true);

}
void CFaceIdentificationDlg::AddSingleData(CString path,CString headpath)//逐条增加当前识别信息
{
	CString headPic;//头像路径
	CString name,gender,tmp_DBFile;
	int age,id;

	int i;

	GetDBinfo(path,name,age,gender,id,tmp_DBFile);
	headPic=CurrentPath+"\\DB\\"+tmp_DBFile+"\\"+name+".bmp";

	m_info.push_back(path);
	m_info.push_back(headpath);

	HBITMAP hBitmap;
	CBitmap *pBitmap;
	pBitmap = new   CBitmap;
	hBitmap = (HBITMAP)LoadImage(AfxGetInstanceHandle(), headPic,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	pBitmap->Attach(hBitmap);
	int a = m_img_list.Add(pBitmap, RGB(0,0,0));

	m_list.InsertItem(a,"",a);
	m_list.SetItemText(a, 1, name);
	m_list.SetItemText(a, 2, gender);
	CString s;
	s.Format("%d",age);
	m_list.SetItemText(a, 3,s );
	s.Format("%d",id);
	m_list.SetItemText(a, 4, s);

	m_list.SetItemText(a, 5,"");//时间范围

	MyGetTimeByAnalysisTime(m_RecognitionAnalysisTime,s);
	m_list.SetItemText(a, 6, s);//识别时间


}
void CFaceIdentificationDlg::initCensus()
{
	int i;
	// 	m_list.SetItemState(0,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES |LVS_EX_FULLROWSELECT);//显示选中的一行

	CString title[4]=
	{
		"姓名",
		"性别",
		"年龄",
		"ID",
	};

	CHeaderCtrl* pHeaderCtrl = m_list.GetHeaderCtrl();
	int Column=pHeaderCtrl->GetItemCount();

	if (Column>1)
	{
		for   (int  j=0;j < Column;j++)   
		{   
			m_list.DeleteColumn(0);   
		}
	}

	Column=pHeaderCtrl->GetItemCount();

	m_list.InsertColumn(0,"头像", LVCFMT_CENTER,40);
	for (i=1;i<5;i++)
	{
		m_list.InsertColumn(i,title[i-1], LVCFMT_CENTER,70);
	}
	m_list.InsertColumn(5,"出现的时间范围", LVCFMT_CENTER,500);
	m_list.InsertColumn(6,"识别日期", LVCFMT_CENTER,150);
	int iIndex=0;
	iIndex =m_list.GetItemCount();


	for (int j=0;j<iIndex;j++)//删除所有行
	{
		m_list.DeleteItem(0);
	}

	int cnt = m_img_list.GetImageCount();
	for(int i = 0; i < cnt; i++)
	{
		m_img_list.Remove(0);
	}
}
void CFaceIdentificationDlg::showCensusLive()
{
	int i;
	initCensus();
	ClearCStringVector(m_info);
	int k=sigleData.size()/2;
	for (i=0;i<k;i++)
	{
		AddSingleData(sigleData[2*i],sigleData[2*i+1]);
	}
}

void CFaceIdentificationDlg::ShowDetectionSelectedData( int row)
{
	CString s;
	CString AnalysisTime;

	s=CurrentFilePath;
	CurrentFilePath=CurrentDetectionCensusFilePath;
	GetAnalysisTime(AnalysisTime,DetectionTxtPath);
	CurrentFilePath=s;

	int fileType=testOpenfile(CurrentDetectionCensusFilePath,true);

	int pos = CurrentDetectionCensusFilePath.ReverseFind('\\'); 
	showpictureTitle = CurrentDetectionCensusFilePath.Right(CurrentDetectionCensusFilePath.GetLength()-pos-1);

	if (fileType==1)//图片
	{
		myLoadPicture(CurrentDetectionCensusFilePath,CurrentDetectionCensusPic);
		if (!CurrentDetectionCensusPic)
		{
			return;
		}
		CString Path=CurrentPath+"\\Record\\"+AnalysisTime+"\\000000000.txt";
		FACE tmp;
		ReadFaceData(Path,tmp);

		myDrawrectangular( CurrentDetectionCensusPic,tmp.faces[row].xmin,tmp.faces[row].ymin,tmp.faces[row].xmax,tmp.faces[row].ymax,255,255,255);

	}
	else if (fileType==2)//视频
	{
		s=((CListCtrl*)GetDlgItem(IDC_LIST_DETE))->GetItemText(row, 1);//第几帧
		CvCapture* pCapture = cvCaptureFromFile(CurrentDetectionCensusFilePath);
		if( !pCapture)
		{
			return ;
		}
		int vframes = cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_COUNT);
		int num=atoi(s);
		if (num<0||num>=vframes)
		{
			return;
		}
		cvSetCaptureProperty(pCapture, CV_CAP_PROP_POS_FRAMES, num);
		IplImage* src=NULL;
		src = cvQueryFrame( pCapture );
		if (!src)
		{
			return;
		}
		if (CurrentDetectionCensusPic)
		{
			cvReleaseImage(&CurrentDetectionCensusPic);
		}

		int i,j;

		CurrentDetectionCensusPic=cvCreateImage(cvSize(src->width,src->height),8,src->nChannels);
		for(i =0; i <src->height; i++)
			for(j =0; j <src->width; j++)
			{
				SetPointColour(CurrentDetectionCensusPic,j,i,src,j,i);
			}

		cvReleaseCapture(&pCapture);

		DrawFace(num, CurrentDetectionCensusPic,AnalysisTime);

		showpictureTitle=showpictureTitle+"  -  第 "+s+" 帧";
	}
		
//	cvSaveImage("F:\\1.jpg",CurrentDetectionCensusPic);
	CShowPicture picture;
	showPictureFlag=true;
	picture.DoModal();
	showPictureFlag=false;
}
void CFaceIdentificationDlg::ShowWelcome()
{
	int i,j;
	int l,r,t,b,R, G, B;

	R=215;
	G=215;
	B=215;
	l=0;r=ImageDisplayArea_w-1;
	t=ImageDisplayArea_y+90;
	b=t+120;

	for(i = 65; i < m_imageBack->height; i++)
		for(j = 0; j < m_imageBack->width; j++)
		{
			m_imageBack->imageData[ i*m_imageBack->widthStep + j*m_imageBack->nChannels ] = 240;
			m_imageBack->imageData[ i*m_imageBack->widthStep + j*m_imageBack->nChannels+1 ] = 240;
			m_imageBack->imageData[ i*m_imageBack->widthStep + j*m_imageBack->nChannels+2 ] = 240;
		}

// 	for(i = t; i <b; i++)
// 	{
// 		if (i%3==0)
// 		{
// 			R++;
// 			G++;
// 			B++;
// 		}
// 		
// 		for(j = l; j <r; j++)
// 		{
// 			SetPointColour(m_imageBack,j ,i,R,G,B);
// 		}
// 	}
// 
// 	t=ImageDisplayArea_y+330;
// 	b=t+120;
// 
// 	for(i = t; i <b; i++)
// 	{
// 		if (i%3==0)
// 		{
// 			R--;
// 			G--;
// 			B--;
// 		}
// 		
// 		for(j = l; j <r; j++)
// 		{
// 			SetPointColour(m_imageBack,j ,i,R,G,B);
// 		}
// 	}
// 
// 	R=255;
// 	G=255;
// 	B=255;
// 	l=0;r=180;
// 	t=ImageDisplayArea_y+210;
// 	b=t+120;
// 
// 	for(i = t; i <b; i++)
// 	{
// 		for(j = l; j <r; j++)
// 		{
// 			SetPointColour(m_imageBack,j ,i,R,G,B);
// 		}
// 	}
// 
// 	l=780;r=ImageDisplayArea_w-1;
// 
// 	for(i = t; i <b; i++)
// 	{
// 		for(j = l; j <r; j++)
// 		{
// 			SetPointColour(m_imageBack,j ,i,R,G,B);
// 		}
// 	}
// 
// 	R=215;
// 	G=215;
// 	B=215;
// 	l=0;r=ImageDisplayArea_w-1;
// 	t=ImageDisplayArea_y;
// 	b=t+90;
// 
// 	for(i = t; i <b; i++)
// 	{
// 		for(j = l; j <r; j++)
// 		{
// 			SetPointColour(m_imageBack,j ,i,R,G,B);
// 		}
// 	}
// 
// 	t=ImageDisplayArea_y+450;
// 	b=m_client_h;
// 
// 	for(i = t; i <b; i++)
// 	{
// 		for(j = l; j <r; j++)
// 		{
// 			SetPointColour(m_imageBack,j ,i,R,G,B);
// 		}
// 	}

//	SetBackPic(CurrentPath+"\\resource\\welcome.jpg", 180, ImageDisplayArea_y+210, 600,120, 0,0, false);

	int sw,sh;
	int dw = m_imageBack->width - 22;
	int dh = 420;
	double dr = (double)dw / dh;

	IplImage *dsp;
	IplImage *image = cvLoadImage(CurrentPath+"\\resource\\det.jpg");
	double imgr = (double)image->width / image->height;

	if( dr >= imgr )
	{
		sh = dh;
		sw = imgr * sh;
	}
	else
	{
		sw = dw;
		sh = sw / imgr;
	}
	dsp = cvCreateImage(cvSize(sw, sh), 8, 3);
	cvResize(image, dsp);

	int lx = (m_dspbuf->width - 350)/2;
	//SetBackPic(CurrentPath+"\\resource\\lena.bmp", lx, ImageDisplayArea_y+40, 350,350, 0,0, false);
	SetBackPic(dsp, 11, ImageDisplayArea_y+30, sw, sh, 0,0,false);
	SetBackPic(CurrentPath+"\\resource\\welcome.jpg", m_imageBack->width-209, ImageDisplayArea_y+460, 198,40, 0,0, false);

// 	if( MenuNum == 1 )
// 	{
// 		CvFont font;//以下用文字标识说明
// 		double hScale=1.4, vScale=1.4;
// 		int lineWidth=2;
// 		cvInitFont(&font,CV_FONT_HERSHEY_PLAIN  |CV_FONT_ITALIC,hScale,vScale,1,lineWidth,8);
// 		cvPutText(m_imageBack, "Lena", cvPoint(lx+137,ImageDisplayArea_y+40+145), &font, CV_RGB(255,0,0));
// 	}

	cvReleaseImage(&image);
	cvReleaseImage(&dsp);
}