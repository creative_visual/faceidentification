#include "StdAfx.h"
#include <io.h>
#include "common.h"

void get_subdirs(string path, vector<string> &dirs)
{
	string dir = path+"\\";

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_SUBDIR)
		{
			dirs.push_back(/*dir + */file.name);
		}
	}
}

void get_subdirs2(string path, vector<string> &dirs)
{
	string dir = path+"\\";

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_SUBDIR)
		{
			dirs.push_back(dir + file.name);
		}
	}
}

void norm_matrix(float *data, int width, int height)
{
	float *tmp = new float[ width * height ];
	memcpy(tmp, data, width * height * sizeof(float));

	int i,j;
	for(j = 0; j < width; j++)
	{
		float sum = 0;
		for(i = 0; i < height; i++)
			sum += tmp[ i * width + j ] * tmp[ i * width + j ];

		float N = sqrt(sum);
		for(i = 0; i < height; i++)
			data[ i * width + j ] = data[ i * width + j ] / N;
	}

	delete[] tmp;
}

void get_path_files(string path, vector<string> &files)
{
	string dir = path+"\\";

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_ARCH)
		{
			files.push_back(file.name);
		}
	}
}

void get_path_files2(string path, vector<string> &files)
{
	string dir = path+"\\";

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_ARCH)
		{
			files.push_back(dir + file.name);
		}
	}
}

void get_image_pathes(string dir_path, vector<string> &image_pathes)
{
	string dir = dir_path+"\\";
	vector<string> files;

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = dir_path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_ARCH)
		{
			files.push_back(dir + file.name);
		}
	}

	for(int i = 0; i < files.size(); i++)
	{
		int l = files[i].rfind(".");
		if( l < 0 )
			continue;

		string format = files[i].substr( l, files[i].length() );
		if( format == ".jpg" || format == ".bmp" || format == ".jpeg" || format == ".png" )
			image_pathes.push_back(files[i]);
	}
}

float *get_img_vector(IplImage *image, int samplew, int sampleh)
{
	IplImage *gray = cvCreateImage(cvSize(image->width,image->height), 8, 1);
	IplImage *std_l = cvCreateImage(cvSize(samplew, sampleh), 8, 1);

	if( image->nChannels == 1 )
		memcpy(gray->imageData, image->imageData, image->widthStep * image->height);
	else
		cvCvtColor(image, gray, CV_BGR2GRAY);

	cvResize(gray, std_l);

	float *y = new float[ samplew * sampleh ];

	int i,j,idx = 0;
	for(i = 0; i < std_l->height; i++)
		for(j = 0; j < std_l->width; j++)
		{
			unsigned char g = std_l->imageData[ i * std_l->widthStep + j ];
			y[ idx ] = g;
			idx++;
		}

	cvReleaseImage(&std_l);
	cvReleaseImage(&gray);

	return y;
}

void draw_recog_info(IplImage *image, rect face, string info, CvFont font)
{
	CvPoint p1,p2,p3,p4;
	p1.x = face.xmin, p1.y = face.ymin;
	p2.x = face.xmax, p2.y = face.ymin;
	p3.x = face.xmax, p3.y = face.ymax; 
	p4.x = face.xmin, p4.y = face.ymax;

	cvLine(image, p1, p2, CV_RGB(255,255,0), 2);
	cvLine(image, p2, p3, CV_RGB(255,255,0), 2);
	cvLine(image, p3, p4, CV_RGB(255,255,0), 2);
	cvLine(image, p4, p1, CV_RGB(255,255,0), 2);

	cvPutText(image, info.c_str(), cvPoint(face.xmin, face.ymin - 5), &font, CV_RGB(255,255,0));
}

void cvRoundRectangle(IplImage *image, CvPoint lefttop, CvPoint rightbottom,int radius,  
	CvScalar color,int thickness=1, int line_type=8, int shift=0)  
{  
	int temp;  
	if(lefttop.x>rightbottom.x)  
	{  
		temp=lefttop.x;  
		lefttop.x=rightbottom.x;  
		rightbottom.x=temp;  
	}  
	if(lefttop.y>rightbottom.y)  
	{  
		temp=lefttop.y;  
		lefttop.y=rightbottom.y;  
		rightbottom.y=temp;  
	}  
	if(rightbottom.x-lefttop.x<2*radius || rightbottom.y-lefttop.y<2*radius)  
	{  
		radius=min((rightbottom.x-lefttop.x)/2,(rightbottom.y-lefttop.y)/2);  
	}  
	CvPoint center;  
	if(thickness>0)  
	{  
		center=cvPoint(rightbottom.x-radius,lefttop.y+radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,0,90,color,1,line_type,shift);  
		center=cvPoint(lefttop.x+radius,lefttop.y+radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,90,180,color,1,line_type,shift);  
		center=cvPoint(lefttop.x+radius,rightbottom.y-radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,180,270,color,1,line_type,shift);  
		center=cvPoint(rightbottom.x-radius,rightbottom.y-radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,270,360,color,1,line_type,shift);  
		cvLine(image,cvPoint(lefttop.x+radius,lefttop.y),cvPoint(rightbottom.x-radius,lefttop.y),color,1,line_type,shift);  
		cvLine(image,cvPoint(lefttop.x+radius,rightbottom.y),cvPoint(rightbottom.x-radius,rightbottom.y),color,1,line_type,shift);  
		cvLine(image,cvPoint(lefttop.x,lefttop.y+radius),cvPoint(lefttop.x,rightbottom.y-radius),color);  
		cvLine(image,cvPoint(rightbottom.x,lefttop.y+radius),cvPoint(rightbottom.x,rightbottom.y-radius),color,1,line_type,shift);  
	}  
	else   
	{  
		center=cvPoint(rightbottom.x-radius,lefttop.y+radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,0,90,color,-1,line_type,shift);  
		center=cvPoint(lefttop.x+radius,lefttop.y+radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,90,180,color,-1,line_type,shift);  
		center=cvPoint(lefttop.x+radius,rightbottom.y-radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,180,270,color,-1,line_type,shift);  
		center=cvPoint(rightbottom.x-radius,rightbottom.y-radius);  
		cvEllipse(image,center,cvSize(radius,radius),0,270,360,color,-1,line_type,shift);  
		cvRectangle(image,cvPoint(lefttop.x+radius,lefttop.y),cvPoint(rightbottom.x-radius,lefttop.y+radius),color,-1,line_type,shift);  
		cvRectangle(image,cvPoint(lefttop.x+radius,rightbottom.y-radius),cvPoint(rightbottom.x-radius,rightbottom.y),color,-1,line_type,shift);  
		cvRectangle(image,cvPoint(lefttop.x,lefttop.y+radius),cvPoint(lefttop.x+radius,rightbottom.y-radius),color,-1,line_type,shift);  
		cvRectangle(image,cvPoint(rightbottom.x-radius,lefttop.y+radius),cvPoint(rightbottom.x,rightbottom.y-radius),color,-1,line_type,shift);  
		cvRectangle(image,cvPoint(lefttop.x+radius,lefttop.y+radius),cvPoint(rightbottom.x-radius,rightbottom.y-radius),color,-1,line_type,shift);  
	}  
}