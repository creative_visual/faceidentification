#pragma once


// DetectionQuery 对话框
#include <iostream>
#include <fstream>
#include <shlwapi.h>

using namespace std;

struct DBINFO
{
	int id;
	CString name;
	int age;
	CString sex;
	string img_path;
	int battention;
};

class DetectionQuery : public CDialogEx
{
	DECLARE_DYNAMIC(DetectionQuery)

public:
	DetectionQuery(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~DetectionQuery();

// 对话框数据
	enum { IDD = IDD_DIALOG_DETECTIONQUERY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();

	vector<CString> paths;//存储路径以及编号
	vector<DBINFO> db_info;
	vector<CString> db_time;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnChoose();
	afx_msg void OnClose();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
