
#pragma once
#include "common.h"

class CMyComboBox
{
public:
	CMyComboBox(void);
	~CMyComboBox(void);

public:
//	void insert_string(string str);
	void init(rect &pos, string arrow_path, IplImage *parent);
	void on_mouse_move(int x, int y);
	void on_lbtn_down(int x, int y);
	void draw();
	void clear();
	void draw_main_rect();

private:
	rect m_dstr, m_drop, m_arr_rect;
	string m_arrow_path;
	vector<string> m_item;
	int m_drop_flag, m_arr_h, m_sel_idx, m_mv_idx;
	IplImage *m_arr_img, *m_parent;
	BYTE m_pr,m_pg,m_pb;
};

//#endif
