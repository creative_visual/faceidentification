// TestDB.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceIdentification.h"
#include "TestDB.h"
#include "afxdialogex.h"
#include "recog/ProcessRecog.h"


// CTestDB 对话框

extern vector<string> g_dirpathes;
string g_db_path, g_out_path;
vector<string> g_names, g_img_pathes;
extern HANDLE g_init_model_hdl;

int g_bwroking = 1;
CRITICAL_SECTION g_cs;

IMPLEMENT_DYNAMIC(CTestDB, CDialog)

CTestDB::CTestDB(CWnd* pParent /*=NULL*/)
	: CDialog(CTestDB::IDD, pParent)
{

}

CTestDB::~CTestDB()
{
}

void CTestDB::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTestDB, CDialog)
	ON_BN_CLICKED(IDOK, &CTestDB::OnBnClickedOk)
	ON_BN_CLICKED(IDC_DBDIR, &CTestDB::OnBnClickedDbdir)
	ON_BN_CLICKED(IDC_OUTDIR, &CTestDB::OnBnClickedOutdir)
	ON_BN_CLICKED(IDCANCEL, &CTestDB::OnBnClickedCancel)
END_MESSAGE_MAP()


// CTestDB 消息处理程序

void test_db_images(string db_dir, string out_dir)
{
	vector<string> src_names, dst_names;
	get_path_files(db_dir,src_names);

	string tmp_src_dir = db_dir + "\\", tmp_dst_dir = out_dir + "\\";
	for(int i = 0; i < src_names.size(); i++)
	{
		EnterCriticalSection(&g_cs);
		int bwroking = g_bwroking;
		LeaveCriticalSection(&g_cs);

		if( !bwroking )
			break;

		string read_path = tmp_src_dir + src_names[i];
		string write_path = tmp_dst_dir + src_names[i];

		IplImage *image = cvLoadImage(read_path.c_str());

		Mat_<double> shape(68, 2);
		if( detect_alignment(image, shape) )
		{
			for(int j = 0; j < 68; j++)
			{
				int x = shape.at<double>(j,0);
				int y = shape.at<double>(j,1);

				cvCircle(image, cvPoint(x,y), 1, cvScalar(0, 255, 0), 4, 6, 0);
			}

			cvSaveImage(write_path.c_str(), image);
		}

		cvReleaseImage(&image);

		theApp.pCTestDB->ShowRate(i,src_names.size());
	}
	theApp.pCTestDB->Finish();
}

DWORD WINAPI StartTestDB(LPVOID lpParamter)
{
	WaitForSingleObject(g_init_model_hdl, INFINITE);
	test_db_images(g_db_path, g_out_path);
	return 1;
}

void CTestDB::OnBnClickedOk()
{
	InitializeCriticalSection(&g_cs);
	CreateThread(NULL, 0, StartTestDB, NULL, 0, NULL);
//	CDialog::OnOK();
	((CStatic *)GetDlgItem(IDC_STATIC_RATE))->SetWindowText("完成进度 ：0.000 %");
	((CStatic *)GetDlgItem(IDC_STATIC_RATE))->ShowWindow(SW_SHOWNA);
//	((CStatic *)GetDlgItem(IDC_STATIC_RATE))->SetWindowPos(0,m_client_w-260,beginDect_y+5,260,15,SWP_SHOWWINDOW);
}

void CTestDB::OnBnClickedDbdir()
{
	BROWSEINFO bInfo;
	ZeroMemory(&bInfo, sizeof(bInfo));
	bInfo.hwndOwner =GetSafeHwnd();
	bInfo.lpszTitle = _T("请选择数据库目录: ");
	bInfo.ulFlags = BIF_RETURNONLYFSDIRS;//BIF_BROWSEINCLUDEFILES;

	LPITEMIDLIST lpDlist; //用来保存返回信息的IDList
	lpDlist = SHBrowseForFolder(&bInfo) ; //显示选择对话框
	if(lpDlist != NULL) //用户按了确定按钮
	{
		TCHAR chPath[MAX_PATH]; //用来存储路径的字符串
		SHGetPathFromIDList(lpDlist, chPath);//把项目标识列表转化成字符串

		g_db_path = chPath;
		GetDlgItem(IDC_DBPATH)->SetWindowText(chPath);
	}
}

void CTestDB::OnBnClickedOutdir()
{
	BROWSEINFO bInfo;
	ZeroMemory(&bInfo, sizeof(bInfo));
	bInfo.hwndOwner =GetSafeHwnd();
	bInfo.lpszTitle = _T("请选择数据库目录: ");
	bInfo.ulFlags = BIF_RETURNONLYFSDIRS;//BIF_BROWSEINCLUDEFILES;

	LPITEMIDLIST lpDlist; //用来保存返回信息的IDList
	lpDlist = SHBrowseForFolder(&bInfo) ; //显示选择对话框
	if(lpDlist != NULL) //用户按了确定按钮
	{
		TCHAR chPath[MAX_PATH]; //用来存储路径的字符串
		SHGetPathFromIDList(lpDlist, chPath);//把项目标识列表转化成字符串

		g_out_path = chPath;
		GetDlgItem(IDC_OUTPATH)->SetWindowText(chPath);
	}
}


void CTestDB::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	g_bwroking=0;

	CDialog::OnCancel();
}
BOOL CTestDB::OnInitDialog()
{

	theApp.pCTestDB=this;

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}
void CTestDB::ShowRate(int i,int sum)
{
	CString rate;
	double r=(double)(i+1)/sum;
	r=r*100;
	rate.Format("%f",r);
	rate=rate.Left(5);
	((CStatic *)GetDlgItem(IDC_STATIC_RATE))->SetWindowText("完成进度 ："+rate+" %");
}
void CTestDB::Finish()
{
	MessageBox("测试完成！");
	CDialog::OnOK();
}