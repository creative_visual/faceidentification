#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#define  MAX_ITERATION_COUNT  30

template<class T>
void Householders_Reduction_to_Bidiagonal_Form(T* A, int nrows,
			int ncols, T* U, T* V, T* diagonal, T* superdiagonal )
{
	int i,j,k,ip1;
	T s, s2, si, scale;
	T *pu, *pui, *pv, *pvi;
	T half_norm_squared;

	// Copy A to U

	memcpy(U,A, sizeof(T) * nrows * ncols);

	//

	diagonal[0] = 0.0;
	s = 0.0;
	scale = 0.0;
	for ( i = 0, pui = U, ip1 = 1; i < ncols; pui += ncols, i++, ip1++ ) {
		superdiagonal[i] = scale * s;
		//       
		//                  Perform Householder transform on columns.
		//
		//       Calculate the normed squared of the i-th column vector starting at 
		//       row i.
		//
		for (j = i, pu = pui, scale = 0.0; j < nrows; j++, pu += ncols)
			scale += fabs( *(pu + i) );

		if (scale > 0.0) {
			for (j = i, pu = pui, s2 = 0.0; j < nrows; j++, pu += ncols) {
				*(pu + i) /= scale;
				s2 += *(pu + i) * *(pu + i);
			}
			//
			//    
			//       Chose sign of s which maximizes the norm
			//  
			s = ( *(pui + i) < 0.0 ) ? sqrt(s2) : -sqrt(s2);
			//
			//       Calculate -2/u'u
			//
			half_norm_squared = *(pui + i) * s - s2;
			//
			//       Transform remaining columns by the Householder transform.
			//
			*(pui + i) -= s;

			for (j = ip1; j < ncols; j++) {
				for (k = i, si = 0.0, pu = pui; k < nrows; k++, pu += ncols)
					si += *(pu + i) * *(pu + j);
				si /= half_norm_squared;
				for (k = i, pu = pui; k < nrows; k++, pu += ncols) {
					*(pu + j) += si * *(pu + i);
				}
			}
		}
		for (j = i, pu = pui; j < nrows; j++, pu += ncols) *(pu + i) *= scale;
		diagonal[i] = s * scale;
		//       
		//                  Perform Householder transform on rows.
		//
		//       Calculate the normed squared of the i-th row vector starting at 
		//       column i.
		//
		s = 0.0;
		scale = 0.0;
		if (i >= nrows || i == (ncols - 1) ) continue;
		for (j = ip1; j < ncols; j++) scale += fabs ( *(pui + j) );
		if ( scale > 0.0 ) {
			for (j = ip1, s2 = 0.0; j < ncols; j++) {
				*(pui + j) /= scale;
				s2 += *(pui + j) * *(pui + j);
			}
			s = ( *(pui + ip1) < 0.0 ) ? sqrt(s2) : -sqrt(s2);
			//
			//       Calculate -2/u'u
			//
			half_norm_squared = *(pui + ip1) * s - s2;
			//
			//       Transform the rows by the Householder transform.
			//
			*(pui + ip1) -= s;
			for (k = ip1; k < ncols; k++)
				superdiagonal[k] = *(pui + k) / half_norm_squared;
			if ( i < (nrows - 1) ) {
				for (j = ip1, pu = pui + ncols; j < nrows; j++, pu += ncols) {
					for (k = ip1, si = 0.0; k < ncols; k++) 
						si += *(pui + k) * *(pu + k);
					for (k = ip1; k < ncols; k++) { 
						*(pu + k) += si * superdiagonal[k];
					}
				}
			}
			for (k = ip1; k < ncols; k++) *(pui + k) *= scale;
		}
	}

	// Update V
	pui = U + ncols * (ncols - 2);
	pvi = V + ncols * (ncols - 1);
	*(pvi + ncols - 1) = 1.0;
	s = superdiagonal[ncols - 1];
	pvi -= ncols;
	for (i = ncols - 2, ip1 = ncols - 1; i >= 0; i--, pui -= ncols,
		pvi -= ncols, ip1-- ) {
			if ( s != 0.0 ) {
				pv = pvi + ncols;
				for (j = ip1; j < ncols; j++, pv += ncols)
					*(pv + i) = ( *(pui + j) / *(pui + ip1) ) / s;
				for (j = ip1; j < ncols; j++) { 
					si = 0.0;
					for (k = ip1, pv = pvi + ncols; k < ncols; k++, pv += ncols)
						si += *(pui + k) * *(pv + j);
					for (k = ip1, pv = pvi + ncols; k < ncols; k++, pv += ncols)
						*(pv + j) += si * *(pv + i);                  
				}
			}
			pv = pvi + ncols;
			for ( j = ip1; j < ncols; j++, pv += ncols ) {
				*(pvi + j) = 0.0;
				*(pv + i) = 0.0;
			}
			*(pvi + i) = 1.0;
			s = superdiagonal[i];
	}

	// Update U

	pui = U + ncols * (ncols - 1);
	for (i = ncols - 1, ip1 = ncols; i >= 0; ip1 = i, i--, pui -= ncols ) {
		s = diagonal[i];
		for ( j = ip1; j < ncols; j++) *(pui + j) = 0.0;
		if ( s != 0.0 ) {
			for (j = ip1; j < ncols; j++) { 
				si = 0.0;
				pu = pui + ncols;
				for (k = ip1; k < nrows; k++, pu += ncols)
					si += *(pu + i) * *(pu + j);
				si = (si / *(pui + i) ) / s;
				for (k = i, pu = pui; k < nrows; k++, pu += ncols)
					*(pu + j) += si * *(pu + i);                  
			}
			for (j = i, pu = pui; j < nrows; j++, pu += ncols){
				*(pu + i) /= s;
			}
		}
		else 
			for (j = i, pu = pui; j < nrows; j++, pu += ncols) *(pu + i) = 0.0;
		*(pui + i) += 1.0;
	}
}

template<class T>
int Givens_Reduction_to_Diagonal_Form( int nrows, int ncols,
	T* U, T* V, T* diagonal, T* superdiagonal )
{

	T epsilon;
	T c, s;
	T f,g,h;
	T x,y,z;
	T *pu, *pv;
	int i,j,k,m;
	int rotation_test;
	int iteration_count;

	for (i = 0, x = 0.0; i < ncols; i++) {
		y = fabs(diagonal[i]) + fabs(superdiagonal[i]);
		if ( x < y ) x = y;
	}
	epsilon = x * DBL_EPSILON;
	for (k = ncols - 1; k >= 0; k--) {
		iteration_count = 0;
		while(1) {
			rotation_test = 1;
			for (m = k; m >= 0; m--) { 
				if (fabs(superdiagonal[m]) <= epsilon) {rotation_test = 0; break;}
				if (fabs(diagonal[m-1]) <= epsilon) break;
			}
			if (rotation_test) {
				c = 0.0;
				s = 1.0;
				for (i = m; i <= k; i++) {  
					f = s * superdiagonal[i];
					superdiagonal[i] *= c;
					if (fabs(f) <= epsilon) break;
					g = diagonal[i];
					h = sqrt(f*f + g*g);
					diagonal[i] = h;
					c = g / h;
					s = -f / h; 
					for (j = 0, pu = U; j < nrows; j++, pu += ncols) { 
						y = *(pu + m - 1);
						z = *(pu + i);
						*(pu + m - 1 ) = y * c + z * s;
						*(pu + i) = -y * s + z * c;
					}
				}
			}
			z = diagonal[k];
			if (m == k ) {
				if ( z < 0.0 ) {
					diagonal[k] = -z;
					for ( j = 0, pv = V; j < ncols; j++, pv += ncols) 
						*(pv + k) = - *(pv + k);
				}
				break;
			}
			else {
				if ( iteration_count >= MAX_ITERATION_COUNT ) return -1;
				iteration_count++;
				x = diagonal[m];
				y = diagonal[k-1];
				g = superdiagonal[k-1];
				h = superdiagonal[k];
				f = ( (y - z) * ( y + z ) + (g - h) * (g + h) )/(2.0 * h * y);
				g = sqrt( f * f + 1.0 );
				if ( f < 0.0 ) g = -g;
				f = ( (x - z) * (x + z) + h * (y / (f + g) - h) ) / x;
				// Next QR Transformtion
				c = 1.0;
				s = 1.0;
				for (i = m + 1; i <= k; i++) {
					g = superdiagonal[i];
					y = diagonal[i];
					h = s * g;
					g *= c;
					z = sqrt( f * f + h * h );
					superdiagonal[i-1] = z;
					c = f / z;
					s = h / z;
					f =  x * c + g * s;
					g = -x * s + g * c;
					h = y * s;
					y *= c;
					for (j = 0, pv = V; j < ncols; j++, pv += ncols) {
						x = *(pv + i - 1);
						z = *(pv + i);
						*(pv + i - 1) = x * c + z * s;
						*(pv + i) = -x * s + z * c;
					}
					z = sqrt( f * f + h * h );
					diagonal[i - 1] = z;
					if (z != 0.0) {
						c = f / z;
						s = h / z;
					} 
					f = c * g + s * y;
					x = -s * g + c * y;
					for (j = 0, pu = U; j < nrows; j++, pu += ncols) {
						y = *(pu + i - 1);
						z = *(pu + i);
						*(pu + i - 1) = c * y + s * z;
						*(pu + i) = -s * y + c * z;
					}
				}
				superdiagonal[m] = 0.0;
				superdiagonal[k] = f;
				diagonal[k] = x;
			}
		} 
	}
	return 0;
}

template<class T>
void Sort_by_Decreasing_Singular_Values(int nrows, int ncols,
	T* singular_values, T* U, T* V)
{
	int i,j,max_index;
	T temp;
	T *p1, *p2;

	for (i = 0; i < ncols - 1; i++) {
		max_index = i;
		for (j = i + 1; j < ncols; j++)
			if (singular_values[j] > singular_values[max_index] ) 
				max_index = j;
		if (max_index == i) continue;
		temp = singular_values[i];
		singular_values[i] = singular_values[max_index];
		singular_values[max_index] = temp;
		p1 = U + max_index;
		p2 = U + i;
		for (j = 0; j < nrows; j++, p1 += ncols, p2 += ncols) {
			temp = *p1;
			*p1 = *p2;
			*p2 = temp;
		} 
		p1 = V + max_index;
		p2 = V + i;
		for (j = 0; j < ncols; j++, p1 += ncols, p2 += ncols) {
			temp = *p1;
			*p1 = *p2;
			*p2 = temp;
		}
	} 
}

template<class T>
int Singular_Value_Decomposition(T* A, int nrows, int ncols, T* U, 
	T* singular_values, T* V, T* dummy_array)
{
	Householders_Reduction_to_Bidiagonal_Form( A, nrows, ncols, U, V,
		singular_values, dummy_array);

	if (Givens_Reduction_to_Diagonal_Form( nrows, ncols, U, V,
		singular_values, dummy_array ) < 0) return -1;

	Sort_by_Decreasing_Singular_Values(nrows, ncols, singular_values, U, V);

	return 0;
}

//template<class T>
void Singular_Value_Decomposition_Inverse(double* U, double* D, double* V,  
	double tolerance, int nrows, int ncols, double *Astar);

void Singular_Value_Decomposition_Inverse(float* U, float* D, float* V,  
	float tolerance, int nrows, int ncols, float *Astar);

// template<class T>
// void Singular_Value_Decomposition_Inverse(T* U, T* D, T* V,  
// 	T tolerance, int nrows, int ncols, T *Astar) 
// {
// 	int i,j,k;
// 	T *pu, *pv, *pa;
// 	T dum;
// 
// 	dum = DBL_EPSILON * D[0] * (T) ncols;
// 	if (tolerance < dum) tolerance = dum;
// 	for ( i = 0, pv = V, pa = Astar; i < ncols; i++, pv += ncols) 
// 		for ( j = 0, pu = U; j < nrows; j++, pa++) 
// 			for (k = 0, *pa = 0.0; k < ncols; k++, pu++)
// 				if (D[k] > tolerance) *pa += *(pv + k) * *pu / D[k];
// }

// template<class T>
// void Matrix_Inverse(T *Matrix_B,T *inv_Matrix_B,int nrows, int ncols)
// {
// 	T *dummy_array = (T*) calloc(ncols, sizeof(T));
// 	T *U = (T *)calloc(nrows*ncols,sizeof(T));
// 	T *S = (T *)calloc(ncols,sizeof(T));
// 	T *V = (T *)calloc(ncols*ncols,sizeof(T));
// 
// 	int err;
// 	err = Singular_Value_Decomposition(Matrix_B, nrows, ncols, U, S, V, dummy_array);
// 
// 	Singular_Value_Decomposition_Inverse(U,S,V,0.00000000001,nrows,ncols,inv_Matrix_B);
// 
// 	free(dummy_array);
// 	free(U);
// 	free(S);
// 	free(V);
// 
// 	return;
// }

void Matrix_Inverse(float *Matrix_B, float *inv_Matrix_B,int nrows, int ncols);

void Matrix_Inverse(double *Matrix_B, double *inv_Matrix_B,int nrows, int ncols);

void Matrix_Inverse2(float *Matrix_B,float *inv_Matrix_B,int nrows, int ncols);

template<class T>
void Matrix_Transpose(T *M, T *M_T,int nrows,int ncols)
{
	int i,j;
	for (i = 0; i < nrows; i ++){
		for (j = 0; j < ncols; j ++){
			//			M_T[i*ncols+j] = M[j*ncols+i];
			M_T[j*nrows+i] = M[i*ncols+j];
		}
	}
}

template<class T>
void Matrix_Multiply(T *M1, int M1_height, int M1_width,
	T *M2, int M2_height, int M2_width,
	T *output_M)
{
	int i,j,k;
	T sum;

	for (i = 0; i < M1_height; i ++){
		for (j = 0; j < M2_width; j ++){
			sum = 0;
			for (k = 0; k < M1_width; k ++){
				sum = sum + M1[i*M1_width+k] * M2[k*M2_width+j];
			}
			output_M[i*M2_width+j] = sum;
		}
	}
}

template<class T>
void Matrix_Multiply(T *M, int height, int width, T k)
{
	int i,j;
	for(i = 0; i < height; i++)
		for(j = 0; j < width; j++)
		{
			M[i * width + j] = M[ i * width + j ] * k;
		}
}

template<class T>
void read_matrix(string path, T *data, int width, int height)
{
	int i,j;
	ifstream file(path.c_str());

	for(i = 0; i < height; i++)
		for(j = 0; j < width; j++)
		{
			file >> data[ i * width + j ];
		}

	file.close();
}

template<class T>
void save_matrix(string path, T *data, int w, int h)
{
	ofstream f(path.c_str());

	for(int i = 0; i < h; i++)
	{
		for(int j = 0; j < w; j++)
			f<<fixed <<setprecision(4)<<data[ i * w + j ]<<"    ";

		f<< endl;
	}

	f.close();
}

template<class T>
void save_matrix(string path, T **data, int w, int h)
{
	ofstream f(path.c_str());

	for(int i = 0; i < h; i++)
	{
		for(int j = 0; j < w; j++)
			f<<fixed <<setprecision(4)<<data[i][j]<<"    ";

		f<< endl;
	}

	f.close();
}

#endif