#include "StdAfx.h"
#include "Matrix.h"

void Singular_Value_Decomposition_Inverse(double* U, double* D, double* V,  
	double tolerance, int nrows, int ncols, double *Astar) 
{
	int i,j,k;
	double *pu, *pv, *pa;
	double dum;

	dum = DBL_EPSILON * D[0] * (double) ncols;
	if (tolerance < dum) tolerance = dum;
	for ( i = 0, pv = V, pa = Astar; i < ncols; i++, pv += ncols) 
		for ( j = 0, pu = U; j < nrows; j++, pa++) 
			for (k = 0, *pa = 0.0; k < ncols; k++, pu++)
				if (D[k] > tolerance) *pa += *(pv + k) * *pu / D[k];
}

void Singular_Value_Decomposition_Inverse(float* U, float* D, float* V,  
	float tolerance, int nrows, int ncols, float *Astar) 
{
	int i,j,k;
	float *pu, *pv, *pa;
	float dum;

	dum = DBL_EPSILON * D[0] * (float) ncols;
	if (tolerance < dum) tolerance = dum;
	for ( i = 0, pv = V, pa = Astar; i < ncols; i++, pv += ncols) 
		for ( j = 0, pu = U; j < nrows; j++, pa++) 
			for (k = 0, *pa = 0.0; k < ncols; k++, pu++)
				if (D[k] > tolerance) *pa += *(pv + k) * *pu / D[k];
}

void Matrix_Inverse(float *Matrix_B,float *inv_Matrix_B,int nrows, int ncols)
{
	float *dummy_array = (float*) calloc(ncols, sizeof(float));
	float *U = (float *)calloc(nrows*ncols,sizeof(float));
	float *S = (float *)calloc(ncols,sizeof(float));
	float *V = (float *)calloc(ncols*ncols,sizeof(float));

	int err;
	err = Singular_Value_Decomposition(Matrix_B, nrows, ncols, U, S, V, dummy_array);

	Singular_Value_Decomposition_Inverse(U,S,V,0.00000000001,nrows,ncols,inv_Matrix_B);

	free(dummy_array);
	free(U);
	free(S);
	free(V);

	return;
}

void Matrix_Inverse(double *Matrix_B,double *inv_Matrix_B,int nrows, int ncols)
{
	double *dummy_array = (double*) calloc(ncols, sizeof(double));
	double *U = (double *)calloc(nrows*ncols,sizeof(double));
	double *S = (double *)calloc(ncols,sizeof(double));
	double *V = (double *)calloc(ncols*ncols,sizeof(double));

	int err;
	err = Singular_Value_Decomposition(Matrix_B, nrows, ncols, U, S, V, dummy_array);

	Singular_Value_Decomposition_Inverse(U,S,V,0.00000000001,nrows,ncols,inv_Matrix_B);

	free(dummy_array);
	free(U);
	free(S);
	free(V);

	return;
}

void Matrix_Inverse2(float *Matrix_B,float *inv_Matrix_B,int nrows, int ncols)
{
	CvMat *MB = cvCreateMat(nrows,ncols,CV_64FC1);

	for (int ind = 0; ind < nrows * ncols; ind ++)
		MB->data.db[ind] = (double)(Matrix_B[ind]);

	CvMat *Inverse_MB = cvCreateMat(ncols,nrows,CV_64FC1);
	if (nrows == ncols)
		cvInvert(MB, Inverse_MB);
	else
		cvInvert(MB, Inverse_MB,CV_SVD);
	for (int ind = 0; ind < nrows * ncols; ind ++)
		inv_Matrix_B[ind] = (float)(Inverse_MB->data.db[ind]);
	cvReleaseMat(&MB);
	cvReleaseMat(&Inverse_MB);
}

void Singular_Value_Decomposition_Solve(float* U, float* D, float* V,  
	float tolerance, int nrows, int ncols, float *B, float* x) 
{
	int i,j,k;
	float *pu, *pv;
	float dum;

	dum = DBL_EPSILON * D[0] * (float) ncols;
	if (tolerance < dum) tolerance = dum;

	for ( i = 0, pv = V; i < ncols; i++, pv += ncols) {
		x[i] = 0.0;
		for (j = 0; j < ncols; j++)
			if (D[j] > tolerance ) {
				for (k = 0, dum = 0.0, pu = U; k < nrows; k++, pu += ncols)
					dum += *(pu + j) * B[k];
				x[i] += dum * *(pv + j) / D[j];
			}
	} 
}

int Matrix_inverse(float *M,float *M_inverse,int nrows,int ncols)
{
	int i;
	float *S;
	float *V;
	float *D;
	float *dummy_array;
	int err;

	S = (float *)malloc(nrows*ncols*sizeof(float));
	V = (float *)malloc(nrows*sizeof(float));
	D = (float *)malloc(nrows*ncols*sizeof(float));
	dummy_array = (float*) malloc(ncols * sizeof(float));

	err = Singular_Value_Decomposition(M, nrows, ncols, S,V, D,dummy_array);   
	if (err < 0){
		printf("SVD Error!\n");
		return -1;
	}

	float cum = 1;
	for (int ind = 0; ind < nrows; ind ++){
		//printf("V %f ",V[ind]);
		cum = cum * V[ind];
	}

	//printf("cum val = %g.\n",cum);

	float t = 0.0;
	Singular_Value_Decomposition_Inverse(S, V, D,/*0.0*/t, nrows, ncols, M_inverse) ;


	free(S);
	free(V);
	free(D);
	free(dummy_array);

	if (cum <  DBL_EPSILON)
		return -1;
	else
		return 0;
}