// DetectionQuery.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceIdentification.h"
#include "DetectionQuery.h"
#include "afxdialogex.h"


// DetectionQuery 对话框

IMPLEMENT_DYNAMIC(DetectionQuery, CDialogEx)

DetectionQuery::DetectionQuery(CWnd* pParent /*=NULL*/)
	: CDialogEx(DetectionQuery::IDD, pParent)
{

}

DetectionQuery::~DetectionQuery()
{
}

void DetectionQuery::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DetectionQuery, CDialogEx)
	ON_LBN_SELCHANGE(IDC_LIST_DQ, &DetectionQuery::OnChoose)
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// DetectionQuery 消息处理程序

BOOL DetectionQuery::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CRect clrect;
	GetClientRect(&clrect);

	int client_w=clrect.Width();
	int client_h=clrect.Height();

	GetDlgItem(IDC_LIST_DQ)->SetWindowPos(0,0,35,client_w,client_h-35,SWP_SHOWWINDOW);
	GetDlgItem(IDC_STATIC_TIP)->SetWindowPos(0,0,10,client_w,20,SWP_SHOWWINDOW);

	CString tip;

	if (theApp.pFaceIdentificationDlg)
	{
		if (theApp.pFaceIdentificationDlg->QueryFlag==false)
		{
			theApp.pFaceIdentificationDlg->ReadAnalysisTime(paths,theApp.pFaceIdentificationDlg->DetectionTxtPath);

			int k=(int)paths.size()/2;
			int i=0;
			for (i=0;i<k;i++)
			{
				((CListBox* )GetDlgItem(IDC_LIST_DQ))->InsertString(2*i,paths[2*i]);
				((CListBox* )GetDlgItem(IDC_LIST_DQ))->InsertString(2*i+1,"");
			}

			tip="以下是所有的检测历史记录，请点击选择一条记录，查看该文件的检测统计结果。";
		}
		else if (theApp.pFaceIdentificationDlg->QueryFlag==true)
		{
			this->SetWindowTextA("查询历史识别");
			theApp.pFaceIdentificationDlg->ReadDBinfo(db_info,db_time);

			int k=(int)db_time.size();
			int i=0;
			CString tmp;
			CString tmp_s;
			for (i=0;i<k;i++)
			{
				tmp_s.Format("%02d",(i+1));
				tmp_s="数据库人员 "+tmp_s;
				tmp.Format("%d",db_info[i].id);
				tmp=tmp_s+"          ID : "+tmp+"         姓名：  "+db_info[i].name+"          性别："+db_info[i].sex;
				tmp_s.Format("%d",db_info[i].age);
				tmp=tmp+"          年龄 : "+tmp_s;
				((CListBox* )GetDlgItem(IDC_LIST_DQ))->InsertString(2*i,tmp);
				((CListBox* )GetDlgItem(IDC_LIST_DQ))->InsertString(2*i+1,"");
			}

			tip="以下是所有的数据库人员的识别记录，请点击选择一条记录，查看该人员的识别统计结果。";
		}
		
		GetDlgItem(IDC_STATIC_TIP)->SetWindowText(tip);
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void DetectionQuery::OnChoose()
{
	// TODO: 在此添加控件通知处理程序代码

	int i=((CListBox* )GetDlgItem(IDC_LIST_DQ))->GetCurSel();
	int k;

	if (theApp.pFaceIdentificationDlg->QueryFlag==false)
	{
		k=(int)paths.size();
		if (i>=0&&i<k)
		{
			if (i%2==0)
			{
				theApp.pFaceIdentificationDlg->ShowCensusData(paths[i],paths[i+1]);
			}

		}
	}
	else if (theApp.pFaceIdentificationDlg->QueryFlag==true)
	{
		k=(int)db_time.size();
		if (i>=0&&i<k*2)
		{
			if (i%2==0)
			{
				theApp.pFaceIdentificationDlg->QueryRecognitionCensusData(db_info[i/2],db_time[i/2]);
			}

		}
	}

	
}


void DetectionQuery::OnClose()
{
	if (!paths.empty())
	{
		paths.clear();
		vector<CString>().swap(paths);
	}

	CDialogEx::OnClose();
}


HBRUSH DetectionQuery::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性
	if (pWnd->GetDlgCtrlID() ==IDC_STATIC_TIP)//控件属性设置  Simple为 true
	{
		pDC->SetTextColor(RGB(255,255,255));
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(0,150,255));
	}
	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}
