#include "common.h"
#include "recog/SparseCoding.h"
#include "Matrix.h"
#include <iostream>
#include <fstream>

using namespace std;

typedef struct myData 
{
	vector<rect> faces;
} FACE;

typedef struct myRData 
{
	FACE face;
	vector<CString> name;
} ALLFACE;

struct recoged_sec
{
	int begin;
	int end;
};

void detect_gray( Mat gray, CascadeClassifier& cascade, vector<rect> &faces_target );
// float *get_dict_matrix(vector<string> &dir_path, float *&AT, float *&ATA, int samplew, int sampleh, 
//   	int &matrix_length, int &nsamples, vector<int> &class_flag, vector<string> &filename);
float *get_dict_matrix(vector<string> &img_path, float *&AT, float *&ATA, int samplew, int sampleh, 
 	 int &matrix_length, vector<int> &class_flag);
void copy_roi_pxl(IplImage *dst, IplImage *src, int x, int y);
void myDetectionForSingle(IplImage* image,CascadeClassifier& cascade,FACE &face);//检测单个图片,获取人脸坐标
vector<int> myRecognitionForSingle(IplImage* image,CascadeClassifier& cascade,ALLFACE &face);//识别单个图片,获取人脸坐标
vector<int> myRecognitionLBP(IplImage* image,CascadeClassifier& cascade,ALLFACE &face);//检测单个图片,获取人脸坐标