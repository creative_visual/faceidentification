#pragma once
#include "afxwin.h"


// CShowPicture 对话框

class CShowPicture : public CDialogEx
{
	DECLARE_DYNAMIC(CShowPicture)

public:
	CShowPicture(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CShowPicture();

	IplImage* m_dspbuf;
	IplImage* m_image;
	int m_hpos,m_vpos;

// 对话框数据
	enum { IDD = IDD_DIALOG_SHOWPIC };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	void GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	CScrollBar m_SBar;
	CScrollBar m_SvBar;
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};
