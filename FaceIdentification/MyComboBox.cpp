#include "StdAfx.h"
#include "MyComboBox.h"


CMyComboBox::CMyComboBox(void)
{	
}

CMyComboBox::~CMyComboBox(void)
{
}

void CMyComboBox::init(rect &pos, string arrow_path, IplImage *parent)
{
	m_item.push_back("SparseCoding");
	m_item.push_back("HD_LBP");

	m_dstr = pos;
	m_drop.xmin = pos.xmin, m_drop.xmax = pos.xmax;
	m_drop.ymin = pos.ymax + 2, m_drop.ymax = m_drop.ymin + (pos.ymax - pos.ymin)*2;
	m_arr_h = pos.ymax - pos.ymin;

	int seedx = (pos.xmin + pos.xmax)/2.0, seedy = (pos.ymin + pos.ymax)/2.0;
	m_pr = 240;//parent->imageData[ seedy * parent->widthStep + seedx*parent->nChannels + 2 ];
	m_pg = 240;//parent->imageData[ seedy * parent->widthStep + seedx*parent->nChannels + 1 ];
	m_pb = 240;//parent->imageData[ seedy * parent->widthStep + seedx*parent->nChannels + 0 ];

	m_arrow_path = arrow_path;
	m_arr_img = cvCreateImage(cvSize(m_arr_h, m_arr_h), 8 , 3);//
	IplImage *tmp = cvLoadImage(m_arrow_path.c_str());
	cvResize(tmp, m_arr_img);
	cvReleaseImage(&tmp);

	m_parent = parent;
	m_drop_flag = 0;
	m_sel_idx = 0, m_mv_idx = 0;

	//draw();
}

void CMyComboBox::clear()
{
	for(int i = m_dstr.ymin; i <= m_dstr.ymax; i++)
		for(int j = m_dstr.xmin; j <= m_dstr.xmax; j++)
		{
			m_parent->imageData[ i * m_parent->widthStep + j * m_parent->nChannels + 2 ] = m_pr;
			m_parent->imageData[ i * m_parent->widthStep + j * m_parent->nChannels + 1 ] = m_pg;
			m_parent->imageData[ i * m_parent->widthStep + j * m_parent->nChannels + 0 ] = m_pb;
		}

//	if( m_drop_flag )
	{
		for(int i = m_drop.ymin; i <= m_drop.ymax; i++)
			for(int j = m_drop.xmin; j <= m_drop.xmax; j++)
			{
				m_parent->imageData[ i * m_parent->widthStep + j * m_parent->nChannels + 2 ] = m_pr;
				m_parent->imageData[ i * m_parent->widthStep + j * m_parent->nChannels + 1 ] = m_pg;
				m_parent->imageData[ i * m_parent->widthStep + j * m_parent->nChannels + 0 ] = m_pb;
			}
	}
}

void CMyComboBox::draw_main_rect()
{
	rect str = m_dstr;
	str.xmax -= m_arr_h;

	cvRectangle(m_parent, cvPoint(m_dstr.xmin, m_dstr.ymin), cvPoint(m_dstr.xmax-m_arr_h, m_dstr.ymax), CV_RGB(0,20,100));

	CvFont font;
	double hScale=0.4, vScale=0.4;
	int lineWidth=1;
	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);
	cvPutText(m_parent, m_item[m_sel_idx].c_str(), cvPoint(m_dstr.xmin+5, m_dstr.ymin+15), &font, CV_RGB(0,0,255));

	// arrow
	int lx = m_dstr.xmax-m_arr_h+1;
	for(int i = m_dstr.ymin; i <= m_dstr.ymax; i++)
		for(int j = lx; j <= m_dstr.xmax; j++)
		{
			m_parent->imageData[ i * m_parent->widthStep + j*m_parent->nChannels + 2 ] = m_arr_img->imageData[ (i-m_dstr.ymin)*m_arr_img->widthStep + (j-lx)*m_arr_img->nChannels + 2 ];
			m_parent->imageData[ i * m_parent->widthStep + j*m_parent->nChannels + 1 ] = m_arr_img->imageData[ (i-m_dstr.ymin)*m_arr_img->widthStep + (j-lx)*m_arr_img->nChannels + 1 ];
			m_parent->imageData[ i * m_parent->widthStep + j*m_parent->nChannels + 0 ] = m_arr_img->imageData[ (i-m_dstr.ymin)*m_arr_img->widthStep + (j-lx)*m_arr_img->nChannels + 0 ];
		}
}

void CMyComboBox::draw()
{
	clear();
	draw_main_rect();

	if( m_drop_flag )
	{
		cvRectangle(m_parent, cvPoint(m_drop.xmin, m_drop.ymin), cvPoint(m_drop.xmax, m_drop.ymax), CV_RGB(0,20,200));

		CvFont font;
		double hScale=0.4, vScale=0.4;
		int lineWidth=1;
		cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);

		for(int i = 0; i < m_item.size(); i++)
			cvPutText(m_parent, m_item[i].c_str(), cvPoint(m_drop.xmin+5, m_drop.ymin+m_arr_h*i+15), &font, CV_RGB(0,0,255));
	}
}

void CMyComboBox::on_lbtn_down(int x, int y)
{
	if( x >= m_dstr.xmax - m_arr_h && x <= m_dstr.xmax && y >= m_dstr.ymin && y <= m_dstr.ymax )
	{
		m_drop_flag = !m_drop_flag;
		draw();
	}
	else if( m_drop_flag )
	{
		m_drop_flag = !m_drop_flag;

		if( x >= m_drop.xmin && x <= m_drop.xmax && y >= m_drop.ymin && y <= m_drop.ymax )
		{
			int mindy = (m_drop.ymin + m_drop.ymax)/2.0;
			if( y < mindy )
				m_sel_idx = 0;
			else
				m_sel_idx = 1;
		}

		draw();
	}
}
