
// FaceIdentificationDlg.h : 头文件
//

#pragma once
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include "afxcmn.h"

#include <direct.h>
#include "shlwapi.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>
#include <iostream>
#include <time.h>

#include "windows.h"
//#include "MyComboBox.h"

#include <iostream>
#include <fstream>
#include <shlwapi.h>

//#include "common.h"
#include <opencv2/opencv.hpp>

#include "DetectionQuery.h"
#include "MyAlgorithm.h"
#include "MyComboBox.h"
#include "ShowPicture.h"

using namespace std;

enum db_status
{
	changed,
	unchanged
};

enum algorithm
{
	HD_LBP,
	sparse_coding
};

enum black_list_type
{
	pic = 1,
	dir,
	txt
};

// CFaceIdentificationDlg 对话框
class CFaceIdentificationDlg : public CDialogEx
{
// 构造
public:
	CFaceIdentificationDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_FACEIDENTIFICATION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


	public:
		CString DetectionFilePath,DetectionFileName;//检测的文件名
		CString RecognitionFilePath,RecognitionFileName;//检测的文件名
		CString CurrentFilePath,CurrentFileName;//检测的文件名

		IplImage* LoadedPicture;
		vector<FACE> m_faces;
		FACE pictureCensusData;
		CString CurrentPath;
		int MenuNum;
		bool QueryFlag;
		CString DetectionTxtPath;
		IplImage* CurrentDetectionCensusPic;//当前点击显示的图片
		CString showpictureTitle;

		void Playing();
		void ShowImageInCenter(IplImage* dst, IplImage* src,bool f=true);
		void showCurrentPicture();
		void showVideo();
		void Detection();
		void ReadAnalysisTime(vector<CString> &paths,CString txtPath);
		void ShowCensusData(CString path,CString AnalysisTime);
		void insertData();
		void Recognition();
		void ReadDBinfo(vector<DBINFO>& info,vector<CString>&db_time);
		void RecognitionVideoCensusData(DBINFO db_info,CString AnalysisTime);
		void QueryRecognitionCensusData(DBINFO db_info,CString AnalysisTime);
		void DetectionForRecognition();//专门为识别检测


	protected:
		CListCtrl m_list;
	IplImage* m_dspbuf;
	IplImage* m_imageBack, *m_alg_dropdown_bk, *m_db_dropdown_bk, *m_sex_bk;
	IplImage* LoadedPictureCopy[2];//专门保存LoadedPicture;

	int m_dialog_w,m_dialog_h;
	int m_client_w,m_client_h;//客户区长宽

	int BlueLine;//标题栏高度
	int MenuLeft_x,MenuLeft_y;//菜单栏左边半区域坐标
	int MenuLeft_w,MenuLeft_h;//菜单栏左边半区域坐标
	int MenuLength;//菜单中间总长度
	int MenuSingleLength;//单个菜单中间长度，不包括两边弧形渐变


	CString MenuLeft_gray,MenuLeft_blue,MenuLeft_b;//菜单栏灰色，蓝色 左半框 

	CString openFile,beginDect,beginRecognition,detectionQuery,recognitionQuery,m_createDBPath,
		m_detParamBtmPath,m_recogParamBtmPath,m_baseBtmPath,m_dbImgPath,m_dbTxtPath;
	int openFile_x,openFile_y,myButton_w,myButton_h;
	int beginDect_x,beginDect_y, m_baseBtnX, m_baseBtnY, m_createDBX, m_createDBY, m_dbDropDownX, m_dbDropDownY, 
		m_detParamX, m_detParamY, m_recogParamX, m_recogParamY, m_baseListX, m_baseListY, m_testDBX, m_testDBY, 
		m_sex_drop_x, m_sex_drop_y;
	CString playPath,stopPath;//播放与暂停按钮
	int play_r,stop_r;//播放与暂停按钮半径

	int ImageDisplayArea_x,ImageDisplayArea_y,ImageDisplayArea_w,ImageDisplayArea_h;//图片与视频显示区域

	bool picture_video_flag;
	bool picture_video_f[2];
	bool videoState[2];//视频播放状态
	bool DetectionState;//视频检测状态
	bool Detection_Recognition_flag;//标记是检测（false） 还是 识别（true）
	bool CensusFlag;//标记 当前是否有显示统计信息
	bool toCensusFlag;

	CString DetectionAnalysisTime,RecognitionAnalysisTime;//文件检测时间
	CString m_DetectionAnalysisTime,m_RecognitionAnalysisTime;//文件检测时间,检测过程，识别线程中使用，防止过程中切换菜单，改变DetectionAnalysisTime,RecognitionAnalysisTime的值

	CPoint mousePoint;

	CString fileAnalysisTime;
	CWinThread *m_thread_showVideoData;
//	CMyComboBox m_recog_alg;
	
	int CurrentAlgorithm;//0 表示HD_LBP， 1 表示 SparseCoding
	CString AlgorithmPath[4];//算法图片路径
	CString AlgorithmTitle,drop_down,rightbottom;
	int drop_down_x,drop_down_y,drop_down_w,drop_down_h;
	bool ShowDropDownListFlag, m_dbDrowDownFlag, m_sexDropDownFlag;
	
	CImageList m_img_list;
	IplImage *m_db_image, *m_del_img, *m_info_ok;
	int m_del_btn_x, m_del_btn_y, m_ok_btn_x, m_ok_btn_y, m_del_all_x, m_del_all_y, m_sex_x, m_sex_y;
	CString m_del_path, m_ok_path, m_del_all_path,m_CurDBPath, m_test_db_path, m_black_list_npath, m_black_list_ypath,
		    m_fsex_path, m_msex_path;
	//vector<string> m_db_names, m_db_pathes;
	CString m_nid, m_nage, m_strname,m_bsex;
	CEdit m_id, m_name, m_sex, m_age;
	CButton *m_btn_ok;
	CStatic m_group, m_sid, m_sname, m_ssex, m_sage;
	vector<DBINFO> m_db_info, m_att_db_info;
	rect m_alg_dropdown_rect;
	int m_db_status, m_sel_algorithm;
	int m_battention, m_attention_x, m_attention_y, m_attention_l;

	bool DetectionOver;
	bool RecognitionOver;
	bool stopThreadFlag;

	vector<CString> m_info;
	IplImage* image_settings;
	CString unknown;

	CString RecognitionTxtPath[2];
	bool MenuFlag;

	int DetectionSchedule;//当前检测到第几帧
	IplImage* play_speed;
	bool FastForwardRewindFlag;//快进快退标志
	double FastForwardRewindNum;//跳转到的帧数
	int currentPos;//记录当前滑块点
	bool draftingFlag;
	bool FinishDetectionFlag;//结束检测线程标志
	bool RecognitionState;
	bool FinishRecognitionFlag;//停止识别线程标志
	CWinThread *m_DetectionForRecognition;
	bool QueryRecognitionFlag;
	vector<CString> sigleData;//存储识别的每个人脸
	CString CurrentDetectionCensusFilePath;//记录当前统计界面显示的是哪个文件的检测信息	
	bool showPictureFlag;

	void MyInit();//初始化设置
	void ShowImageInCenter(IplImage* dst,int x,int y,int w,int h, IplImage* src);
	void GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos);
	void SetPointColour(IplImage* dst,int x,int y,int r,int g,int b);
	void SetPointColour(IplImage* dst,int x,int y,IplImage* src,int p_x,int p_y);
	void BasicBackGround();
	void InitPictureColour(IplImage* image,int l,int r,int t,int b,int R,int G,int B);//初始化图片一个区域
	void InitSetValue();//初始化设置变量的值
	void SetBackPic(CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f);//f==false原图，f==true镜像对称
	void SetBackPic(IplImage *image,int x,int y,int w,int h,int p_x,int p_y,bool f);
	void InitMenu();
	void ClickMenu(int num);//初始化菜单按钮
	void MyDetection(CPoint point);
	void MyRecognition(CPoint point);
	void MyCensus(CPoint point);
	void OnMySettings(CPoint point);
	void myOpenfile(CString &path,CString &filename);
	void showOpenfile();
	int testOpenfile(CString FilePath="",bool flag=false);
	void showPicture();
	
	
	void showVideoPlayAndStop(CString buttonPath,int r);
	void playVideo(CWinThread *&thread );
	void stopPlayVideo(CWinThread *&thread);
	void StartDetection();
	void WriteCStringR(CString s,FILE *fp);//自动换行
	void ReadCString(CString &s,FILE *fp);
	void ClearCStringVector(vector<CString> &data);
	void CreateNewDir(CString path);//新建一个目录
	void DrawFace(int num, IplImage* src,CString AnalysisTime);
	void pictureDetection();
	void myLoadPicture(CString picturePath,IplImage* &image);
	void myLoadPictureBy4channels(CString picturePath,IplImage*& image);
	void myDrawrectangular(IplImage* image,int l,int t,int r,int b,BYTE R,BYTE G,BYTE B);
	void WriteDetectionResults(CString txtPath,FACE &face);
//	void ReadDetectionAnalysisTime(vector<CString> &paths);
	void GetAnalysisTime(CString &AnalysisTime,CString txtPath);
	void SetAnalysisTime(CString Path,CString AnalysisTime);
	void stopDectVideo();
	void OpenAndPlay(CPoint point);
	void PictureCensusData(CString filepath,CString AnalysisTime);
	void VideoCensusData(CString path,CString AnalysisTime);
	void ReadFaceData(CString Path,FACE &faceData);
	void ReadFaceData(CString Path,int &faceCounts);
	void MyCopyImage(IplImage* &dst, IplImage* src);
	void StartRecognition();//识别部分
	void GetFaces(int num,CString AnalysisTime,FACE &face);
	void ShowDropDownList();
	void ShowDropDown(vector<string> pic_path, IplImage *bg, int x, int y, int w, int h);
	void SetDspbufPic(IplImage* &dst,CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f);//f==false原图，f==true镜像对称
	void ClearDropDownList();
	void ClearDropDownList(IplImage *bg, int x, int y);
	void ShowCurrentAlgorithm();
	void DrawMyRectangle(int l,int r,int t,int b);
	void CopyInHorizontal(IplImage* image,int l,int r,int t,int b,int x);
	void CopyImageInVerticalByMirror(IplImage* image,int l,int r,int t,int b,int y);
	void CopyInVertical(IplImage* image,int l,int r,int t,int b,int y);
	void ShowSettingsCtrl(int flag);
	void SetAttentionCheck(int flag);
	void EditDBInfo(CString img_path);
	int  DeleteDBInfo(int index);
	int GetBlackListType(string path);
	void DrawEditBlackListImage(CString path);
	void SetPathAndName();
	void DeleteDB();

	void RateOfAdvance(IplImage* image,int l,int r,int t,int b);//画一个进度条
	void RateOfAdvance(IplImage* image,int l,int r,int t,int b,float rate);
	void UpdateMyRectangle(int l,int r,int t,int b);
	void PutText(CString str, IplImage *image, int x, int y, float scale, CvScalar color = CV_RGB(10,10,10));
	void DrawGroupCtrl(CString str, IplImage *image, int x, int y, int w, int h, float scale);
	void WriteRecognitionResults(CString txtPath,vector<ALLFACE> &face);
	void ReadRecognitionResults(CString txtPath,vector<ALLFACE> &face);
	void DrawFace( IplImage* src,ALLFACE face);
	void ShowImageInCenter(IplImage* dst, IplImage* src,bool f,ALLFACE face);
	void WriteDBinfo(DBINFO info);
	
	void GetDBName(CString &db_time,CString &filefilepath,CString &name);
	void GetHeadPic(IplImage* image,ALLFACE &face);
	void WriteRecognitionCensusResults(CString txtPath,vector<ALLFACE> &face);
	void GetDBinfo(CString db_path,CString &name,int &age,CString &gender,int &id,CString &DBFile);
	void ReadRecognitionCensusResults(CString txtPath,vector<CString> &tmp_info,vector<int> &tmp_inf);
	void ShowRecognitionCensusData(CString path,CString AnalysisTime);
	void RecognitionVideoCensusData(CString path,CString AnalysisTime);
	void ShowPicComparison(int index);
	void WriteCensusResults(CString txtPath,vector<CString> &data);
	void ReadCensusResults(CString txtPath,vector<CString> &data);
	void MyCopyImage(IplImage* &dst, IplImage* src,int x,int y,int w,int h);
	void MyCopyImage(IplImage* &dst,int x,int y, IplImage* src);
	void SetDspbufPic(IplImage* &dst,CString pathname,int x,int y,int w,int h);
	void MyGetTime(CString &AnalysisTime);
	int IsValidString(CString str);
	void ShowDBDropDown();
	void ClearDBDropDown();
	void LoadDBImages(vector<string> &pathes);
	void originalDBImages();
	void PictureRecognition();
	void PictureRecognitionShow();
	void SaveFrameNum(int num,ALLFACE &face);
	void GetFrameNum(CString path,CString& numString);
	void MyGetTimeByAnalysisTime(CString AnalysisTime,CString &mytime);
	void LoadDBImages(string txt_path);
	void DrawFace( IplImage* src,FACE face);
	void DetectionPlaying();
	void RateOfAdvance(IplImage* image,int l,int r,int t,float rate);//画一个进度条
	void NumToTime(CString &timeString,int  num);//将将帧数转化为时间
	void MyCopyImage(IplImage* &dst,int x1,int y1, IplImage* src,int x,int y,int w,int h);
	void ShowPlayaTime(int curframeid);
	void ShowImageInCenter(IplImage* dst,int x,int y,int w,int h, IplImage* src,int curframeid);
	BOOL PreTranslateMessage(MSG* pMsg);
	void DetectionPauseAndResume();
	void FinishDetection();
	void SetBreakOffRecord(CString Path,CString filePath,int framNum);//设置中途停止检测/识别记录
	void GetBreakOffRecord(CString Path,CString filePath,int &framNum);//获取中途停止检测/识别记录
	void FinishRecognition();
	void ShowRecognition(int curframeid,int vframes);
	void ShowDetection(int curframeid,int vframes);
	void StartDetectionForRecognition();//专门为识别检测
	void CurrentRecognitionVideoCensusData();
	void AddSingleData(CString path,CString headpath);//逐条增加当前识别信息
	void initCensus();
	void showCensusLive();
	void ShowDetectionSelectedData( int row);
	void ShowWelcome();
// 实现
protected:
	HICON m_hIcon;

	CFont   font;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnClose();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLvnItemchangedListDete(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListbase(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnChoose(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};
